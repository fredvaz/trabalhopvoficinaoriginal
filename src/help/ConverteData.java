package help;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class ConverteData {

    public LocalDate converterStringtoLocalDate(String str) {

        LocalDate data;

        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        data = LocalDate.parse(str, formatador);

        return data;
    }

    public LocalDate converteDatetoLocalDate(java.sql.Date d) {
        String dataString;
        LocalDate dataLocal;
        DateTimeFormatter formatador;
        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        dataString = formato.format(d);
        dataLocal = LocalDate.parse(dataString, formatador);

        return dataLocal;
    }

    public java.sql.Date converteLocalDatetoDate(LocalDate data) throws Exception {
        DateTimeFormatter formatador;
        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dataString;
        java.sql.Date dataTipoDate;
        dataString = formatador.format(data);
        dataTipoDate = converteStringtoDate(dataString);

        return dataTipoDate;
    }

    public java.sql.Date converteStringtoDate(String data) throws Exception {
        if (data == null || data.equals("")) {
            return null;
        }
        @SuppressWarnings("UnusedAssignment")
        java.sql.Date date = null;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        date = new java.sql.Date(((java.util.Date) formatter.parse(data)).getTime());
        return date;
    }

    public boolean ValidaData(LocalDate data) {
        int a, m, d;
        boolean flag = false;

        a = data.getYear();
        m = data.getMonthValue();
        d = data.getDayOfMonth();
        System.out.println("Dia: " + d + "Mes: " + m + "Ano: " + a);
        if ((a % 4 == 0 && a % 100 != 0) || (a % 400 == 0)) {

            if ((m == 2) && (d >= 0 && d <= 29)) {
                flag = true;
            }
        } else {
            if ((m == 2) && (d >= 0 && d <= 28)) {
                flag = true;
            } else {
                flag = false;
            }

        }

        return flag;
    }

    public boolean isValid(String date) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate d = LocalDate.parse(date, formatter);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public java.sql.Time ConverteTimerString(String hora) throws Exception {
        if (hora == null || hora.equals("")) {
            return null;
        }
        java.sql.Time time = null;
        DateFormat formato = new SimpleDateFormat("HH:mm");
        time = new java.sql.Time(formato.parse(hora).getTime());
        return time;
    }

    public java.sql.Time ConverteLocalTimer(LocalTime hora) throws Exception {
        java.sql.Time horaBanco = null;
        String horaString;
        horaString = hora.toString();
        horaBanco = ConverteTimerString(horaString);

        return horaBanco;
    }

    public LocalTime ConverteSqlTimerLocalTimer(java.sql.Time horaBanco) {
        String horaString;
        horaString = horaBanco.toString();

        LocalTime hora = LocalTime.parse(horaString);
        return hora;
    }

}
