package help;

import java.text.DecimalFormat;

public class Mascara {

    public String removeMascaraCpf(String str) {

        str = str.replace(".", "");
        str = str.replace("-", "");

        return str;
    }

    public String removeMascaraCnpj(String str) {

        str = str.replace(".", "");
        str = str.replace("-", "");
        str = str.replace("/", "");

        return str;
    }

    public String removeMascaraData(String str) {

        str = str.replace("/", "");
        return str;
    }

    public String removeMascaraDouble(String str) {

        str = str.replace(",", ".");
        str = str.replace("R", "");
        str = str.replace("$", "");
        return str;
    }

    public String removeMascaraTelefone(String str) {
        str = str.replace("(", "");
        str = str.replace(")", "");
        str = str.replace("-", "");
        return str;
    }

    public String removeMascaraPlaca(String str) {

        str = str.replace("-", "");
        return str;
    }

    public double converterDoubleDoisDecimais(double precoDouble) {

        DecimalFormat fmt = new DecimalFormat("0.00");
        String string = fmt.format(precoDouble);
        String[] part = string.split("[,]");
        String string2 = part[0] + "." + part[1];
        double preco = Double.parseDouble(string2);
        return preco;
    }

 

}
