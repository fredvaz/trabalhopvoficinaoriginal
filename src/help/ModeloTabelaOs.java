package help;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import models.PecaServico;

public class ModeloTabelaOs extends AbstractTableModel {

    private final List<PecaServico> lista = new ArrayList<>();
    private final String[] colunas = {"Código", "Descrição", "Qtd", "Valor", "SubTotal"};

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                return lista.get(rowIndex).getCodigo();

            case 1:
                return lista.get(rowIndex).getDescricao();

            case 2:
                return lista.get(rowIndex).getQuantidade();

            case 3:
              return lista.get(rowIndex).getValor();
             //   return lista.get(rowIndex).getValorStr();

            case 4:
               return lista.get(rowIndex).getSubtotal();
               // return lista.get(rowIndex).getSubtotalStr();

        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return colunas[column];
    }

    public void addRow(PecaServico ps) {
        lista.add(ps);
        fireTableDataChanged();
    }

    public void removeRow(int row) {
        lista.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public ArrayList<PecaServico> getList() {
        return (ArrayList<PecaServico>) lista;
    }

    public void clearTable() {

        int tam = getRowCount() - 1;
        lista.clear();
        fireTableRowsDeleted(0, tam);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                lista.get(rowIndex).setCodigo(Integer.parseInt(aValue.toString()));
                break;
            case 1:
                lista.get(rowIndex).setDescricao(aValue.toString());
                break;
            case 2:
                lista.get(rowIndex).setQuantidade(Integer.parseInt(aValue.toString()));
                break;
            case 3:
                lista.get(rowIndex).setValor(Double.parseDouble(aValue.toString()));
                break;
            case 4:
                lista.get(rowIndex).setSubtotal(Double.parseDouble(aValue.toString()));
               // lista.get(rowIndex).setSubtotalStr(aValue.toString());
                break;
        }
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

}
