package help;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

public class DataSistema {

    public String dataSis() {
        Date data = new Date();
        SimpleDateFormat formatar = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = formatar.format(data);
        return dataFormatada;

    }
    
    public int horaSis(){
        int horaAtual = LocalTime.now().getHour();
       
        return horaAtual;
    }

    public int minutoSis(){
        
        int minutoAtual = LocalTime.now().getMinute();
          
        return minutoAtual;
    }
}