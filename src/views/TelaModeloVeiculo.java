package views;

import help.CampoTxt;
import controllers.ModeloDAO;
import help.LimitaNroCaracteres;
import java.awt.event.KeyEvent;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import models.ModeloVeiculo;

public final class TelaModeloVeiculo extends javax.swing.JDialog {

    private final ModeloDAO modDao;
    private final CampoTxt ct = new CampoTxt();
    private int controlBotao;
    int codModVeic;
    String opItem;
    private String modveicDtAt;

    public TelaModeloVeiculo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);
        modDao = new ModeloDAO();
        controlBotao = 0;
        txtModelo.setDocument(new LimitaNroCaracteres(13));
        txtMarca.setDocument(new LimitaNroCaracteres(13));
        txtCor.setDocument(new LimitaNroCaracteres(9));
        txtQtdPortas.setDocument(new LimitaNroCaracteres(3));
        txtMotor.setDocument(new LimitaNroCaracteres(6));
        txtCombustivel.setDocument(new LimitaNroCaracteres(10));
        codModVeic = 0;
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);
        preencheTabela();
        if (!modDao.VerificaModelo()) {
            tableModelo.requestFocus();
            tableModelo.changeSelection(0, 0, false, false);
        } else if (modDao.VerificaModelo()) {

            btnPesquisar.setEnabled(false);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelModelo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtModelo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCor = new javax.swing.JTextField();
        txtQtdPortas = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtMotor = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCombustivel = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableModelo = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnPesquisar = new javax.swing.JButton();
        btnRestaurar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        radioDesativados = new javax.swing.JRadioButton();
        radioAtivados = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modelo de Veiculos");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelModelo.setBackground(new java.awt.Color(204, 204, 204));
        panelModelo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Nome Modelo*");

        txtModelo.setEditable(false);
        txtModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtModeloKeyPressed(evt);
            }
        });

        jLabel3.setText("Marca*");

        txtMarca.setEditable(false);
        txtMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMarcaKeyPressed(evt);
            }
        });

        jLabel4.setText("Cor*");

        txtCor.setEditable(false);
        txtCor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCorKeyPressed(evt);
            }
        });

        txtQtdPortas.setEditable(false);
        txtQtdPortas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQtdPortasKeyPressed(evt);
            }
        });

        jLabel5.setText(" Portas*");

        jLabel6.setText("Motor*");

        txtMotor.setEditable(false);
        txtMotor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMotorKeyPressed(evt);
            }
        });

        jLabel7.setText("Combustível*");

        txtCombustivel.setEditable(false);
        txtCombustivel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCombustivelKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelModeloLayout = new javax.swing.GroupLayout(panelModelo);
        panelModelo.setLayout(panelModeloLayout);
        panelModeloLayout.setHorizontalGroup(
            panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModeloLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(txtCor, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtQtdPortas, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMotor, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(10, 10, 10))
        );
        panelModeloLayout.setVerticalGroup(
            panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModeloLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelModeloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtQtdPortas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMotor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableModelo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Modelo", "Marca", "Cor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableModelo.setColumnSelectionAllowed(true);
        tableModelo.getTableHeader().setReorderingAllowed(false);
        tableModelo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableModeloMouseClicked(evt);
            }
        });
        tableModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableModeloKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableModeloKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableModelo);
        tableModelo.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tableModelo.getColumnModel().getColumnCount() > 0) {
            tableModelo.getColumnModel().getColumn(0).setResizable(false);
            tableModelo.getColumnModel().getColumn(0).setPreferredWidth(8);
            tableModelo.getColumnModel().getColumn(1).setResizable(false);
            tableModelo.getColumnModel().getColumn(1).setPreferredWidth(95);
            tableModelo.getColumnModel().getColumn(2).setResizable(false);
            tableModelo.getColumnModel().getColumn(2).setPreferredWidth(100);
            tableModelo.getColumnModel().getColumn(3).setResizable(false);
            tableModelo.getColumnModel().getColumn(3).setPreferredWidth(80);
        }

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('t');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("Desativados");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("Ativos");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(radioDesativados)
                        .addGap(18, 18, 18)
                        .addComponent(radioAtivados))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRestaurar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRestaurar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioDesativados)
                    .addComponent(radioAtivados))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1)
                .addGap(10, 10, 10))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelModelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(panelModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setMnemonic('A');
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnExcluir.setBackground(new java.awt.Color(204, 204, 204));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnExcluir.setMnemonic('E');
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        btnExcluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExcluirKeyPressed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setMnemonic('D');
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar.png"))); // NOI18N
        btnAtivar.setMnemonic('V');
        btnAtivar.setText("Ativar");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {
            codModVeic = Integer.parseInt(itemTabela());

            controlBotao = 2;
            ct.campoEditavel(panelModelo);
            tableModelo.setEnabled(false);
            tableModelo.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
           
            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setVisible(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            txtModelo.requestFocus();

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void txtModeloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtModeloKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtMarca.requestFocus();
        }
    }//GEN-LAST:event_txtModeloKeyPressed

    private void txtMarcaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMarcaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCor.requestFocus();
        }
    }//GEN-LAST:event_txtMarcaKeyPressed

    private void txtCorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtQtdPortas.requestFocus();
        }
    }//GEN-LAST:event_txtCorKeyPressed

    private void txtQtdPortasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtdPortasKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtMotor.requestFocus();
        }
    }//GEN-LAST:event_txtQtdPortasKeyPressed

    private void txtMotorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMotorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCombustivel.requestFocus();
        }
    }//GEN-LAST:event_txtMotorKeyPressed

    private void txtCombustivelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCombustivelKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnGravar.requestFocus();
        }
    }//GEN-LAST:event_txtCombustivelKeyPressed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        controlBotao = 1;

        ct.clear(panelModelo);
        ct.campoEditavel(panelModelo);
        tableModelo.setVisible(false);
        tableModelo.setEnabled(false);
        btnIncluir.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        
        btnGravar.setEnabled(true);
        btnGravar.setVisible(true);
        btnCancelar.setEnabled(true);
        btnCancelar.setVisible(true);
        txtModelo.requestFocus();
        btnRestaurar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnPesquisar.setVisible(false);
        btnPesquisar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:

                if (txtModelo.getText().trim().isEmpty() || txtMarca.getText().trim().isEmpty()
                        || txtCor.getText().trim().isEmpty() || txtMotor.getText().trim().isEmpty()
                        || txtQtdPortas.getText().trim().isEmpty()
                        || txtCombustivel.getText().trim().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    txtModelo.requestFocus();

                } else {
                    cadastroModelo();

                }

                break;

            case 2:
                alteraModeloVeiculo();
                break;

        }
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);
        
        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);

        ct.clear(panelModelo);
        ct.campoNaoEditavel(panelModelo);
        btnIncluir.requestFocus();
        tableModelo.setVisible(true);
        tableModelo.setEnabled(true);
        preencheTabela();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            codModVeic = Integer.parseInt(itemTabela());
            if (!modDao.consultaStatusModVeiculo(codModVeic)) {
                tableModelo.setEnabled(false);
                tableModelo.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
               
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                excluiModeloVeic();
            } else {
                JOptionPane.showMessageDialog(null, "DESATIVE O MODELO DE VEÍCULO ANTES DE EXCLUIR!");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO DE VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void tableModeloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableModeloKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            DefaultTableModel modeloTab = (DefaultTableModel) tableModelo.getModel();

            int row = tableModelo.getSelectedRow();
            String valor = tableModelo.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            try {

                listaTextFiel();
            } catch (IndexOutOfBoundsException e) {

            }
        }
    }//GEN-LAST:event_tableModeloKeyPressed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            switch (controlBotao) {
                case 1:

                    if (txtModelo.getText().trim().isEmpty() || txtMarca.getText().trim().isEmpty()
                            || txtCor.getText().trim().isEmpty() || txtMotor.getText().trim().isEmpty()
                            || txtQtdPortas.getText().trim().isEmpty()
                            || txtCombustivel.getText().trim().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                        txtModelo.requestFocus();

                    } else {
                        cadastroModelo();

                    }

                    break;

                case 2:
                    alteraModeloVeiculo();
                    break;

            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (!modDao.VerificaModelo()) {
                preencheTabelaPesquisa();
            }

        }
    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void tableModeloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableModeloKeyReleased

        if (!modDao.VerificaModelo()) {
            listaTextFiel();
        }

    }//GEN-LAST:event_tableModeloKeyReleased

    private void tableModeloMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableModeloMouseClicked
        if (!modDao.VerificaModelo()) {
            listaTextFiel();
        }

    }//GEN-LAST:event_tableModeloMouseClicked

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed

        ct.clear(panelModelo);
        txtPesquisa.setText("");
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.requestFocus();
        desativaBotao();
        radioDesativados.setVisible(true);
        radioAtivados.setVisible(true);
        
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed

        ct.clear(panelModelo);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        preencheTabela();
        ativaBotao();
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void btnExcluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExcluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codModVeic = Integer.parseInt(itemTabela());
                tableModelo.setEnabled(false);
                tableModelo.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
               
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                excluiModeloVeic();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO DE VEÍCULO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnExcluirKeyPressed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codModVeic = Integer.parseInt(itemTabela());

                controlBotao = 2;
                ct.campoEditavel(panelModelo);
                tableModelo.setEnabled(false);
                tableModelo.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
               
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                btnGravar.setEnabled(true);
                btnGravar.setVisible(true);
                btnCancelar.setVisible(true);
                btnCancelar.setEnabled(true);
                txtModelo.requestFocus();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO VEÍCULO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed
        if (modDao.VerificaModelo()) {

            btnDesativar.setEnabled(false);
        } else {
            preencheTabelaDesativados();
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
        }
    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed
        preencheTabela();
        btnAtivar.setVisible(false);
        btnAtivar.setEnabled(false);
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_radioAtivadosActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        modveicDtAt = "desativado";
        try {
            int row = tableModelo.getSelectedRow();
            String valor = tableModelo.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            if (modDao.pendenciaModelo(cod)) {
                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO MODELO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    modDao.AtivaDesativa(modveicDtAt, cod);
                    JOptionPane.showMessageDialog(null, "MODELO VEÍCULO FOI DESATIVADO COM SUCESSO");
                }

                if (!valor.equals("")) {

                    preencheTabela();
                }
            } else {
                JOptionPane.showMessageDialog(null, "MODELO VEÍCULO NÃO PODE SER DESATIVADO! ELE ESTÁ SENDO UTILIZADO");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        modveicDtAt = "ativado";
        try {
            int row = tableModelo.getSelectedRow();
            String valor = tableModelo.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇÃO DO MODELO DE VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                modDao.AtivaDesativa(modveicDtAt, cod);
                JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO FOI ATIVADO COM SUCESSO");
            }

            if (!valor.equals("")) {

                preencheTabelaDesativados();
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM MODELO DE VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed

    public void listaTextFiel() {

        int row = tableModelo.getSelectedRow();
        String strCod = tableModelo.getValueAt(row, 0).toString();
        int codModeloVeiculo = Integer.parseInt(strCod);
        ModeloVeiculo m;

        if (modDao.buscaModelo(codModeloVeiculo)) {
            m = modDao.consultaModeloVeic(codModeloVeiculo);

            ct.clear(panelModelo);
            String qtdPortasStr = Integer.toString(m.getQtdPortas());
            txtQtdPortas.setText(qtdPortasStr);

            txtModelo.setText(m.getModelo());
            txtMarca.setText(m.getMarca());
            txtCor.setText(m.getCor());
            txtCombustivel.setText(m.getCombustivel());
            txtMotor.setText(m.getMotor());

        }
    }

    private void cadastroModelo() {
        boolean flag;
        int qtdPort = Integer.parseInt(txtQtdPortas.getText());

        ModeloVeiculo mv = new ModeloVeiculo();

        mv.setModelo(txtModelo.getText());
        mv.setMarca(txtMarca.getText());
        mv.setCor(txtCor.getText());
        mv.setQtdPortas(qtdPort);
        mv.setMotor(txtMotor.getText());
        mv.setCombustivel(txtCombustivel.getText());
        if (modDao.verificaModeloIgual(mv)) {

            JOptionPane.showMessageDialog(null, "ESTE MODELO DE VEÍCULO JÁ FOI CADASTRADO");
            ct.clear(panelModelo);
            ct.campoEditavel(panelModelo);
            tableModelo.setVisible(false);
            tableModelo.setEnabled(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setEnabled(true);
            btnCancelar.setVisible(true);
            txtModelo.requestFocus();
            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);
            txtModelo.requestFocus();

        } else {

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO MODELO DE VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = modDao.addModelo(mv);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO CADASTRADO COM SUCESSO");

                        ct.clear(panelModelo);

                        ct.campoNaoEditavel(panelModelo);
                        SelecionaPrimeiraLinha();
                        preencheTabela();
                        tableModelo.setVisible(true);
                        tableModelo.setEnabled(true);
                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnPesquisar.setVisible(true);
                        btnPesquisar.setEnabled(true);
                    } else if (flag == false) {

                        btnIncluir.setEnabled(false);
                        btnAlterar.setEnabled(false);
                        btnExcluir.setEnabled(false);
                        
                        btnGravar.setEnabled(true);
                        btnGravar.setVisible(true);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

                }

            } else if (resp == 1) {

                ct.clear(panelModelo);
                ct.campoNaoEditavel(panelModelo);

                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
            }

        }

    }

    private void alteraModeloVeiculo() {

        boolean flag;

        if (txtModelo.getText().trim().isEmpty() || txtMarca.getText().trim().isEmpty()
                || txtQtdPortas.getText().isEmpty() || txtCor.getText().trim().isEmpty()
                || txtMotor.getText().trim().isEmpty() || txtCombustivel.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
            txtModelo.requestFocus();
        } else {

            ModeloVeiculo mv = new ModeloVeiculo();

            mv.setModelo(txtModelo.getText());
            mv.setMarca((txtMarca.getText()));
            mv.setCor(txtCor.getText());
            mv.setQtdPortas(Integer.parseInt(txtQtdPortas.getText()));
            mv.setMotor(txtMotor.getText());
            mv.setCombustivel(txtCombustivel.getText());

            if (modDao.verificaModeloIgual(mv)) {

                JOptionPane.showMessageDialog(null, "ESTE MODELO DE VEÍCULO JÁ FOI CADASTRADO");
            } else {

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO MODELO DE VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {
                    try {
                        flag = modDao.alterarModVeiculo(mv, codModVeic);
                        if (flag == true) {
                            JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO ALTERADO COM SUCESSO");
                            ct.clear(panelModelo);
                            ct.campoNaoEditavel(panelModelo);
                            txtPesquisa.setVisible(true);
                            btnGravar.setEnabled(false);
                            btnGravar.setVisible(false);
                            btnCancelar.setEnabled(false);
                            btnCancelar.setVisible(false);
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else if (resp == 1) {
                    ct.clear(panelModelo);
                    ct.campoNaoEditavel(panelModelo);
                    txtPesquisa.setVisible(true);
                    tableModelo.setEnabled(true);
                    tableModelo.setVisible(true);
                    preencheTabela();
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                   
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);
                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setEnabled(false);
                    btnCancelar.setVisible(false);
                }

                preencheTabela();
                tableModelo.setEnabled(true);
                tableModelo.setVisible(true);
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
               
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);

            }

        }

    }

    public void excluiModeloVeic() {
        boolean flag;

        if (modDao.pendenciaModelo(codModVeic)) {
            if (modDao.buscaModelo(codModVeic)) {
                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DO MODELO DE VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {

                    flag = modDao.excluirModeloVeic(codModVeic);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO EXCLUÍDO COM SUCESSO");

                        preencheTabela();
                        tableModelo.setEnabled(true);
                        tableModelo.setVisible(true);

                        ct.clear(panelModelo);

                        ct.campoNaoEditavel(panelModelo);

                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                       
                        btnPesquisar.setVisible(true);
                        btnPesquisar.setEnabled(true);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);

                    }
                } else if (resp == 1) {

                    ct.clear(panelModelo);

                    ct.campoNaoEditavel(panelModelo);
                    tableModelo.setEnabled(true);
                    tableModelo.setVisible(true);
                    preencheTabela();
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);

                }

            } else {
                JOptionPane.showMessageDialog(null, "NÃO EXISTE MODELO DE VEÍCULO COM ESTE CÓDIGO");

                ct.clear(panelModelo);

                ct.campoNaoEditavel(panelModelo);
                tableModelo.setEnabled(true);
                tableModelo.setVisible(true);
                preencheTabela();
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
                
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
            }
        } else {

            ct.clear(panelModelo);
            JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO ESTÁ SENDO UTILIZADO E NÃO PODE SER EXCLUIDO");

            ct.campoNaoEditavel(panelModelo);
            tableModelo.setEnabled(true);
            tableModelo.setVisible(true);
            preencheTabela();
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
            
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
        }

    }

    public String itemTabela() {

        int row = tableModelo.getSelectedRow();
        String valor = tableModelo.getValueAt(row, 0).toString();

        return valor;
    }

    public void preencheTabelaPesquisa() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableModelo.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.buscaModeloTabela(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                m.getCodigo(),
                m.getModelo(),
                m.getMarca(),
                m.getCor()

            });

        }

    }

    public void SelecionaPrimeiraLinha() {
        tableModelo.clearSelection();
        tableModelo.changeSelection(0, 0, false, false);
        tableModelo.requestFocus();
    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableModelo.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.preencheTabela()) {

            modeloTab.addRow(new Object[]{
                m.getCodigo(),
                m.getModelo(),
                m.getMarca(),
                m.getCor()

            });

        }

    }

    public void preencheTabelaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableModelo.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.preencheTabelaDesativados()) {

            modeloTab.addRow(new Object[]{
                m.getCodigo(),
                m.getModelo(),
                m.getMarca(),
                m.getCor()

            });

        }

    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableModelo.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.buscaModeloTabelaDesativados(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                m.getCodigo(),
                m.getModelo(),
                m.getMarca(),
                m.getCor()

            });

        }

    }

    public void desativaBotao() {
        btnIncluir.setEnabled(false);

    }

    public void ativaBotao() {
        btnIncluir.setEnabled(true);

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelModelo;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tableModelo;
    private javax.swing.JTextField txtCombustivel;
    private javax.swing.JTextField txtCor;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtMotor;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JTextField txtQtdPortas;
    // End of variables declaration//GEN-END:variables
}
