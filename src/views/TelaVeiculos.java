package views;
import help.CampoTxt;
import help.Mascara;
import controllers.ClienteDAO;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import models.Cliente;
import models.PessoaFisica;
import models.PessoaJuridica;
import controllers.ModeloDAO;
import controllers.VeiculoDAO;
import help.ConverteData;
import help.DataSistema;
import help.LimitaNroCaracteres;
import help.LimitaNroCaracteresNumero;
import java.awt.Frame;
import java.time.LocalDate;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultFormatterFactory;
import models.ModeloVeiculo;
import models.Veiculo;

public final class TelaVeiculos extends javax.swing.JDialog {

    private Mascara masc = new Mascara();
    private CampoTxt ct = new CampoTxt();
    private DataSistema dt = new DataSistema();
    private MaskFormatter placaMask;
    private ConverteData conv = new ConverteData();
    private final ClienteDAO cDAO;
    private final VeiculoDAO vDAO;
    private final ModeloDAO mDAO;
    private String opcao;
    private String opcMod;
    private int codCliente;
    private int codModelo;
    private int codigoVeiculo;
    private int controlBotao;
    String opItem;
    private String veiculoAtDt;

    public TelaVeiculos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);

        txtAno.setDocument(new LimitaNroCaracteresNumero(4));
        txtKm.setDocument(new LimitaNroCaracteresNumero(6));
        txtChassi.setDocument(new LimitaNroCaracteres(25));

        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
        btnAtivar.setVisible(false);

        try {
            placaMask = new MaskFormatter("???-####");

        } catch (ParseException ex) {
            Logger.getLogger(TelaVeiculos.class.getName()).log(Level.SEVERE, null, ex);
        }
        opcao = "";
        cDAO = new ClienteDAO();
        vDAO = new VeiculoDAO();
        controlBotao = 0;
        codigoVeiculo = 0;
        mDAO = new ModeloDAO();

        if (!vDAO.VerificaVeiculo()) {
            DefaultTableModel modeloTab = (DefaultTableModel) tableVeiculo.getModel();
            tableVeiculo.setRowSorter(new TableRowSorter(modeloTab));
            preencheTabelaVeic();
            tableVeiculo.requestFocus();
            tableVeiculo.changeSelection(0, 0, false, false);
        } else if (vDAO.VerificaVeiculo()) {

            btnPesquisar.setEnabled(false);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelCliente = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ftxtCnpjCpf = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNomeCliente = new javax.swing.JTextField();
        txtCodCliente = new javax.swing.JTextField();
        panelVeic1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ftxPlaca = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        txtChassi = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtAno = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtKm = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtModelo = new javax.swing.JTextField();
        panelModeloveiculo = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtNomeModelo = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtMotor = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtQtdPortas = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtCor = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtCombustivel = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableVeiculo = new javax.swing.JTable();
        panelPesquisa = new javax.swing.JPanel();
        btnRestaurar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        radioDesativados = new javax.swing.JRadioButton();
        radioAtivados = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Veiculos");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelCliente.setBackground(new java.awt.Color(204, 204, 204));
        panelCliente.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Cliente*");

        ftxtCnpjCpf.setEditable(false);

        jLabel3.setText("CPF/CNPJ");

        jLabel4.setText("Nome/Razão Social");

        txtNomeCliente.setEditable(false);

        txtCodCliente.setEditable(false);
        txtCodCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodClienteKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelClienteLayout = new javax.swing.GroupLayout(panelCliente);
        panelCliente.setLayout(panelClienteLayout);
        panelClienteLayout.setHorizontalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClienteLayout.createSequentialGroup()
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel2))
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(ftxtCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addComponent(txtNomeCliente)
                        .addContainerGap())))
        );
        panelClienteLayout.setVerticalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClienteLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftxtCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        panelVeic1.setBackground(new java.awt.Color(204, 204, 204));
        panelVeic1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setText("Placa*");

        ftxPlaca.setEditable(false);
        ftxPlaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ftxPlacaKeyPressed(evt);
            }
        });

        jLabel6.setText("Chassi*");

        txtChassi.setEditable(false);
        txtChassi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtChassiKeyPressed(evt);
            }
        });

        jLabel7.setText("Ano*");

        txtAno.setEditable(false);
        txtAno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAnoKeyPressed(evt);
            }
        });

        jLabel8.setText("KM*");

        txtKm.setEditable(false);
        txtKm.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtKmKeyPressed(evt);
            }
        });

        jLabel9.setText("Modelo Veículo*");

        txtModelo.setEditable(false);
        txtModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtModeloKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelVeic1Layout = new javax.swing.GroupLayout(panelVeic1);
        panelVeic1.setLayout(panelVeic1Layout);
        panelVeic1Layout.setHorizontalGroup(
            panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVeic1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtModelo))
                .addGap(10, 10, 10)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ftxPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(10, 10, 10)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAno, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(10, 10, 10)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtKm, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(10, 10, 10)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(panelVeic1Layout.createSequentialGroup()
                        .addComponent(txtChassi)
                        .addGap(10, 10, 10))))
        );
        panelVeic1Layout.setVerticalGroup(
            panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVeic1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelVeic1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftxPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChassi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtKm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelModeloveiculo.setBackground(new java.awt.Color(204, 204, 204));
        panelModeloveiculo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Nome Modelo:");

        txtNomeModelo.setEditable(false);

        jLabel11.setText("Marca:");

        txtMarca.setEditable(false);

        jLabel12.setText("Motor:");

        txtMotor.setEditable(false);

        jLabel13.setText("Qtd.Portas:");

        txtQtdPortas.setEditable(false);

        jLabel14.setText("Cor:");

        txtCor.setEditable(false);

        jLabel15.setText("Combustível:");

        txtCombustivel.setEditable(false);

        javax.swing.GroupLayout panelModeloveiculoLayout = new javax.swing.GroupLayout(panelModeloveiculo);
        panelModeloveiculo.setLayout(panelModeloveiculoLayout);
        panelModeloveiculoLayout.setHorizontalGroup(
            panelModeloveiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModeloveiculoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelModeloveiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModeloveiculoLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtQtdPortas, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCor))
                    .addGroup(panelModeloveiculoLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMotor)))
                .addContainerGap())
        );
        panelModeloveiculoLayout.setVerticalGroup(
            panelModeloveiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModeloveiculoLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelModeloveiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtNomeModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txtMotor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelModeloveiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtQtdPortas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtCor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableVeiculo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Placa", "Modelo", "CPF/CNPJ", "Cliente"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableVeiculo.getTableHeader().setReorderingAllowed(false);
        tableVeiculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableVeiculoMouseClicked(evt);
            }
        });
        tableVeiculo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableVeiculoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableVeiculoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableVeiculo);
        if (tableVeiculo.getColumnModel().getColumnCount() > 0) {
            tableVeiculo.getColumnModel().getColumn(0).setResizable(false);
            tableVeiculo.getColumnModel().getColumn(0).setPreferredWidth(8);
            tableVeiculo.getColumnModel().getColumn(1).setResizable(false);
            tableVeiculo.getColumnModel().getColumn(1).setPreferredWidth(15);
            tableVeiculo.getColumnModel().getColumn(2).setResizable(false);
            tableVeiculo.getColumnModel().getColumn(2).setPreferredWidth(8);
            tableVeiculo.getColumnModel().getColumn(3).setResizable(false);
            tableVeiculo.getColumnModel().getColumn(3).setPreferredWidth(15);
            tableVeiculo.getColumnModel().getColumn(4).setResizable(false);
            tableVeiculo.getColumnModel().getColumn(4).setPreferredWidth(80);
        }

        panelPesquisa.setBackground(new java.awt.Color(204, 204, 204));
        panelPesquisa.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('T');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });
        btnRestaurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnRestaurarKeyPressed(evt);
            }
        });

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        btnPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPesquisarKeyPressed(evt);
            }
        });

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("Desativados");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("Ativos");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPesquisaLayout = new javax.swing.GroupLayout(panelPesquisa);
        panelPesquisa.setLayout(panelPesquisaLayout);
        panelPesquisaLayout.setHorizontalGroup(
            panelPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPesquisaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPesquisaLayout.createSequentialGroup()
                        .addComponent(radioDesativados)
                        .addGap(18, 18, 18)
                        .addComponent(radioAtivados))
                    .addGroup(panelPesquisaLayout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addGap(10, 10, 10)
                        .addComponent(btnRestaurar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa)))
                .addContainerGap())
        );
        panelPesquisaLayout.setVerticalGroup(
            panelPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPesquisaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRestaurar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioDesativados)
                    .addComponent(radioAtivados))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                    .addComponent(panelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelVeic1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelModeloveiculo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelVeic1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelModeloveiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(panelPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setMnemonic('A');
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnExcluir.setBackground(new java.awt.Color(204, 204, 204));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnExcluir.setMnemonic('E');
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        btnExcluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExcluirKeyPressed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setMnemonic('d');
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar (1).png"))); // NOI18N
        btnAtivar.setMnemonic('v');
        btnAtivar.setText("Ativar");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(200, 200, 200)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtModeloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtModeloKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (controlBotao == 1) {

                if (txtModelo.getText().equals("")) {

                    preencheModeloVeic();
                } else {
                    retModVeic();

                }

                if (!txtModelo.getText().equals("")) {
                    ftxPlaca.requestFocus();
                }

            } else if (controlBotao == 2) {
                if (txtModelo.getText().equals("")) {

                    preencheModeloVeic();
                } else {
                    retModVeic();

                }

                if (!txtModelo.getText().equals("")) {
                    ftxPlaca.requestFocus();
                }

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheModeloVeic();
        }

    }//GEN-LAST:event_txtModeloKeyPressed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        controlBotao = 1;
        txtCodCliente.setEditable(true);
        txtModelo.setEditable(true);
        ct.campoEditavel(panelVeic1);
        ct.clear(panelCliente);
        ct.clear(panelModeloveiculo);
        ct.clear(panelVeic1);
        tableVeiculo.setEnabled(false);
        tableVeiculo.setVisible(false);
        btnIncluir.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
       
        btnRestaurar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnPesquisar.setVisible(false);
        btnPesquisar.setEnabled(false);
        btnGravar.setEnabled(true);
        btnGravar.setVisible(true);
        btnCancelar.setVisible(true);
        btnCancelar.setEnabled(true);

        txtAno.setDocument(new LimitaNroCaracteresNumero(4));
        txtKm.setDocument(new LimitaNroCaracteresNumero(6));
        txtChassi.setDocument(new LimitaNroCaracteres(25));

        ftxPlaca.setValue(null);
        ftxPlaca.setFormatterFactory(new DefaultFormatterFactory(placaMask));
        txtCodCliente.requestFocus();
        btnDesativar.setEnabled(false);
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void txtCodClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodClienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (controlBotao == 1) {

                if (txtCodCliente.getText().equals("")) {

                    preencheCliente();
                } else {
                    retCliente();

                }

                if (!txtCodCliente.getText().equals("")) {
                    txtModelo.requestFocus();
                }

            } else if (controlBotao == 2) {
                if (txtCodCliente.getText().equals("")) {

                    preencheCliente();
                } else {
                    retCliente();

                }

                if (!txtCodCliente.getText().equals("")) {
                    txtModelo.requestFocus();
                }

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheCliente();
        }


    }//GEN-LAST:event_txtCodClienteKeyPressed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed
        ct.clear(panelCliente);
        ct.clear(panelModeloveiculo);
        ct.clear(panelVeic1);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        preencheTabelaVeic();
        ativaBotao();
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        ct.clear(panelCliente);
        ct.clear(panelModeloveiculo);
        ct.clear(panelVeic1);
        txtPesquisa.setText("");
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.requestFocus();
        desativaBotao();
        radioDesativados.setVisible(true);
        radioAtivados.setVisible(true);
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:

                if (txtCodCliente.getText().trim().isEmpty() || txtModelo.getText().trim().isEmpty()
                        || masc.removeMascaraPlaca(ftxPlaca.getText()).trim().isEmpty()
                        || txtAno.getText().trim().isEmpty() || txtKm.getText().trim().isEmpty()
                        || txtChassi.getText().trim().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    txtCodCliente.requestFocus();

                } else {
                    cadastroVeic();

                }

                break;

            case 2:
                alteraVeiculo();
                break;

        }


    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        ct.clear(panelCliente);
        ct.clear(panelModeloveiculo);
        ct.clear(panelVeic1);
        ct.campoNaoEditavel(panelCliente);
        ct.campoNaoEditavel(panelModeloveiculo);
        ct.campoNaoEditavel(panelVeic1);

        tableVeiculo.setVisible(true);
        tableVeiculo.setEnabled(true);
        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);
        
        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnDesativar.setEnabled(true);

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void ftxPlacaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxPlacaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtAno.requestFocus();
        }
    }//GEN-LAST:event_ftxPlacaKeyPressed

    private void txtAnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAnoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String dataStr = dt.dataSis();
            LocalDate dataAt = conv.converterStringtoLocalDate(dataStr);
            int anodig = Integer.parseInt(txtAno.getText());
            int anoat = dataAt.getYear();
            if (anodig > anoat) {
                JOptionPane.showMessageDialog(null, "ANO INFORMADO É MAIOR QUE O ANO ATUAL");
                txtAno.setText("");
                txtAno.requestFocus();
            } else {
                txtKm.requestFocus();
            }

        }
    }//GEN-LAST:event_txtAnoKeyPressed

    private void txtKmKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtKmKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtChassi.requestFocus();
        }
    }//GEN-LAST:event_txtKmKeyPressed

    private void txtChassiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtChassiKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnGravar.requestFocus();
        }
    }//GEN-LAST:event_txtChassiKeyPressed

    private void tableVeiculoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableVeiculoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int row = tableVeiculo.getSelectedRow();
            String valor = tableVeiculo.getValueAt(row, 1).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tableVeiculoKeyPressed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            switch (controlBotao) {
                case 1:

                    if (txtCodCliente.getText().trim().isEmpty() || txtModelo.getText().trim().isEmpty()
                            || masc.removeMascaraPlaca(ftxPlaca.getText()).trim().isEmpty()
                            || txtAno.getText().trim().isEmpty() || txtKm.getText().trim().isEmpty()
                            || txtChassi.getText().trim().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                        txtCodCliente.requestFocus();

                    } else {
                        cadastroVeic();

                    }

                    break;

                case 2:
                    alteraVeiculo();
                    break;

            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void tableVeiculoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableVeiculoKeyReleased
        listaTextFiel();
    }//GEN-LAST:event_tableVeiculoKeyReleased

    private void tableVeiculoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableVeiculoMouseClicked
        listaTextFiel();
    }//GEN-LAST:event_tableVeiculoMouseClicked

    private void btnPesquisarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPesquisarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);
            txtPesquisa.setText("");
            btnRestaurar.setVisible(true);
            btnRestaurar.setEnabled(true);
            txtPesquisa.setEnabled(true);
            txtPesquisa.setVisible(true);
            txtPesquisa.requestFocus();
            desativaBotao();
        }
    }//GEN-LAST:event_btnPesquisarKeyPressed

    private void btnRestaurarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnRestaurarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);
            preencheTabelaVeic();
            ativaBotao();
        }
    }//GEN-LAST:event_btnRestaurarKeyPressed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            codigoVeiculo = Integer.parseInt(itemTabela());
            if (!vDAO.consultaStatusVeiculo(codigoVeiculo)) {
                tableVeiculo.setEnabled(false);
                tableVeiculo.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
                
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                excluiVeiculo();
            } else {
                JOptionPane.showMessageDialog(null, "DESATIVE O VEÍCULO ANTES DE EXCLUIR!");

            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnExcluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExcluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codigoVeiculo = Integer.parseInt(itemTabela());
                if (!vDAO.consultaStatusVeiculo(codigoVeiculo)) {
                    tableVeiculo.setEnabled(false);
                    tableVeiculo.setVisible(false);
                    btnIncluir.setEnabled(false);
                    btnAlterar.setEnabled(false);
                    btnExcluir.setEnabled(false);
                    
                    btnRestaurar.setEnabled(false);
                    btnRestaurar.setVisible(false);
                    btnPesquisar.setVisible(false);
                    btnPesquisar.setEnabled(false);
                    excluiVeiculo();
                } else {
                    JOptionPane.showMessageDialog(null, "DESATIVE O VEÍCULO ANTES DE EXCLUIR!");
                    btnRestaurar.setEnabled(false);
                    btnRestaurar.setVisible(false);
                    btnPesquisar.setVisible(false);
                    btnPesquisar.setEnabled(false);
                }

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM VEÍCULO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnExcluirKeyPressed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (radioAtivados.isSelected()) {
                preencheTabelaPesquisa();
            } else if (radioDesativados.isSelected()) {

                preencheTabelaPesquisaDesativados();
            }
        }
    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {
            codigoVeiculo = Integer.parseInt(itemTabela());
            controlBotao = 2;
            txtCodCliente.setEditable(true);
            txtModelo.setEditable(true);
            ct.campoEditavel(panelVeic1);
            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);
            tableVeiculo.setEnabled(false);
            tableVeiculo.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            
            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setVisible(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            txtCodCliente.requestFocus();

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codigoVeiculo = Integer.parseInt(itemTabela());
                controlBotao = 2;
                txtCodCliente.setEditable(true);
                txtModelo.setEditable(true);
                ct.campoEditavel(panelVeic1);
                ct.clear(panelCliente);
                ct.clear(panelModeloveiculo);
                ct.clear(panelVeic1);
                tableVeiculo.setEnabled(false);
                tableVeiculo.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
                
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                btnGravar.setEnabled(true);
                btnGravar.setVisible(true);
                btnCancelar.setVisible(true);
                btnCancelar.setEnabled(true);
                txtCodCliente.requestFocus();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        veiculoAtDt = "desativado";
        try {
            int row = tableVeiculo.getSelectedRow();
            String valor = tableVeiculo.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            if (vDAO.pendenciaVeiculo(cod)) {
                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    vDAO.AtivaDesativa(veiculoAtDt, cod);
                    JOptionPane.showMessageDialog(null, "VEÍCULO FOI DESATIVADO COM SUCESSO");
                }

                if (!valor.equals("")) {

                    preencheTabelaVeic();
                }
            } else {
                JOptionPane.showMessageDialog(null, "VEÍCULO NÃO PODE SER DESATIVADO! ELE ESTÁ SENDO UTILIZADO");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        veiculoAtDt = "ativado";
        try {
            int row = tableVeiculo.getSelectedRow();
            String valor = tableVeiculo.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇÃO DO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                vDAO.AtivaDesativa(veiculoAtDt, cod);
                JOptionPane.showMessageDialog(null, "VEÍCULO FOI ATIVADO COM SUCESSO");
            }

            if (!valor.equals("")) {

                preencheTabelaVeicDesativados();
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM VEÍCULO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed
        if (vDAO.VerificaVeiculo()) {

            btnDesativar.setEnabled(false);
        } else {
            preencheTabelaVeicDesativados();
            btnDesativar.setEnabled(false);
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
        }
    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed
        preencheTabelaVeic();
        btnAtivar.setVisible(false);
        btnAtivar.setEnabled(false);
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_radioAtivadosActionPerformed

    public void excluiVeiculo() {
        boolean flag;

        if (vDAO.pendenciaVeiculo(codigoVeiculo)) {
            if (vDAO.buscaVeiculo(codigoVeiculo)) {

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {

                    flag = vDAO.excluirVeiculo(codigoVeiculo);
                    if (flag == true) {

                        JOptionPane.showMessageDialog(null, "VEÍCULO EXCLUÍDO COM SUCESSO");

                        preencheTabelaVeic();
                        tableVeiculo.setEnabled(true);
                        tableVeiculo.setVisible(true);

                        ct.clear(panelCliente);
                        ct.clear(panelModeloveiculo);
                        ct.clear(panelVeic1);
                        ct.campoNaoEditavel(panelCliente);
                        ct.campoNaoEditavel(panelModeloveiculo);
                        ct.campoNaoEditavel(panelVeic1);
                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        
                        btnPesquisar.setVisible(true);
                        btnPesquisar.setEnabled(true);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);

                    }
                } else if (resp == 1) {

                    ct.clear(panelCliente);
                    ct.clear(panelModeloveiculo);
                    ct.clear(panelVeic1);

                    ct.campoNaoEditavel(panelCliente);
                    ct.campoNaoEditavel(panelModeloveiculo);
                    ct.campoNaoEditavel(panelVeic1);
                    tableVeiculo.setEnabled(true);
                    tableVeiculo.setVisible(true);
                    preencheTabelaVeic();
                    tableVeiculo.requestFocus();
                    tableVeiculo.changeSelection(0, 0, false, false);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);

                }

            } else {

                JOptionPane.showMessageDialog(null, "NÃO VEÍCULO COM ESTE CÓDIGO");

                ct.clear(panelCliente);
                ct.clear(panelModeloveiculo);
                ct.clear(panelVeic1);

                ct.campoNaoEditavel(panelCliente);
                ct.campoNaoEditavel(panelModeloveiculo);
                ct.campoNaoEditavel(panelVeic1);
                tableVeiculo.setEnabled(true);
                tableVeiculo.setVisible(true);
                preencheTabelaVeic();
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
                
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
            }
        } else {

            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);

            JOptionPane.showMessageDialog(null, "VEÍCULO ESTÁ SENDO UTILIZADO E NÃO PODE SER EXCLUIDO");

            ct.campoNaoEditavel(panelCliente);
            ct.campoNaoEditavel(panelModeloveiculo);
            ct.campoNaoEditavel(panelVeic1);
            tableVeiculo.setEnabled(true);
            tableVeiculo.setVisible(true);
            preencheTabelaVeic();
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
            
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
        }

    }

    private void alteraVeiculo() {

        boolean flag;

        if (txtCodCliente.getText().trim().isEmpty() || txtModelo.getText().trim().isEmpty()
                || masc.removeMascaraPlaca(ftxPlaca.getText()).trim().isEmpty()
                || txtAno.getText().trim().isEmpty() || txtKm.getText().trim().isEmpty()
                || txtChassi.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
            txtCodCliente.requestFocus();
        } else {

            Veiculo ve = new Veiculo();

            ve.setCod_cliente(Integer.parseInt(txtCodCliente.getText()));

            ve.setPlaca(masc.removeMascaraPlaca(ftxPlaca.getText()));
            ve.setCod_modveic(Integer.parseInt(txtModelo.getText()));
            ve.setAno(Integer.parseInt(txtAno.getText()));
            ve.setKm(Integer.parseInt(txtKm.getText()));
            ve.setChassi(txtChassi.getText());

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = vDAO.alterarVeiculo(ve, codigoVeiculo);
                    if (flag == true) {

                        JOptionPane.showMessageDialog(null, "VEÍCULO ALTERADO COM SUCESSO");
                        ct.clear(panelCliente);
                        ct.clear(panelModeloveiculo);
                        ct.clear(panelVeic1);
                        ct.campoNaoEditavel(panelCliente);
                        ct.campoNaoEditavel(panelModeloveiculo);
                        ct.campoNaoEditavel(panelVeic1);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);
                        btnDesativar.setEnabled(true);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (resp == 1) {
                ct.clear(panelCliente);
                ct.clear(panelModeloveiculo);
                ct.clear(panelVeic1);
                ct.campoNaoEditavel(panelCliente);
                ct.campoNaoEditavel(panelModeloveiculo);
                ct.campoNaoEditavel(panelVeic1);
                tableVeiculo.setEnabled(true);
                tableVeiculo.setVisible(true);
                preencheTabelaVeic();

                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
                
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);
                btnDesativar.setEnabled(true);
            }

            preencheTabelaVeic();
            tableVeiculo.setEnabled(true);
            tableVeiculo.setVisible(true);
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
            
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setEnabled(false);
            btnCancelar.setVisible(false);

        }

    }

    public void listaTextFiel() {

        int row = tableVeiculo.getSelectedRow();
        String strCod = tableVeiculo.getValueAt(row, 0).toString();
        int codV = Integer.parseInt(strCod);
        Veiculo v;
        v = vDAO.consultaVeiculo(codV);

        ModeloVeiculo m = vDAO.recebeConsultaVeiculo();
        if (vDAO.buscaVeiculo(codV)) {

            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);
            ftxPlaca.setText(v.getPlaca());
            txtAno.setText(Integer.toString(v.getAno()));
            txtKm.setText(Integer.toString(v.getKm()));
            txtChassi.setText(v.getChassi());
            txtNomeModelo.setText(m.getModelo());
            txtMarca.setText(m.getMarca());
            txtCor.setText(m.getCor());
            txtMotor.setText(m.getMotor());
            txtQtdPortas.setText(Integer.toString(m.getQtdPortas()));
            txtCombustivel.setText(m.getCombustivel());
            ftxtCnpjCpf.setText(v.getCpfCnpjCliente());
            txtNomeCliente.setText(v.getNomeCliente());

        }
    }

    public String itemTabela() {

        int row = tableVeiculo.getSelectedRow();
        String valor = tableVeiculo.getValueAt(row, 0).toString();

        return valor;
    }

    public void preencheTabelaPesquisa() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableVeiculo.getModel();
        modeloTab.setNumRows(0);

        for (Veiculo v : vDAO.buscaVeiculoTabela(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                v.getCodigo(),
                v.getPlaca(),
                v.getNomeCliente(),
                v.getCpfCnpjCliente(),
                v.getNomeModelo()

            });

        }

    }

    public void retCliente() {
        int codigo = Integer.parseInt(txtCodCliente.getText());

        if (cDAO.buscaCliente(codigo)) {

            if (cDAO.consultaStatusCliente(codigo)) {

                Cliente c;
                c = cDAO.consultaCliente(codigo);
                if (c instanceof PessoaFisica) {
                    ftxtCnpjCpf.setText(((PessoaFisica) c).getCpfCnpj());
                    txtNomeCliente.setText(((PessoaFisica) c).getNome());

                } else if (c instanceof PessoaJuridica) {
                    txtNomeCliente.setText(((PessoaJuridica) c).getRazaoSocial());
                    ftxtCnpjCpf.setText(((PessoaJuridica) c).getCpfCnpj());

                }
                ftxPlaca.requestFocus();

            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM CLIENTE QUE ESTEJA ATIVO");
                txtCodCliente.setText("");
                txtCodCliente.requestFocus();
            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE CLIENTE COM ESTE CÓDIGO");
            txtCodCliente.setText("");
            txtCodCliente.requestFocus();
        }

    }

    public void retModVeic() {
        int codigo = Integer.parseInt(txtModelo.getText());

        if (mDAO.buscaModelo(codigo)) {
            if (mDAO.consultaStatusModVeiculo(codigo)) {
                ModeloVeiculo m;
                m = mDAO.consultaModeloVeic(codigo);

                txtNomeModelo.setText(m.getModelo());
                txtMarca.setText(m.getMarca());
                txtMotor.setText(m.getCor());
                txtCor.setText(m.getMotor());
                txtQtdPortas.setText(Integer.toString(m.getQtdPortas()));
                txtCombustivel.setText(m.getCombustivel());
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM MODELO DE VEÍCULO QUE ESTEJA ATIVO");
                txtCodCliente.setText("");
                txtCodCliente.requestFocus();
            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE MODELO DE VEÍCULO COM ESTE CÓDIGO");
            txtModelo.setText("");
            txtModelo.requestFocus();
        }

    }

    public void preencheCliente() {
        TelaCliente tcli = new TelaCliente((Frame) getParent(), true);
        tcli.setVisible(true);
        tcli.SelecionaPrimeiraLinha();
        try {
            opcao = tcli.opItem;
            codCliente = Integer.parseInt(opcao);
            if (cDAO.consultaStatusCliente(codCliente)) {
                Cliente c;
                c = cDAO.consultaCliente(codCliente);
                if (c instanceof PessoaFisica) {
                    ftxtCnpjCpf.setText(((PessoaFisica) c).getCpfCnpj());
                    txtNomeCliente.setText(((PessoaFisica) c).getNome());

                } else if (c instanceof PessoaJuridica) {
                    txtNomeCliente.setText(((PessoaJuridica) c).getRazaoSocial());
                    ftxtCnpjCpf.setText(((PessoaJuridica) c).getCpfCnpj());

                }

                txtCodCliente.setText(opcao);
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM CLIENTE QUE ESTEJA ATIVO");
                txtCodCliente.setText("");
                txtCodCliente.requestFocus();
            }

        } catch (NullPointerException e) {

        }

    }

    public void preencheModeloVeic() {
        TelaModeloVeiculo tmodv = new TelaModeloVeiculo((Frame) getParent(), true);
        tmodv.setVisible(true);

        try {
            opcMod = tmodv.opItem;
            codModelo = Integer.parseInt(opcMod);
            if (mDAO.consultaStatusModVeiculo(codModelo)) {

                ModeloVeiculo m;
                m = mDAO.consultaModeloVeic(codModelo);

                txtNomeModelo.setText(m.getModelo());
                txtMarca.setText(m.getMarca());
                txtMotor.setText(m.getCor());
                txtCor.setText(m.getMotor());
                txtQtdPortas.setText(Integer.toString(m.getQtdPortas()));
                txtCombustivel.setText(m.getCombustivel());

                txtModelo.setText(opcMod);
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM MODELO DE VEÍCULO QUE ESTEJA ATIVO");
                txtCodCliente.setText("");
                txtCodCliente.requestFocus();
            }

        } catch (NullPointerException e) {

        }

    }

    public void preencheTabelaVeic() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableVeiculo.getModel();
        modeloTab.setNumRows(0);

        for (Veiculo v : vDAO.preencheTabelaVeiculo()) {

            modeloTab.addRow(new Object[]{
                v.getCodigo(),
                v.getPlaca(),
                v.getNomeModelo(),
                v.getCpfCnpjCliente(),
                v.getNomeCliente()
            });

        }

    }

    public void preencheTabelaVeicDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableVeiculo.getModel();
        modeloTab.setNumRows(0);

        for (Veiculo v : vDAO.preencheTabelaVeiculoDesativados()) {

            modeloTab.addRow(new Object[]{
                v.getCodigo(),
                v.getPlaca(),
                v.getNomeModelo(),
                v.getCpfCnpjCliente(),
                v.getNomeCliente()
            });

        }

    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableVeiculo.getModel();
        modeloTab.setNumRows(0);

        for (Veiculo v : vDAO.buscaVeiculoTabelaDesativados(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                v.getCodigo(),
                v.getPlaca(),
                v.getNomeCliente(),
                v.getCpfCnpjCliente(),
                v.getNomeModelo()

            });

        }

    }

    private void cadastroVeic() {
        boolean flag;

        int codCli = Integer.parseInt(txtCodCliente.getText());
        int codMod = Integer.parseInt(txtModelo.getText());
        Veiculo ve = new Veiculo();

        ve.setCod_cliente(codCli);
        ve.setPlaca(masc.removeMascaraPlaca(ftxPlaca.getText()));
        ve.setCod_modveic(codMod);
        ve.setAno(Integer.parseInt(txtAno.getText()));
        ve.setKm(Integer.parseInt(txtKm.getText()));
        ve.setChassi(txtChassi.getText());

        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO VEICULO", "Confirmação", JOptionPane.YES_NO_OPTION);

        if (resp == 0) {
            try {
                flag = vDAO.addVeiculo(ve);
                if (flag == true) {
                    JOptionPane.showMessageDialog(null, "VEICULO CADASTRADO COM SUCESSO");
                    ct.clear(panelCliente);
                    ct.clear(panelModeloveiculo);
                    ct.clear(panelVeic1);
                    ct.campoNaoEditavel(panelCliente);
                    ct.campoNaoEditavel(panelModeloveiculo);
                    ct.campoNaoEditavel(panelVeic1);
                    tableVeiculo.setEnabled(true);
                    tableVeiculo.setVisible(true);
                    preencheTabelaVeic();
                    tableVeiculo.requestFocus();
                    tableVeiculo.changeSelection(0, 0, false, false);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);

                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setVisible(false);
                    btnCancelar.setEnabled(false);
                    btnDesativar.setEnabled(true);
                } else if (flag == false) {

                    btnIncluir.setEnabled(false);
                    btnAlterar.setEnabled(false);
                    btnExcluir.setEnabled(false);
                    
                    btnGravar.setEnabled(true);
                    btnGravar.setVisible(true);
                    btnDesativar.setEnabled(true);
                }
            } catch (Exception ex) {
                Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

            }

        } else if (resp == 1) {
            ct.clear(panelCliente);
            ct.clear(panelModeloveiculo);
            ct.clear(panelVeic1);
            ct.campoNaoEditavel(panelCliente);
            ct.campoNaoEditavel(panelModeloveiculo);
            ct.campoNaoEditavel(panelVeic1);
            tableVeiculo.setEnabled(true);
            tableVeiculo.setVisible(true);
            preencheTabelaVeic();
            tableVeiculo.requestFocus();
            tableVeiculo.changeSelection(0, 0, false, false);
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
           
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setEnabled(false);
            btnCancelar.setVisible(false);
            btnDesativar.setEnabled(true);
        }

    }

    public void desativaBotao() {
        btnIncluir.setEnabled(false);

    }

    public void ativaBotao() {
        btnIncluir.setEnabled(true);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JFormattedTextField ftxPlaca;
    private javax.swing.JFormattedTextField ftxtCnpjCpf;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelCliente;
    private javax.swing.JPanel panelModeloveiculo;
    private javax.swing.JPanel panelPesquisa;
    private javax.swing.JPanel panelVeic1;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tableVeiculo;
    private javax.swing.JTextField txtAno;
    private javax.swing.JTextField txtChassi;
    private javax.swing.JTextField txtCodCliente;
    private javax.swing.JTextField txtCombustivel;
    private javax.swing.JTextField txtCor;
    private javax.swing.JTextField txtKm;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtMotor;
    private javax.swing.JTextField txtNomeCliente;
    private javax.swing.JTextField txtNomeModelo;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JTextField txtQtdPortas;
    // End of variables declaration//GEN-END:variables
}
