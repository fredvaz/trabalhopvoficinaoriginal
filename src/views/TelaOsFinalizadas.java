package views;

import controllers.OrdemServicoDAO;
import java.awt.event.KeyEvent;
import javax.swing.table.DefaultTableModel;
import models.OrdemServico;

public final class TelaOsFinalizadas extends javax.swing.JDialog {

    private final OrdemServicoDAO osDAO = new OrdemServicoDAO();
    
    public TelaOsFinalizadas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        preencheTabela();
        if (!osDAO.VerificaOrdemServico()) {

            tableOsFinalizadas.requestFocus();
            tableOsFinalizadas.changeSelection(0, 0, false, false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableOsFinalizadas = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ordem de Serviço Finalizada");
        setResizable(false);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableOsFinalizadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Cliente", "PLACA", "MODELO", "DATA CONCLUSÃO", "HORÁRIO CONCLUSÃO", "MECÂNICO", "Situação", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableOsFinalizadas);
        if (tableOsFinalizadas.getColumnModel().getColumnCount() > 0) {
            tableOsFinalizadas.getColumnModel().getColumn(0).setPreferredWidth(2);
            tableOsFinalizadas.getColumnModel().getColumn(1).setPreferredWidth(190);
            tableOsFinalizadas.getColumnModel().getColumn(2).setPreferredWidth(8);
            tableOsFinalizadas.getColumnModel().getColumn(3).setPreferredWidth(30);
            tableOsFinalizadas.getColumnModel().getColumn(4).setPreferredWidth(30);
            tableOsFinalizadas.getColumnModel().getColumn(5).setPreferredWidth(50);
            tableOsFinalizadas.getColumnModel().getColumn(6).setPreferredWidth(50);
            tableOsFinalizadas.getColumnModel().getColumn(7).setPreferredWidth(20);
            tableOsFinalizadas.getColumnModel().getColumn(8).setPreferredWidth(10);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            preencheTabela();

        }
    }//GEN-LAST:event_formKeyReleased

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableOsFinalizadas.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaFinalizadas()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getNomeCliente(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getDataConclusaoStr(),
                os.getHorarioSaida(),
                os.getNomeMecanico(),
                os.getStatus(),
               os.getValor_totalStr()

            });

        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableOsFinalizadas;
    // End of variables declaration//GEN-END:variables
}
