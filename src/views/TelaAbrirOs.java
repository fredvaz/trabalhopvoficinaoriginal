package views;

import controllers.LoginDAO;
import java.text.ParseException;
import help.CampoTxt;
import javax.swing.JOptionPane;
import models.OrdemServico;
import controllers.OrdemServicoDAO;
import controllers.ServicoPecaDAO;
import controllers.VeiculoDAO;
import help.ConverteData;
import help.Mascara;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import models.PecaServico;
import models.Veiculo;
import help.DataSistema;
import help.ModeloTabelaOs;
import help.SomenteNumeros;
import java.awt.event.ItemEvent;
import java.text.NumberFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import models.Usuario;

public class TelaAbrirOs extends javax.swing.JDialog {

    private int controlBotao;
    private final CampoTxt ct;
    private final OrdemServicoDAO osDAO;
    private final TelaPrincipal tp;
    private MaskFormatter placaMask;
    private final ConverteData conv;
    private String opcVeic;
    private String opcMec;
    private String opcPs;
    private int codCliente;
    private int codUsuario;
    private int codPecaServico;
    private int codigoVeiculo;
    private int codigoOrdemServico;
    private int horaPrevista;
    private int minutoPrevisto;
    private String dataPrevista;
    private String dataAtual;
    String horaAtual;
    String minutoAtual;
    private LocalDate dataAtualCad;
    private int horaAtualCadastrada;
    private int minutoAtualCadastrado;
    private final ArrayList<PecaServico> listBancoPecaServico;
    private final ArrayList<PecaServico> listaBanco;
    String opItem;
    private final Mascara masc;
    private final DateTimeFormatter formatador;
    private final DataSistema dtSis;
    private final ModeloTabelaOs osTableMod = new ModeloTabelaOs();
    private double total;
    private String txtTotalStr;
    Locale localeBR = new Locale("pt", "BR");
    NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);

    public TelaAbrirOs(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);

        osDAO = new OrdemServicoDAO();
        masc = new Mascara();
        controlBotao = 0;
        ct = new CampoTxt();
        conv = new ConverteData();
        codCliente = 0;
        codigoVeiculo = 0;
        codUsuario = 0;
        codPecaServico = 0;

        total = 0;
        opItem = "";
        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {

            placaMask = new MaskFormatter("???-####");
        } catch (ParseException ex) {
            Logger.getLogger(TelaAbrirOs.class.getName()).log(Level.SEVERE, null, ex);
        }

        tablePecaServicoOs.setModel(osTableMod);

        listBancoPecaServico = new ArrayList<>();
        listaBanco = new ArrayList<>();
        dtSis = new DataSistema();
        txtps.setDocument(new SomenteNumeros());
        tp = new TelaPrincipal();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelPrincipal = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNomeCliente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jfplaca = new javax.swing.JFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        txtTelefone = new javax.swing.JTextField();
        txtCpfCnpj = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        paneLScroolArea = new javax.swing.JScrollPane();
        tablePecaServicoOs = new javax.swing.JTable();
        panelDescricaoProblema = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtAreaProblema = new javax.swing.JTextArea();
        panelPecaServico = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtps = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtQtd = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDescricaoPecServ = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtValorUn = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        btnEditar = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();
        panelHoraPrevista = new javax.swing.JPanel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        jcomboHora = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        JcomboMinuto = new javax.swing.JComboBox<>();
        panelMecanico = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtCodMec = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtMecanico = new javax.swing.JTextField();
        panelModVeic = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNomeModelo = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtCor = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        panelHoraAtual = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jfDataInclusao = new javax.swing.JFormattedTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jComboBoxHoraAtual = new javax.swing.JComboBox<>();
        jComboBoxMinutoAtual = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Os");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        panelPrincipal.setBackground(new java.awt.Color(204, 204, 204));
        panelPrincipal.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("Veiculo*");

        txtNomeCliente.setEditable(false);

        jLabel5.setText("Nome/RazãoSocial Cliente");

        jfplaca.setEditable(false);
        jfplaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jfplacaKeyPressed(evt);
            }
        });

        jLabel12.setText("Telefone Cliente");

        txtTelefone.setEditable(false);

        txtCpfCnpj.setEditable(false);

        jLabel18.setText("CPF/CNPJ");

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jfplaca, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(txtCpfCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(txtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addGap(7, 7, 7)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jfplaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCpfCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        paneLScroolArea.setBackground(new java.awt.Color(204, 204, 204));
        paneLScroolArea.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablePecaServicoOs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        paneLScroolArea.setViewportView(tablePecaServicoOs);

        panelDescricaoProblema.setBackground(new java.awt.Color(204, 204, 204));
        panelDescricaoProblema.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setText("Descrição do Problema*");

        txtAreaProblema.setEditable(false);
        txtAreaProblema.setColumns(20);
        txtAreaProblema.setRows(5);
        txtAreaProblema.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAreaProblemaKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(txtAreaProblema);

        javax.swing.GroupLayout panelDescricaoProblemaLayout = new javax.swing.GroupLayout(panelDescricaoProblema);
        panelDescricaoProblema.setLayout(panelDescricaoProblemaLayout);
        panelDescricaoProblemaLayout.setHorizontalGroup(
            panelDescricaoProblemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDescricaoProblemaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelDescricaoProblemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                    .addComponent(jLabel7))
                .addGap(10, 10, 10))
        );
        panelDescricaoProblemaLayout.setVerticalGroup(
            panelDescricaoProblemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDescricaoProblemaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelPecaServico.setBackground(new java.awt.Color(204, 204, 204));
        panelPecaServico.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Codigo:*");

        txtps.setEditable(false);
        txtps.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpsKeyPressed(evt);
            }
        });

        jLabel8.setText("Quantidade:*");

        txtQtd.setEditable(false);
        txtQtd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQtdKeyPressed(evt);
            }
        });

        jLabel19.setText("Descrição Peça/Serviço");

        txtDescricaoPecServ.setEditable(false);

        jLabel20.setText("Valor Un");

        txtValorUn.setEditable(false);

        txtTotal.setEditable(false);

        jLabel22.setText("Total");

        btnEditar.setBackground(new java.awt.Color(204, 204, 204));
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnRemover.setBackground(new java.awt.Color(204, 204, 204));
        btnRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnRemover.setText("Remover");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPecaServicoLayout = new javax.swing.GroupLayout(panelPecaServico);
        panelPecaServico.setLayout(panelPecaServicoLayout);
        panelPecaServicoLayout.setHorizontalGroup(
            panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPecaServicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPecaServicoLayout.createSequentialGroup()
                        .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(txtQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(txtValorUn, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(txtTotal)))
                    .addGroup(panelPecaServicoLayout.createSequentialGroup()
                        .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(btnRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPecaServicoLayout.createSequentialGroup()
                        .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtps, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(10, 10, 10)
                        .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(txtDescricaoPecServ))))
                .addContainerGap())
        );
        panelPecaServicoLayout.setVerticalGroup(
            panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPecaServicoLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtps, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescricaoPecServ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel22)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQtd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValorUn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditar)
                    .addComponent(btnRemover))
                .addGap(10, 10, 10))
        );

        panelHoraPrevista.setBackground(new java.awt.Color(204, 204, 204));
        panelHoraPrevista.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jDateChooser1.setBackground(new java.awt.Color(204, 204, 204));
        jDateChooser1.setEnabled(false);
        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser1PropertyChange(evt);
            }
        });
        jDateChooser1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jDateChooser1KeyPressed(evt);
            }
        });

        jLabel6.setText("Previsão Entrega*");

        jcomboHora.setBackground(new java.awt.Color(204, 204, 204));
        jcomboHora.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jcomboHora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" }));
        jcomboHora.setEnabled(false);
        jcomboHora.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcomboHoraItemStateChanged(evt);
            }
        });
        jcomboHora.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jcomboHoraKeyPressed(evt);
            }
        });

        jLabel13.setText("Hora*");

        jLabel14.setText("Minuto*");

        JcomboMinuto.setBackground(new java.awt.Color(204, 204, 204));
        JcomboMinuto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        JcomboMinuto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "00", "30" }));
        JcomboMinuto.setEnabled(false);
        JcomboMinuto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                JcomboMinutoItemStateChanged(evt);
            }
        });
        JcomboMinuto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JcomboMinutoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelHoraPrevistaLayout = new javax.swing.GroupLayout(panelHoraPrevista);
        panelHoraPrevista.setLayout(panelHoraPrevistaLayout);
        panelHoraPrevistaLayout.setHorizontalGroup(
            panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHoraPrevistaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jcomboHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JcomboMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(10, 10, 10))
        );
        panelHoraPrevistaLayout.setVerticalGroup(
            panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHoraPrevistaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelHoraPrevistaLayout.createSequentialGroup()
                        .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel14))
                        .addGap(6, 6, 6)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHoraPrevistaLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelHoraPrevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jcomboHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JcomboMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(10, 10, 10))
        );

        panelMecanico.setBackground(new java.awt.Color(204, 204, 204));
        panelMecanico.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Cód.Mec*");

        txtCodMec.setEditable(false);
        txtCodMec.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodMecKeyPressed(evt);
            }
        });

        jLabel4.setText("Nome Mecânico Responsável");

        txtMecanico.setEditable(false);

        javax.swing.GroupLayout panelMecanicoLayout = new javax.swing.GroupLayout(panelMecanico);
        panelMecanico.setLayout(panelMecanicoLayout);
        panelMecanicoLayout.setHorizontalGroup(
            panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMecanicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodMec, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(panelMecanicoLayout.createSequentialGroup()
                        .addComponent(txtMecanico)
                        .addContainerGap())))
        );
        panelMecanicoLayout.setVerticalGroup(
            panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMecanicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel4))
                .addGap(2, 2, 2)
                .addGroup(panelMecanicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodMec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMecanico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        panelModVeic.setBackground(new java.awt.Color(204, 204, 204));
        panelModVeic.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Modelo Veiculo");

        txtNomeModelo.setEditable(false);

        jLabel9.setText("Cor");

        txtCor.setEditable(false);

        jLabel11.setText("Marca");

        txtMarca.setEditable(false);

        javax.swing.GroupLayout panelModVeicLayout = new javax.swing.GroupLayout(panelModVeic);
        panelModVeic.setLayout(panelModVeicLayout);
        panelModVeicLayout.setHorizontalGroup(
            panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModVeicLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtNomeModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(txtMarca, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(txtCor, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        panelModVeicLayout.setVerticalGroup(
            panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModVeicLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel11)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelModVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomeModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelHoraAtual.setBackground(new java.awt.Color(204, 204, 204));
        panelHoraAtual.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel15.setText("Data Inclusão");

        jfDataInclusao.setEditable(false);

        jLabel16.setText("Hora");

        jLabel17.setText("Minuto");

        jComboBoxHoraAtual.setBackground(new java.awt.Color(204, 204, 204));
        jComboBoxHoraAtual.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jComboBoxHoraAtual.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "00" }));
        jComboBoxHoraAtual.setEnabled(false);

        jComboBoxMinutoAtual.setBackground(new java.awt.Color(204, 204, 204));
        jComboBoxMinutoAtual.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jComboBoxMinutoAtual.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "00" }));
        jComboBoxMinutoAtual.setEnabled(false);

        javax.swing.GroupLayout panelHoraAtualLayout = new javax.swing.GroupLayout(panelHoraAtual);
        panelHoraAtual.setLayout(panelHoraAtualLayout);
        panelHoraAtualLayout.setHorizontalGroup(
            panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHoraAtualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jfDataInclusao, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxHoraAtual, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addGap(8, 8, 8)
                .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxMinutoAtual, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(10, 10, 10))
        );
        panelHoraAtualLayout.setVerticalGroup(
            panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHoraAtualLayout.createSequentialGroup()
                .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelHoraAtualLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelHoraAtualLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel16)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelHoraAtualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jfDataInclusao, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxHoraAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxMinutoAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_16x16.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(454, 454, 454)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(panelDescricaoProblema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelPecaServico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelModVeic, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelMecanico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelHoraAtual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelHoraPrevista, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(paneLScroolArea))
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelHoraAtual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelModVeic, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelMecanico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelHoraPrevista, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelDescricaoProblema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPecaServico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(paneLScroolArea, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        tp.atualizaInformativo();
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        controlBotao = 1;

        ct.clear(panelPrincipal);
        camposEditaveis();
        ct.clear(panelDescricaoProblema);
        ct.campoEditavel(panelDescricaoProblema);
        ct.clear(panelPecaServico);

        btnIncluir.setEnabled(false);

        btnGravar.setEnabled(true);
        btnGravar.setVisible(true);
        btnCancelar.setEnabled(true);
        btnCancelar.setVisible(true);

        jcomboHora.setEnabled(true);
        JcomboMinuto.setEnabled(true);
        txtAreaProblema.setEnabled(true);
        txtAreaProblema.setEditable(true);

        jfplaca.setValue(null);
        jfplaca.setFormatterFactory(new DefaultFormatterFactory(placaMask));
        jfplaca.requestFocus();

        if (dtSis.horaSis() < 10) {
            horaAtual = new StringBuilder().append(0).append(Integer.toString(dtSis.horaSis())).toString();
        } else {
            horaAtual = Integer.toString(dtSis.horaSis());
        }
        if (dtSis.minutoSis() < 10) {
            minutoAtual = new StringBuilder().append(0).append(Integer.toString(dtSis.minutoSis())).toString();
        } else {
            minutoAtual = Integer.toString(dtSis.minutoSis());
        }

        jComboBoxHoraAtual.addItem(horaAtual);
        jComboBoxHoraAtual.setSelectedIndex(2);
        jComboBoxMinutoAtual.addItem(minutoAtual);
        jComboBoxMinutoAtual.setSelectedIndex(2);
        dataAtual = dtSis.dataSis();
        jfDataInclusao.setText(dataAtual);

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:

                if (jfplaca.getText().trim().isEmpty()
                        || txtCodMec.getText().trim().isEmpty()
                        || txtAreaProblema.getText().trim().isEmpty()
                        || jDateChooser1.getDate() == null
                        || jcomboHora.getSelectedIndex() == 0
                        || JcomboMinuto.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    jfplaca.requestFocus();

                } else {
                    cadastroOrdemServico();

                }

                break;

            case 2:
                alterarOs();
                break;

        }
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        if (controlBotao == 2) {

            btnIncluir.setEnabled(true);

            desativaComboBox();
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setVisible(false);
            btnCancelar.setEnabled(false);
            jfDataInclusao.setText("");
            jDateChooser1.setEnabled(false);
            jDateChooser1.setDate(null);
            ct.clear(panelDescricaoProblema);
            ct.clear(panelPecaServico);
            ct.clear(panelPrincipal);
            ct.clear(panelModVeic);
            ct.clear(panelMecanico);
            try {
                osTableMod.clearTable();
            } catch (IndexOutOfBoundsException e) {

            }

            txtAreaProblema.setText("");
            jfplaca.setValue(null);
            ct.campoNaoEditavel(panelPrincipal);

            ct.campoNaoEditavel(panelPecaServico);
            ct.campoNaoEditavel(panelDescricaoProblema);
            ct.campoNaoEditavel(panelMecanico);

            this.dispose();
        }
        btnIncluir.setEnabled(true);

        desativaComboBox();
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        jfDataInclusao.setText("");
        jDateChooser1.setEnabled(false);
        jDateChooser1.setDate(null);
        ct.clear(panelDescricaoProblema);
        ct.clear(panelPecaServico);
        ct.clear(panelPrincipal);
        ct.clear(panelModVeic);
        ct.clear(panelMecanico);
        try {
            osTableMod.clearTable();
        } catch (IndexOutOfBoundsException e) {

        }

        txtAreaProblema.setText("");
        jfplaca.setValue(null);
        ct.campoNaoEditavel(panelPrincipal);

        ct.campoNaoEditavel(panelPecaServico);
        ct.campoNaoEditavel(panelDescricaoProblema);
        ct.campoNaoEditavel(panelMecanico);

        btnIncluir.requestFocus();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtCodMecKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodMecKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (controlBotao == 1) {

                if (txtCodMec.getText().equals("")) {

                    preencheMecanico();
                } else {
                    retMecanico();

                }

                if (!txtCodMec.getText().equals("")) {
                    jDateChooser1.getDateEditor().getUiComponent().requestFocus();

                }

            } else if (controlBotao == 2) {
                if (txtCodMec.getText().equals("")) {

                    preencheMecanico();
                } else {
                    retMecanico();

                }

                if (!txtCodMec.getText().equals("")) {
                    jDateChooser1.getDateEditor().getUiComponent().requestFocus();
                }

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheMecanico();
        }
    }//GEN-LAST:event_txtCodMecKeyPressed

    private void txtAreaProblemaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAreaProblemaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtps.requestFocus();
        }
    }//GEN-LAST:event_txtAreaProblemaKeyPressed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            switch (controlBotao) {
                case 1:

                    if (jfplaca.getText().trim().isEmpty()
                            || txtCodMec.getText().trim().isEmpty()
                            || txtAreaProblema.getText().trim().isEmpty()
                            || jDateChooser1.getDate() == null
                            || jcomboHora.getSelectedIndex() == 0
                            || JcomboMinuto.getSelectedIndex() == 0) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                        jfplaca.requestFocus();

                    } else {
                        cadastroOrdemServico();

                    }

                    break;

                case 2:
                    alterarOs();
                    break;

            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void txtpsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (controlBotao == 1) {

                if (txtps.getText().trim().equals("")) {

                    preenchePecaServico();

                } else {
                    retPecaServico();

                }

                if (!txtps.getText().trim().equals("")) {
                    txtQtd.requestFocus();

                }

            } else if (controlBotao == 2) {
                if (txtps.getText().trim().equals("")) {

                    preenchePecaServico();
                } else {
                    retPecaServico();

                }

                if (!txtps.getText().trim().equals("")) {
                    txtQtd.requestFocus();

                }

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preenchePecaServico();
        }
    }//GEN-LAST:event_txtpsKeyPressed

    private void jDateChooser1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser1PropertyChange

        if (!(jDateChooser1.getDate() == null)) {
            if (controlBotao == 1) {
                LocalDate dtAtual;
                LocalDate dtPrevista;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String date = sdf.format(jDateChooser1.getDate());
                dataPrevista = date;
                dtAtual = conv.converterStringtoLocalDate(dataAtual);
                dtPrevista = conv.converterStringtoLocalDate(dataPrevista);

                if (dtPrevista.isBefore(dtAtual)) {
                    JOptionPane.showMessageDialog(null, "DATA INFORMADA É ANTERIOR A DATA DE INCLUSÃO.");

                    jDateChooser1.setDate(null);
                    jDateChooser1.getDateEditor().getUiComponent().requestFocus();

                } else {
                    jcomboHora.setSelectedIndex(0);
                    jcomboHora.requestFocus();
                }

            } else if (controlBotao == 2) {
                LocalDate dtPrevista;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String date = sdf.format(jDateChooser1.getDate());

                dataPrevista = date;
                dtPrevista = conv.converterStringtoLocalDate(dataPrevista);

                if (dtPrevista.isBefore(dataAtualCad)) {
                    JOptionPane.showMessageDialog(null, "DATA INFORMADA É ANTERIOR A DATA DE INCLUSÃO.");

                    jDateChooser1.setDate(null);
                    jDateChooser1.getDateEditor().getUiComponent().requestFocus();

                } else {
                    jcomboHora.setSelectedIndex(0);
                    jcomboHora.requestFocus();
                }

            }

        } else {
            jDateChooser1.getDateEditor().getUiComponent().requestFocus();
        }


    }//GEN-LAST:event_jDateChooser1PropertyChange

    private void jDateChooser1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jDateChooser1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (jDateChooser1.getDateEditor() != null) {

                txtAreaProblema.requestFocus();
            }
        }
    }//GEN-LAST:event_jDateChooser1KeyPressed

    private void jfplacaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jfplacaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (controlBotao == 1) {

                if (masc.removeMascaraPlaca(jfplaca.getText()).trim().equals("")) {

                    preencheVeic();
                } else {
                    retVeiculo();

                }

                if (!masc.removeMascaraPlaca(jfplaca.getText()).trim().equals("")) {

                    txtCodMec.requestFocus();

                }

            } else if (controlBotao == 2) {
                if (masc.removeMascaraPlaca(jfplaca.getText()).trim().equals("")) {

                    preencheVeic();
                } else {
                    retVeiculo();

                }

                if (!masc.removeMascaraPlaca(jfplaca.getText()).trim().equals("")) {
                    txtCodMec.requestFocus();
                }

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheVeic();
        }
    }//GEN-LAST:event_jfplacaKeyPressed

    private void txtQtdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtdKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (!txtQtd.getText().trim().equals("")) {

                if (!txtps.isEditable()) {

                    txtps.setEditable(true);
                    alterarQuantidade();
                    txtQtd.setText(null);
                    txtps.setText(null);
                    txtps.requestFocus();
                } else {
                    txtps.requestFocus();
                    preencheJtablePecaServico();
                    txtps.setText("");
                    txtDescricaoPecServ.setText("");
                    txtValorUn.setText("");
                    txtQtd.setText("");
                }

            } else {
                JOptionPane.showMessageDialog(null, "INFORME A QUANTIDADE");
                txtQtd.requestFocus();
            }

        }
    }//GEN-LAST:event_txtQtdKeyPressed

    private void JcomboMinutoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JcomboMinutoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!(jDateChooser1.getDate() == null)) {

                if (!(jcomboHora.getSelectedIndex() == 0)) {
                    if (controlBotao == 1) {
                        if (JcomboMinuto.getSelectedIndex() != 0) {
                            LocalDate dtAtual;
                            LocalDate dtPrevista;

                            dtAtual = conv.converterStringtoLocalDate(dataAtual);
                            dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                            int horaAt = dtSis.horaSis();
                            int horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                            int minAt = dtSis.minutoSis();
                            int minPrev = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

                            if (dtPrevista.isEqual(dtAtual)) {

                                if (horaPrev == horaAt) {

                                    if (minPrev < minAt) {

                                        JOptionPane.showMessageDialog(null, "MINUTO INFORMADO É ANTERIOR A MINUTO ATUAL");
                                        JcomboMinuto.setSelectedIndex(0);
                                        JcomboMinuto.requestFocus();
                                    } else {
                                        minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                        txtAreaProblema.requestFocus();
                                    }

                                } else {
                                    minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                    txtAreaProblema.requestFocus();

                                }
                            } else {
                                minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                txtAreaProblema.requestFocus();

                            }
                        }
                    } else if (controlBotao == 2) {

                        if (JcomboMinuto.getSelectedIndex() != 0) {

                            LocalDate dtPrevista;

                            dtPrevista = conv.converterStringtoLocalDate(dataPrevista);

                            int horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());

                            int minPrev = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

                            if (dtPrevista.isEqual(dataAtualCad)) {

                                if (horaPrev == horaAtualCadastrada) {

                                    if (minPrev < minutoAtualCadastrado) {

                                        JOptionPane.showMessageDialog(null, "MINUTO INFORMADO É ANTERIOR A MINUTO ATUAL");
                                        JcomboMinuto.setSelectedIndex(0);
                                        JcomboMinuto.requestFocus();
                                    } else {
                                        minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                        txtAreaProblema.requestFocus();
                                    }

                                } else {
                                    minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                    txtAreaProblema.requestFocus();

                                }
                            } else {
                                minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                txtAreaProblema.requestFocus();

                            }
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "INFORME A HORA PRIMEIRO");
                }

            } else {
                JOptionPane.showMessageDialog(null, "INFORME A DATA PRIMEIRO");
            }

        }
    }//GEN-LAST:event_JcomboMinutoKeyPressed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        removerItem();
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        txtQtd.requestFocus();
        alterar();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void jcomboHoraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jcomboHoraKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!(jDateChooser1.getDate() == null)) {
                LocalDate dtAtual = null;
                LocalDate dtPrevista;
                int horaPrev;

                if (controlBotao == 1) {
                    dtAtual = conv.converterStringtoLocalDate(dataAtual);

                    int horaAt = dtSis.horaSis();
                    if (jcomboHora.getSelectedIndex() != 0) {
                        dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                        horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                        if (dtPrevista.isEqual(dtAtual)) {

                            if (horaPrev < horaAt) {

                                JOptionPane.showMessageDialog(null, "HORA INFORMADA É ANTERIOR A HORA ATUAL");
                                jcomboHora.setSelectedIndex(0);
                                jcomboHora.requestFocus();
                            } else {
                                horaPrevista = jcomboHora.getSelectedIndex();
                                JcomboMinuto.requestFocus();
                            }

                        } else {
                            horaPrevista = jcomboHora.getSelectedIndex();
                            JcomboMinuto.requestFocus();

                        }
                    }
                } else if (controlBotao == 2) {

                    if (jcomboHora.getSelectedIndex() != 0) {
                        dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                        horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                        if (dtPrevista.isEqual(dataAtualCad)) {

                            if (horaPrev < horaAtualCadastrada) {

                                JOptionPane.showMessageDialog(null, "HORA INFORMADA É ANTERIOR A HORA ATUAL");
                                jcomboHora.setSelectedIndex(0);
                                jcomboHora.requestFocus();
                            } else {
                                horaPrevista = jcomboHora.getSelectedIndex();
                                JcomboMinuto.requestFocus();
                            }

                        } else {
                            horaPrevista = jcomboHora.getSelectedIndex();
                            JcomboMinuto.requestFocus();

                        }
                    }

                }
            } else {
                JOptionPane.showMessageDialog(null, "INFORME A DATA PRIMEIRO");
            }

        }
    }//GEN-LAST:event_jcomboHoraKeyPressed

    private void JcomboMinutoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_JcomboMinutoItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if (!(JcomboMinuto.getSelectedIndex() == 0)) {
                if (!(jcomboHora.getSelectedIndex() == 0)) {

                    if (!(jDateChooser1.getDate() == null)) {

                        if (controlBotao == 1) {
                            if (JcomboMinuto.getSelectedIndex() != 0) {
                                LocalDate dtAtual;
                                LocalDate dtPrevista;

                                dtAtual = conv.converterStringtoLocalDate(dataAtual);
                                dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                                int horaAt = dtSis.horaSis();
                                int horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                                int minAt = dtSis.minutoSis();
                                int minPrev = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

                                if (dtPrevista.isEqual(dtAtual)) {

                                    if (horaPrev == horaAt) {

                                        if (minPrev < minAt) {

                                            JOptionPane.showMessageDialog(null, "MINUTO INFORMADO É ANTERIOR A MINUTO ATUAL");
                                            JcomboMinuto.setSelectedIndex(0);
                                            JcomboMinuto.requestFocus();
                                        } else {
                                            minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                            txtAreaProblema.requestFocus();
                                        }

                                    } else {
                                        minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                        txtAreaProblema.requestFocus();

                                    }
                                } else {
                                    minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                    txtAreaProblema.requestFocus();

                                }
                            }
                        } else if (controlBotao == 2) {

                            if (JcomboMinuto.getSelectedIndex() != 0) {

                                LocalDate dtPrevista;

                                dtPrevista = conv.converterStringtoLocalDate(dataPrevista);

                                int horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());

                                int minPrev = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

                                if (dtPrevista.isEqual(dataAtualCad)) {

                                    if (horaPrev == horaAtualCadastrada) {

                                        if (minPrev < minutoAtualCadastrado) {

                                            JOptionPane.showMessageDialog(null, "MINUTO INFORMADO É ANTERIOR A MINUTO ATUAL");
                                            JcomboMinuto.setSelectedIndex(0);
                                            JcomboMinuto.requestFocus();
                                        } else {
                                            minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                            txtAreaProblema.requestFocus();
                                        }

                                    } else {
                                        minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                        txtAreaProblema.requestFocus();

                                    }
                                } else {
                                    minutoPrevisto = JcomboMinuto.getSelectedIndex();
                                    txtAreaProblema.requestFocus();

                                }
                            }
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "INFORME A DATA PRIMEIRO");
                        JcomboMinuto.setSelectedIndex(0);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "INFORME A HORA PRIMEIRO");
                    JcomboMinuto.setSelectedIndex(0);
                }

            }

        }
    }//GEN-LAST:event_JcomboMinutoItemStateChanged

    private void jcomboHoraItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcomboHoraItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if (!(jcomboHora.getSelectedIndex() == 0)) {
                if (!(jDateChooser1.getDate() == null)) {

                    LocalDate dtAtual = null;
                    LocalDate dtPrevista;
                    int horaPrev;

                    if (controlBotao == 1) {
                        dtAtual = conv.converterStringtoLocalDate(dataAtual);

                        int horaAt = dtSis.horaSis();
                        if (jcomboHora.getSelectedIndex() != 0) {
                            dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                            horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                            if (dtPrevista.isEqual(dtAtual)) {

                                if (horaPrev < horaAt) {

                                    JOptionPane.showMessageDialog(null, "HORA INFORMADA É ANTERIOR A HORA ATUAL");
                                    jcomboHora.setSelectedIndex(0);
                                    jcomboHora.requestFocus();
                                } else {
                                    horaPrevista = jcomboHora.getSelectedIndex();
                                    JcomboMinuto.requestFocus();
                                }

                            } else {
                                horaPrevista = jcomboHora.getSelectedIndex();
                                JcomboMinuto.requestFocus();

                            }
                        }
                    } else if (controlBotao == 2) {

                        if (jcomboHora.getSelectedIndex() != 0) {
                            dtPrevista = conv.converterStringtoLocalDate(dataPrevista);
                            horaPrev = Integer.parseInt(jcomboHora.getSelectedItem().toString());
                            if (dtPrevista.isEqual(dataAtualCad)) {

                                if (horaPrev < horaAtualCadastrada) {

                                    JOptionPane.showMessageDialog(null, "HORA INFORMADA É ANTERIOR A HORA ATUAL");
                                    jcomboHora.setSelectedIndex(0);
                                    jcomboHora.requestFocus();
                                } else {
                                    horaPrevista = jcomboHora.getSelectedIndex();
                                    JcomboMinuto.requestFocus();
                                }

                            } else {
                                horaPrevista = jcomboHora.getSelectedIndex();
                                JcomboMinuto.requestFocus();

                            }
                        }

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "INFORME A DATA PRIMEIRO");
                    jcomboHora.setSelectedIndex(0);
                }
            }
        }


    }//GEN-LAST:event_jcomboHoraItemStateChanged

    private void cadastroOrdemServico() {
        boolean flag;
        Locale localeBR = new Locale("pt", "BR");
      
        LocalDate dataAt = conv.converterStringtoLocalDate(dataAtual);
        LocalDate dataPrv = conv.converterStringtoLocalDate(dataPrevista);
        int hAt = Integer.parseInt(horaAtual);
        int mAt = Integer.parseInt(minutoAtual);
        LocalTime horarioAtual = LocalTime.of(hAt, mAt);
        horaPrevista = Integer.parseInt(jcomboHora.getSelectedItem().toString());
        minutoPrevisto = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

        LocalTime horarioPrevisto = LocalTime.of(horaPrevista, minutoPrevisto);

        OrdemServico os = new OrdemServico();

        os.setCod_cliente(codCliente);
        os.setCod_veiculo(codigoVeiculo);
        os.setCod_mecanico(codUsuario);
        os.setPecaServicos(osTableMod.getList());
        os.setDescProblema(txtAreaProblema.getText());
        os.setHorarioEntrada(horarioAtual);
        os.setHorarioPrevisto(horarioPrevisto);
        os.setDataPrevista(dataPrv);
        os.setDataEntrada(dataAt);
        os.setStatus("Aberta");
        os.setValor_total(Double.parseDouble(txtTotalStr));
        //txtTotalStr = String.valueOf(totalForm);
       // txtTotal.setText(dinheiroBR.format(total));
        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DA ORDEM DE SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);

        if (resp == 0) {
            try {

                flag = osDAO.addOrdemServico(os, listaBanco);
                if (flag == true) {
                    JOptionPane.showMessageDialog(null, "ORDEM DE SERVIÇO CADASTRADA COM SUCESSO");
                    tp.atualizaInformativo();
                    ct.clear(panelDescricaoProblema);
                    ct.clear(panelPecaServico);
                    ct.clear(panelPrincipal);
                    ct.clear(panelModVeic);
                    ct.clear(panelMecanico);
                    txtAreaProblema.setText("");
                    jfplaca.setValue(null);
                    ct.campoNaoEditavel(panelPrincipal);
                    ct.campoNaoEditavel(panelPecaServico);
                    ct.campoNaoEditavel(panelDescricaoProblema);
                    ct.campoNaoEditavel(panelMecanico);
                    txtAreaProblema.setText("");
                    osTableMod.clearTable();
                    txtAreaProblema.setEditable(false);
                    jfDataInclusao.setText("");
                    jDateChooser1.setEnabled(false);
                    jDateChooser1.setDate(null);
                    btnIncluir.setEnabled(true);
                    desativaComboBox();
                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setVisible(false);
                    btnCancelar.setEnabled(false);
                } else if (flag == false) {

                    btnIncluir.setEnabled(false);

                    btnGravar.setEnabled(true);
                    btnGravar.setVisible(true);

                }
            } catch (Exception ex) {
                Logger.getLogger(TelaCliente.class
                        .getName()).log(Level.SEVERE, null, ex);

            }

        } else if (resp == 1) {
            ct.clear(panelPecaServico);
            ct.clear(panelPrincipal);
            ct.clear(panelDescricaoProblema);
            ct.campoNaoEditavel(panelPecaServico);
            ct.campoNaoEditavel(panelPrincipal);
            ct.campoNaoEditavel(panelDescricaoProblema);
            paneLScroolArea.setEnabled(true);
            paneLScroolArea.setVisible(true);

            paneLScroolArea.requestFocus();
            osTableMod.clearTable();
            txtAreaProblema.setText("");
            btnIncluir.setEnabled(true);

            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setEnabled(false);
            btnCancelar.setVisible(false);
        }

    }

    public void preencheVeic() {

        TelaVeiculos tveic = new TelaVeiculos((Frame) getParent(), true);
        tveic.setVisible(true);

        try {
            opcVeic = tveic.opItem;

            VeiculoDAO vDAO = new VeiculoDAO();
            Veiculo v;
            v = vDAO.consultaVeiculoPlaca(opcVeic);
            if (vDAO.consultaStatusVeiculo(v.getCodigo())) {
                txtNomeCliente.setText(v.getNomeCliente());
                txtCpfCnpj.setText(v.getCpfCnpjCliente());
                txtNomeModelo.setText(v.getNomeModelo());
                txtCor.setText(v.getCorModelo());
                txtMarca.setText(v.getMarcaModelo());
                txtTelefone.setText(v.getTelefoneCliente());
                jfplaca.setText(opcVeic.toUpperCase());
                codigoVeiculo = v.getCodigo();
                codCliente = v.getCod_cliente();
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM VEÍCULO QUE ESTEJA ATIVO");
                jfplaca.setText("");
                jfplaca.requestFocus();
            }

        } catch (NullPointerException e) {

        }

    }

    public void preencheMecanico() {
        TelaUsuário tmec = new TelaUsuário((Frame) getParent(), true);
        tmec.setVisible(true);

        try {
            opcMec = tmec.opItem;
            codUsuario = Integer.parseInt(opcMec);
            LoginDAO logDAO = new LoginDAO();
            Usuario user;
            user = logDAO.consultaUsuario(codUsuario);
            if (!((user.getGrupo().equals("Mecânico")) && (user.getStatusUsuario().equals("ativado")))) {
                JOptionPane.showMessageDialog(null, "INFORME UM USUÁRIO DO GRUPO MECÂNICO QUE ESTEJA ATIVO");
                txtCodMec.setText("");
                txtCodMec.requestFocus();
            } else {
                txtMecanico.setText(user.getNome());

                txtCodMec.setText(opcMec);

            }

        } catch (NumberFormatException e) {

        }

    }

    public void preenchePecaServico() {
        TelaPecaServico tps = new TelaPecaServico((Frame) getParent(), true);
        tps.setVisible(true);

        try {
            opcPs = tps.opItem;
            codPecaServico = Integer.parseInt(opcPs);
            ServicoPecaDAO psDAO = new ServicoPecaDAO();

            PecaServico ps;
            ps = psDAO.consultaPecaServico(codPecaServico);
            if (psDAO.consultaStatusPecaServico(codPecaServico)) {
                txtDescricaoPecServ.setText(ps.getDescricao());
                txtValorUn.setText(Double.toString(ps.getValor()));
                txtps.setText(opcPs);
            } else {
                if (ps.getTipo() == 1) {
                    JOptionPane.showMessageDialog(null, "ESCOLHA UMA PEÇA QUE ESTEJA ATIVA");
                    txtps.setText("");
                    txtps.requestFocus();
                } else if (ps.getTipo() == 2) {
                    JOptionPane.showMessageDialog(null, "ESCOLHA UMA SERVIÇO QUE ESTEJA ATIVO");
                    txtps.setText("");
                    txtps.requestFocus();
                }
            }

        } catch (NumberFormatException e) {

        }

    }

    public void retVeiculo() {
        String placaVeic = masc.removeMascaraPlaca(jfplaca.getText());
        VeiculoDAO vDAO = new VeiculoDAO();

        if (vDAO.buscaVeiculoPlaca(placaVeic)) {
            Veiculo v;
            v = vDAO.consultaVeiculoPlaca(placaVeic);
            if (vDAO.consultaStatusVeiculo(v.getCodigo())) {
                txtNomeCliente.setText(v.getNomeCliente());
                txtCpfCnpj.setText(v.getCpfCnpjCliente());
                txtNomeModelo.setText(v.getNomeModelo());
                txtCor.setText(v.getCorModelo());
                txtMarca.setText(v.getMarcaModelo());
                txtTelefone.setText(v.getTelefoneCliente());
                jfplaca.setText(placaVeic.toUpperCase());
                codigoVeiculo = v.getCodigo();
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM VEÍCULO QUE ESTEJA ATIVO");
                jfplaca.setText("");
                jfplaca.requestFocus();
            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE VEICULO COM ESTE CÓDIGO");
            jfplaca.setText("");
            jfplaca.requestFocus();
        }

    }

    public void retMecanico() {
        int codigo = Integer.parseInt(txtCodMec.getText());
        LoginDAO logDAO = new LoginDAO();
        if (logDAO.buscaUsuario(codigo)) {

            Usuario user;
            user = logDAO.consultaUsuario(codigo);

            if (!((user.getGrupo().equals("Mecânico")) && (user.getStatusUsuario().equals("ativado")))) {
                JOptionPane.showMessageDialog(null, "INFORME UM USUÁRIO DO GRUPO MECÂNICO QUE ESTEJA ATIVO");
                txtCodMec.setText("");
                txtCodMec.requestFocus();
            } else {
                codUsuario = codigo;
                txtMecanico.setText(user.getNome());

            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE MECÂNICO COM ESTE CÓDIGO");
            txtCodMec.setText("");
            txtCodMec.requestFocus();
        }

    }

    public void retPecaServico() {
        int codigo = Integer.parseInt(txtps.getText());

        ServicoPecaDAO psDAO = new ServicoPecaDAO();
        if (psDAO.buscaPecaServico(codigo)) {

            PecaServico ps;
            ps = psDAO.consultaPecaServico(codigo);
            if (psDAO.consultaStatusPecaServico(codigo)) {
                txtps.setText(Integer.toString(ps.getCodigo()));
                txtDescricaoPecServ.setText(ps.getDescricao());
                txtValorUn.setText(Double.toString(ps.getValor()));
            } else {
                if (ps.getTipo() == 1) {
                    JOptionPane.showMessageDialog(null, "ESCOLHA UMA PEÇA QUE ESTEJA ATIVA");
                    txtps.setText("");
                    txtps.requestFocus();
                } else if (ps.getTipo() == 2) {
                    JOptionPane.showMessageDialog(null, "ESCOLHA UMA SERVIÇO QUE ESTEJA ATIVO");
                    txtps.setText("");
                    txtps.requestFocus();
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE PEÇA OU SERVICO COM ESTE CÓDIGO");
            txtps.setText("");
            txtps.requestFocus();
        }

    }

    public void preencheJtablePecaServico() {

        int codigo = Integer.parseInt(txtps.getText());
        double subtotalFormatado;
        double totalFormatado;
        ServicoPecaDAO psDAO = new ServicoPecaDAO();
        PecaServico ps;
        ps = psDAO.consultaPecaServico(codigo);

        listBancoPecaServico.add(ps);
        double subTotal = 0;
        int quant = Integer.parseInt(txtQtd.getText());

        if (!txtTotal.getText().isEmpty()) {
            // total = Float.valueOf(txtTotal.getText());
            total = Float.valueOf(txtTotalStr);
        } else {
            total = 0;
        }

        boolean contem = false;
        int pos = 0;

        subTotal += quant * ps.getValor();
        total += subTotal;

        ArrayList<PecaServico> carrinho = osTableMod.getList();
        for (PecaServico pesserv : carrinho) {
            if (pesserv.getCodigo() == ps.getCodigo()) {
                contem = true;
                pos = carrinho.indexOf(pesserv);
            }
        }
        carrinho = null;
        if (!contem) {

            subtotalFormatado = masc.converterDoubleDoisDecimais(subTotal);
            totalFormatado = masc.converterDoubleDoisDecimais(total);

            ps.setQuantidade(quant);
            ps.setSubtotal(subtotalFormatado);
            osTableMod.addRow(ps);
            //  txtTotal.setText(String.valueOf(totalFormatado));
            txtTotalStr = String.valueOf(totalFormatado);
            txtTotal.setText(dinheiroBR.format(total));

        } else {
            int quantAtual = (int) osTableMod.getValueAt(pos, 2);
            double subTotalAtual = (double) osTableMod.getValueAt(pos, 4);
            totalFormatado = masc.converterDoubleDoisDecimais(total);

            subTotalAtual += subTotal;
            quantAtual += quant;
            subtotalFormatado = masc.converterDoubleDoisDecimais(subTotalAtual);
            osTableMod.setValueAt(quantAtual, pos, 2);
            osTableMod.setValueAt(subtotalFormatado, pos, 4);
            // txtTotal.setText(String.valueOf(totalFormatado));
            txtTotalStr = String.valueOf(totalFormatado);
            txtTotal.setText(dinheiroBR.format(total));

        }

    }

    private void alterar() {

        if (osTableMod.getList().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "NAO HÁ NENHUM ITEM", "Erro", JOptionPane.ERROR_MESSAGE);
        } else if (tablePecaServicoOs.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(null, "SELECIONE A LINHA PARA ALTERAR", "Alterar", JOptionPane.WARNING_MESSAGE);
        } else {
            int quant, codigoPeca;
            quant = Integer.parseInt(osTableMod.getValueAt(tablePecaServicoOs.getSelectedRow(), 2).toString());
            codigoPeca = Integer.parseInt(osTableMod.getValueAt(tablePecaServicoOs.getSelectedRow(), 0).toString());
            txtps.setText("" + codigoPeca);
            txtQtd.setText("" + quant);
            txtps.setEditable(false);
        }
    }

    private void removerItemID(int id) {

        double auxSubTotal;
        double totalForm;

        auxSubTotal = Double.valueOf(osTableMod.getValueAt(id, 4).toString());

        total -= auxSubTotal;
        totalForm = masc.converterDoubleDoisDecimais(total);
        //txtTotal.setText(String.valueOf(totalForm));
        txtTotalStr = String.valueOf(totalForm);
        txtTotal.setText(dinheiroBR.format(total));
        osTableMod.removeRow(id);

    }

    private void removerItem() {

        double auxSubTotal;
        double totalForm;
        txtps.setEditable(true);
        txtps.setText(null);
        txtQtd.setText(null);
        txtps.requestFocus();
        if (osTableMod.getList().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "NAO HÁ NENHUM ITEM PARA SER REMOVIDO. ADICIONE!", "Erro", JOptionPane.ERROR_MESSAGE);
        } else if (tablePecaServicoOs.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(null, "SELECIONE A LINHA PARA REMOVER", "Remover", JOptionPane.WARNING_MESSAGE);
        } else {
            auxSubTotal = Double.valueOf(osTableMod.getValueAt(tablePecaServicoOs.getSelectedRow(), 4).toString());

            if (controlBotao == 2) {
                // total = Double.parseDouble(txtTotal.getText());
                total = Double.parseDouble(txtTotalStr);
            }
            total -= auxSubTotal;
            totalForm = masc.converterDoubleDoisDecimais(total);

            //txtTotal.setText(String.valueOf(totalForm));
            txtTotalStr = String.valueOf(totalForm);
            txtTotal.setText(dinheiroBR.format(total));
            osTableMod.removeRow(tablePecaServicoOs.getSelectedRow());
        }
    }

    private void alterarQuantidade() {

        int quant, pos = 0;
        double valorUni = 0, auxTotal, subTotal;
        double totalForm;
        int codigo;

        quant = Integer.parseInt(txtQtd.getText());
        codigo = Integer.parseInt(txtps.getText());
        ArrayList<PecaServico> pecasServicosOs = osTableMod.getList();

        for (PecaServico pecServ : pecasServicosOs) {
            if (pecServ.getCodigo() == (codigo)) {
                pos = pecasServicosOs.indexOf(pecServ);
                valorUni = pecServ.getValor();
            }
        }

        pecasServicosOs = null;
        //total = Double.valueOf(txtTotal.getText());
        total = Double.valueOf(txtTotalStr);

        if (quant < 0) {
            JOptionPane.showMessageDialog(rootPane, "QUANTIDADE INVÁLIDA", "Aviso", JOptionPane.WARNING_MESSAGE);
        } else if (quant == 0) {
            removerItemID(pos);
        } else {

            subTotal = Double.valueOf(osTableMod.getValueAt(pos, 4).toString());
            total -= subTotal;
            osTableMod.setValueAt(quant, pos, 2);
            osTableMod.setValueAt((quant * valorUni), pos, 4);

            total += (quant * valorUni);
            totalForm = masc.converterDoubleDoisDecimais(total);

            //txtTotal.setText("" + totalForm);
            txtTotalStr = String.valueOf(totalForm);
            txtTotal.setText(dinheiroBR.format(total));

            txtQtd.setText("");
            txtps.setText("");
            txtps.setEnabled(true);
            btnRemover.setEnabled(true);

        }
    }

    public void desativaComboBox() {

        JcomboMinuto.setEnabled(false);
        jcomboHora.setEnabled(false);
        jcomboHora.setSelectedIndex(0);
        JcomboMinuto.setSelectedIndex(0);
        jComboBoxHoraAtual.setSelectedIndex(0);
        jComboBoxMinutoAtual.setSelectedIndex(0);

    }

    public void AtivaComboBox() {
        JcomboMinuto.setEnabled(true);
        jcomboHora.setEnabled(true);
        jcomboHora.setSelectedIndex(0);
        JcomboMinuto.setSelectedIndex(0);
        jComboBoxHoraAtual.setSelectedIndex(0);
        jComboBoxMinutoAtual.setSelectedIndex(0);
    }

    private void camposEditaveis() {

        jfplaca.setEditable(true);
        txtCodMec.setEditable(true);
        jDateChooser1.setEnabled(true);
        txtps.setEditable(true);
        txtQtd.setEditable(true);

    }

    public void listaTextFiel() {

        int codOs;

        codOs = Integer.parseInt(opItem);
        OrdemServico os;
        os = osDAO.consultaOrdemServico(codOs);
        codigoOrdemServico = codOs;
        codCliente = os.getCod_cliente();
        codUsuario = os.getCod_mecanico();
        codigoVeiculo = os.getCod_veiculo();

        if (osDAO.buscaOs(codOs)) {

            jfplaca.setText(os.getPlacaVeiculo());
            VeiculoDAO vDAO = new VeiculoDAO();

            if (vDAO.buscaVeiculoPlaca(os.getPlacaVeiculo())) {
                Veiculo v;
                v = vDAO.consultaVeiculoPlaca(os.getPlacaVeiculo());
                txtNomeCliente.setText(os.getNomeCliente());
                txtCpfCnpj.setText(v.getCpfCnpjCliente());
                txtNomeModelo.setText(v.getNomeModelo());
                txtCor.setText(v.getCorModelo());
                txtMarca.setText(v.getMarcaModelo());
                txtTelefone.setText(v.getTelefoneCliente());

                codigoVeiculo = v.getCodigo();
            }
            txtCodMec.setText(Integer.toString(os.getCod_mecanico()));
            LoginDAO logDAO = new LoginDAO();
            if (logDAO.buscaUsuario(os.getCod_mecanico())) {

                Usuario user;
                user = logDAO.consultaUsuario(os.getCod_mecanico());
                txtMecanico.setText(user.getNome());
            }

            txtAreaProblema.setEditable(true);

            txtAreaProblema.setText(os.getDescProblema());
            preencheTabelaPeca();
            dataAtualCad = os.getDataEntrada();
            horaAtualCadastrada = os.getHorarioEntrada().getHour();
            minutoAtualCadastrado = os.getHorarioEntrada().getMinute();

            String horaprStr = Integer.toString(os.getHorarioPrevisto().getHour());
            String minutoprStr = Integer.toString(os.getHorarioPrevisto().getMinute());
            String horaatStr = Integer.toString(os.getHorarioEntrada().getHour());

            String minutoatStr = Integer.toString(os.getHorarioEntrada().getMinute());

            if (os.getHorarioPrevisto().getHour() < 10) {
                horaprStr = new StringBuilder().append(0).append(Integer.toString(os.getHorarioPrevisto().getHour())).toString();
            }

            if (os.getHorarioPrevisto().getMinute() < 10) {
                minutoprStr = new StringBuilder().append(0).append(Integer.toString(os.getHorarioPrevisto().getMinute())).toString();
            }

            if (os.getHorarioEntrada().getHour() < 10) {
                horaatStr = new StringBuilder().append(0).append(Integer.toString(os.getHorarioEntrada().getHour())).toString();
            }
            if (os.getHorarioEntrada().getMinute() < 10) {
                minutoatStr = new StringBuilder().append(0).append(Integer.toString(os.getHorarioEntrada().getMinute())).toString();
            }
            jComboBoxHoraAtual.addItem(horaatStr);
            jComboBoxHoraAtual.setSelectedIndex(2);
            jComboBoxMinutoAtual.addItem(minutoatStr);
            jComboBoxMinutoAtual.setSelectedIndex(2);

            try {
                jDateChooser1.setDate(conv.converteLocalDatetoDate(os.getDataPrevista()));

            } catch (Exception ex) {
                Logger.getLogger(TelaAbrirOs.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            jcomboHora.setSelectedItem(horaprStr);
            JcomboMinuto.setSelectedItem(minutoprStr);
            jfDataInclusao.setText(formatador.format(os.getDataEntrada()));
        }
    }

    public void preencheTabelaPeca() {

        int codOs;

        codOs = Integer.parseInt(opItem);
        OrdemServico orse;
        orse = osDAO.consultaOrdemServico(codOs);
        double totalOs = 0;
        for (PecaServico ps : orse.getPecaServicos()) {

            osTableMod.addRow(ps);
            totalOs += ps.getSubtotal();

        }
        // txtTotal.setText(Double.toString(totalOs));

        txtTotalStr = String.valueOf(totalOs);
        txtTotal.setText(dinheiroBR.format(totalOs));

    }

    public void alterarOs() {

        boolean flag;
       
        
        if (jfplaca.getText().trim().isEmpty()
                || txtCodMec.getText().trim().isEmpty()
                || txtAreaProblema.getText().trim().isEmpty()
                || jDateChooser1.getDate() == null
                || jcomboHora.getSelectedIndex() == 0
                || JcomboMinuto.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
            jfplaca.requestFocus();
        } else {

            LocalDate dataPrv = conv.converterStringtoLocalDate(dataPrevista);

            horaPrevista = Integer.parseInt(jcomboHora.getSelectedItem().toString());
            minutoPrevisto = Integer.parseInt(JcomboMinuto.getSelectedItem().toString());

            LocalTime horarioPrevisto = LocalTime.of(horaPrevista, minutoPrevisto);
            OrdemServico os = new OrdemServico();

            os.setPlacaVeiculo(jfplaca.getText());
            os.setCod_cliente(codCliente);
            os.setCod_veiculo(codigoVeiculo);
            os.setCod_mecanico(codUsuario);
            os.setPecaServicos(osTableMod.getList());
            os.setDescProblema(txtAreaProblema.getText());

            os.setHorarioPrevisto(horarioPrevisto);
            os.setDataPrevista(dataPrv);

            os.setStatus("Em andamento");
            os.setValor_total(Double.parseDouble(txtTotalStr));

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO VEÍCULO", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = osDAO.alterarOrdemServico(os, codigoOrdemServico);
                    if (flag == true) {

                        JOptionPane.showMessageDialog(null, "ORDEM DE SERVIÇO ALTERADA COM SUCESSO");
                        ct.clear(panelPecaServico);
                        ct.clear(panelPrincipal);
                        ct.clear(panelModVeic);
                        ct.clear(panelMecanico);
                        txtAreaProblema.setText("");
                        jfplaca.setValue(null);
                        ct.campoNaoEditavel(panelPrincipal);
                        ct.campoNaoEditavel(panelPecaServico);
                        ct.campoNaoEditavel(panelDescricaoProblema);
                        ct.campoNaoEditavel(panelMecanico);
                        txtAreaProblema.setText("");
                        osTableMod.clearTable();
                        txtAreaProblema.setEditable(false);
                        jfDataInclusao.setText("");
                        jDateChooser1.setEnabled(false);
                        jDateChooser1.setDate(null);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);
                        tp.atualizaInformativo();
                        this.dispose();

                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class
                            .getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "NÃO FOI POSSÍVEL ALTTERAR A ORDEM DE SERVIÇO");
                }

            } else if (resp == 1) {
                ct.clear(panelDescricaoProblema);
                ct.clear(panelPecaServico);
                ct.clear(panelPrincipal);
                ct.clear(panelModVeic);
                ct.clear(panelMecanico);
                txtAreaProblema.setText("");
                jfplaca.setValue(null);
                ct.campoNaoEditavel(panelPrincipal);
                ct.campoNaoEditavel(panelPecaServico);
                ct.campoNaoEditavel(panelDescricaoProblema);
                ct.campoNaoEditavel(panelMecanico);
                txtAreaProblema.setText("");
                osTableMod.clearTable();
                txtAreaProblema.setEditable(false);
                jfDataInclusao.setText("");
                jDateChooser1.setEnabled(false);
                jDateChooser1.setDate(null);
                desativaComboBox();

                btnIncluir.setEnabled(true);

                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);
            }

            btnIncluir.setEnabled(true);

            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setEnabled(false);
            btnCancelar.setVisible(false);

        }

    }

    public void ativaAlterarOs() {
        if (tp.grupo.equals("Mecânico")) {
            controlBotao = 2;
            btnIncluir.setEnabled(false);
            txtps.setEditable(true);
            txtQtd.setEditable(true);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            txtAreaProblema.requestFocus();
            jcomboHora.setEnabled(false);
            JcomboMinuto.setEnabled(false);
        } else {
            controlBotao = 2;
            jfplaca.setEditable(true);
            txtCodMec.setEditable(true);
            txtps.setEditable(true);
            txtQtd.setEditable(true);
            jDateChooser1.setEnabled(true);
            jfplaca.requestFocus();
            btnIncluir.setEnabled(false);

            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
        }

    }

    public String getOpItem() {
        return opItem;
    }

    public void setOpItem(String opItem) {
        this.opItem = opItem;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JcomboMinuto;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSair;
    private javax.swing.JComboBox<String> jComboBoxHoraAtual;
    private javax.swing.JComboBox<String> jComboBoxMinutoAtual;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox<String> jcomboHora;
    private javax.swing.JFormattedTextField jfDataInclusao;
    private javax.swing.JFormattedTextField jfplaca;
    private javax.swing.JScrollPane paneLScroolArea;
    private javax.swing.JPanel panelDescricaoProblema;
    private javax.swing.JPanel panelHoraAtual;
    private javax.swing.JPanel panelHoraPrevista;
    private javax.swing.JPanel panelMecanico;
    private javax.swing.JPanel panelModVeic;
    private javax.swing.JPanel panelPecaServico;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JTable tablePecaServicoOs;
    private javax.swing.JTextArea txtAreaProblema;
    private javax.swing.JTextField txtCodMec;
    private javax.swing.JTextField txtCor;
    private javax.swing.JTextField txtCpfCnpj;
    private javax.swing.JTextField txtDescricaoPecServ;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtMecanico;
    private javax.swing.JTextField txtNomeCliente;
    private javax.swing.JTextField txtNomeModelo;
    private javax.swing.JTextField txtQtd;
    private javax.swing.JTextField txtTelefone;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtValorUn;
    private javax.swing.JTextField txtps;
    // End of variables declaration//GEN-END:variables
}
