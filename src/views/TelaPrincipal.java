package views;

import bankConnection.Conexao;
import help.DataSistema;
import help.Ip;
import controllers.LoginDAO;
import controllers.OrdemServicoDAO;
import filters.TelaBuscaAno;
import filters.TelaBuscaMarca;
import filters.TelaBuscaModelo;
import filters.TelaBuscaDataInclusao;
import filters.TelaBuscaNumeroOS;
import filters.TelaBuscaStituacaoOS;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.OrdemServico;
import models.Usuario;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaPrincipal extends javax.swing.JFrame {

    LoginDAO log = new LoginDAO();
    Usuario usuario = new Usuario();
    private final OrdemServicoDAO osDAO;
    public String acesso;
    public String grupo;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public TelaPrincipal() {
        initComponents();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        osDAO = new OrdemServicoDAO();
        Ip ip = new Ip();
        labelIp.setText(ip.getIp());
        DataSistema dt = new DataSistema();
        lbData.setText(dt.dataSis());
        String user = log.buscarUltimoLogin().toUpperCase();
        lbUsuario.setText(user);
        acesso = lbUsuario.getText();
        usuario = log.consultaUsuario(log.buscarUltimoLoginCodigo());

        grupo = usuario.getGrupo();

        lbUsuario.setVisible(true);
        this.setIconImage(new ImageIcon(getClass().getResource("/img/omgf.png")).getImage());
        atualizaInformativo();

        if (grupo.equals("Mecânico")) {

            menuCadastro.setVisible(false);
            MenuITemOsFinalizadas.setVisible(false);
            menuitemOsAbrir.setVisible(false);
            menuRelatório.setVisible(false);
            menuItemModeloVeiculo.setVisible(false);
            preencheTabelaMecLog();

        } else {

            preencheTabela();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMaior = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        labelIp = new javax.swing.JLabel();
        panelInformativo = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableInformativo = new javax.swing.JTable();
        btnAtualizar = new javax.swing.JButton();
        btnOsStituacao = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        labelOpcao = new javax.swing.JLabel();
        labelResultado = new javax.swing.JLabel();
        panelNomeOficina = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelUsuario = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lbUsuario = new javax.swing.JLabel();
        panelData = new javax.swing.JPanel();
        lbData = new javax.swing.JLabel();
        barMenu = new javax.swing.JMenuBar();
        menuCadastro = new javax.swing.JMenu();
        menuCliente = new javax.swing.JMenuItem();
        menuVeiculos = new javax.swing.JMenuItem();
        meunuitemPeca = new javax.swing.JMenuItem();
        menuItemMecanico = new javax.swing.JMenuItem();
        menuOS = new javax.swing.JMenu();
        menuitemOsAbrir = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        MenuITemOsFinalizadas = new javax.swing.JMenuItem();
        menuRelatório = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        submenuTrocarUsuario = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuItemModeloVeiculo = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        menuSair = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gas Monkey Oficina");
        setBackground(new java.awt.Color(204, 204, 204));
        setMinimumSize(new java.awt.Dimension(1340, 700));
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        panelMaior.setBackground(new java.awt.Color(204, 204, 204));
        panelMaior.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PostgreSQL.png"))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("IP Local:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Base de Dados: ogm");

        labelIp.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        panelInformativo.setBackground(new java.awt.Color(204, 204, 204));
        panelInformativo.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "INFORMATIVO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jScrollPane1.setFocusable(false);

        tableInformativo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo O.S.", "Situação", "Data Prevista", "Horario Previsto"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableInformativo.setFocusable(false);
        jScrollPane1.setViewportView(tableInformativo);
        if (tableInformativo.getColumnModel().getColumnCount() > 0) {
            tableInformativo.getColumnModel().getColumn(0).setPreferredWidth(15);
            tableInformativo.getColumnModel().getColumn(1).setPreferredWidth(40);
            tableInformativo.getColumnModel().getColumn(2).setPreferredWidth(35);
            tableInformativo.getColumnModel().getColumn(3).setPreferredWidth(40);
        }

        btnAtualizar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Synchronize_12x12.png"))); // NOI18N
        btnAtualizar.setMnemonic('A');
        btnAtualizar.setText("Atualizar");
        btnAtualizar.setToolTipText("Atualiza os dados deste painel");
        btnAtualizar.setFocusable(false);
        btnAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });

        btnOsStituacao.setBackground(new java.awt.Color(204, 204, 204));
        btnOsStituacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnOsStituacao.setMnemonic('O');
        btnOsStituacao.setText("O.S Situacao");
        btnOsStituacao.setToolTipText("Abre a tela de Acompanhamento");
        btnOsStituacao.setFocusable(false);
        btnOsStituacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOsStituacaoActionPerformed(evt);
            }
        });

        jComboBox1.setBackground(new java.awt.Color(204, 204, 204));
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione uma opção", "Total O.S.:", "Total O.S. abertas:", "Total O.S. em andamento:", "Total O.S. atrasadas:", "Total O.S. finalizadas:", "Total O.S. pra Hoje:" }));
        jComboBox1.setFocusable(false);
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        labelOpcao.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        labelResultado.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        javax.swing.GroupLayout panelInformativoLayout = new javax.swing.GroupLayout(panelInformativo);
        panelInformativo.setLayout(panelInformativoLayout);
        panelInformativoLayout.setHorizontalGroup(
            panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInformativoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(panelInformativoLayout.createSequentialGroup()
                        .addGroup(panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelInformativoLayout.createSequentialGroup()
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(120, 120, 120)
                                .addComponent(btnAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelInformativoLayout.createSequentialGroup()
                                .addComponent(labelOpcao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelResultado)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInformativoLayout.createSequentialGroup()
                        .addGap(0, 0, 0)
                        .addComponent(btnOsStituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelInformativoLayout.setVerticalGroup(
            panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInformativoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(btnOsStituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(panelInformativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelResultado)
                    .addComponent(labelOpcao))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout panelMaiorLayout = new javax.swing.GroupLayout(panelMaior);
        panelMaior.setLayout(panelMaiorLayout);
        panelMaiorLayout.setHorizontalGroup(
            panelMaiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMaiorLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panelMaiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMaiorLayout.createSequentialGroup()
                        .addGroup(panelMaiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addGroup(panelMaiorLayout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelIp, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(40, 40, 40))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMaiorLayout.createSequentialGroup()
                        .addComponent(panelInformativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        panelMaiorLayout.setVerticalGroup(
            panelMaiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMaiorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelInformativo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(panelMaiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelIp, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(20, 20, 20))
        );

        panelNomeOficina.setBackground(new java.awt.Color(204, 204, 204));
        panelNomeOficina.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("001 - Oficina Gas Monkey");

        javax.swing.GroupLayout panelNomeOficinaLayout = new javax.swing.GroupLayout(panelNomeOficina);
        panelNomeOficina.setLayout(panelNomeOficinaLayout);
        panelNomeOficinaLayout.setHorizontalGroup(
            panelNomeOficinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNomeOficinaLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addContainerGap(705, Short.MAX_VALUE))
        );
        panelNomeOficinaLayout.setVerticalGroup(
            panelNomeOficinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNomeOficinaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addGap(10, 10, 10))
        );

        panelUsuario.setBackground(new java.awt.Color(204, 204, 204));
        panelUsuario.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("USUÁRIO - ");

        lbUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout panelUsuarioLayout = new javax.swing.GroupLayout(panelUsuario);
        panelUsuario.setLayout(panelUsuarioLayout);
        panelUsuarioLayout.setHorizontalGroup(
            panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUsuarioLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbUsuario)
                .addGap(40, 40, 40))
        );
        panelUsuarioLayout.setVerticalGroup(
            panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUsuarioLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lbUsuario))
                .addGap(10, 10, 10))
        );

        panelData.setBackground(new java.awt.Color(204, 204, 204));
        panelData.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbData.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbData.setText("dd/mm/yyyy");

        javax.swing.GroupLayout panelDataLayout = new javax.swing.GroupLayout(panelData);
        panelData.setLayout(panelDataLayout);
        panelDataLayout.setHorizontalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDataLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lbData)
                .addGap(30, 30, 30))
        );
        panelDataLayout.setVerticalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDataLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lbData)
                .addGap(10, 10, 10))
        );

        barMenu.setBackground(new java.awt.Color(204, 204, 204));
        barMenu.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        menuCadastro.setBackground(new java.awt.Color(204, 204, 204));
        menuCadastro.setMnemonic('D');
        menuCadastro.setText("Cadastros");

        menuCliente.setMnemonic('C');
        menuCliente.setText("Clientes");
        menuCliente.setToolTipText("Atalho F1");
        menuCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClienteActionPerformed(evt);
            }
        });
        menuCadastro.add(menuCliente);

        menuVeiculos.setMnemonic('V');
        menuVeiculos.setText("Veículos");
        menuVeiculos.setToolTipText("Atalho F3");
        menuVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuVeiculosActionPerformed(evt);
            }
        });
        menuCadastro.add(menuVeiculos);

        meunuitemPeca.setMnemonic('P');
        meunuitemPeca.setText("Peças e Serviços");
        meunuitemPeca.setToolTipText("Atalho F5");
        meunuitemPeca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                meunuitemPecaActionPerformed(evt);
            }
        });
        menuCadastro.add(meunuitemPeca);

        menuItemMecanico.setMnemonic('U');
        menuItemMecanico.setText("Usuários");
        menuItemMecanico.setToolTipText("Atalho F4");
        menuItemMecanico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemMecanicoActionPerformed(evt);
            }
        });
        menuCadastro.add(menuItemMecanico);

        barMenu.add(menuCadastro);

        menuOS.setMnemonic('O');
        menuOS.setText("O.S.");

        menuitemOsAbrir.setMnemonic('A');
        menuitemOsAbrir.setText("Abrir O.S.");
        menuitemOsAbrir.setToolTipText("Atalho F6");
        menuitemOsAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemOsAbrirActionPerformed(evt);
            }
        });
        menuOS.add(menuitemOsAbrir);

        jMenuItem10.setMnemonic('S');
        jMenuItem10.setText("Acompanhamento O.S");
        jMenuItem10.setToolTipText("Atalho F8");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        menuOS.add(jMenuItem10);

        MenuITemOsFinalizadas.setMnemonic('F');
        MenuITemOsFinalizadas.setText("O.S. Finalizadas");
        MenuITemOsFinalizadas.setToolTipText("Atalho F9");
        MenuITemOsFinalizadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuITemOsFinalizadasActionPerformed(evt);
            }
        });
        menuOS.add(MenuITemOsFinalizadas);

        barMenu.add(menuOS);

        menuRelatório.setMnemonic('R');
        menuRelatório.setText("Relatórios");
        menuRelatório.setToolTipText("Relatórios do sistema");

        jMenu3.setMnemonic('C');
        jMenu3.setText("Clientes");

        jMenuItem4.setMnemonic('F');
        jMenuItem4.setText("Pessoa física");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem9.setMnemonic('J');
        jMenuItem9.setText("Pessoa jurídica");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        menuRelatório.add(jMenu3);

        jMenu4.setMnemonic('V');
        jMenu4.setText("Veículos");

        jMenuItem11.setMnemonic('M');
        jMenuItem11.setText("Marca");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem11);

        jMenuItem12.setMnemonic('M');
        jMenuItem12.setText("Modelo");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem12);

        jMenuItem13.setMnemonic('A');
        jMenuItem13.setText("Ano de fabricação");
        jMenuItem13.setToolTipText("");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem13);

        menuRelatório.add(jMenu4);

        jMenuItem14.setText("Peças");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        menuRelatório.add(jMenuItem14);

        jMenu5.setMnemonic('O');
        jMenu5.setText("Ordens de serviço");

        jMenuItem17.setMnemonic('N');
        jMenuItem17.setText("Número da O.S");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem17);

        jMenuItem15.setMnemonic('S');
        jMenuItem15.setText("Status O.S");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem15);

        jMenuItem16.setMnemonic('D');
        jMenuItem16.setText("Data de inclusão");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem16);

        menuRelatório.add(jMenu5);

        jMenuItem18.setText("Usuários");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        menuRelatório.add(jMenuItem18);

        barMenu.add(menuRelatório);

        jMenu2.setMnemonic('C');
        jMenu2.setText("Configurações");
        jMenu2.setToolTipText("Configurações do sistema");

        jMenuItem8.setMnemonic('P');
        jMenuItem8.setText("Preferências");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem8);

        submenuTrocarUsuario.setMnemonic('T');
        submenuTrocarUsuario.setText("Trocar Usuário");
        submenuTrocarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenuTrocarUsuarioActionPerformed(evt);
            }
        });
        jMenu2.add(submenuTrocarUsuario);
        jMenu2.add(jSeparator1);

        menuItemModeloVeiculo.setMnemonic('M');
        menuItemModeloVeiculo.setText("Modelo de Veículos");
        menuItemModeloVeiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemModeloVeiculoActionPerformed(evt);
            }
        });
        jMenu2.add(menuItemModeloVeiculo);

        barMenu.add(jMenu2);

        jMenu6.setMnemonic('O');
        jMenu6.setText("Sobre");
        jMenu6.setToolTipText("Informações sobre os desenvolvedores");
        jMenu6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu6MouseClicked(evt);
            }
        });
        barMenu.add(jMenu6);

        menuSair.setMnemonic('S');
        menuSair.setText("Sair");
        menuSair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuSairMouseClicked(evt);
            }
        });
        menuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
        barMenu.add(menuSair);

        setJMenuBar(barMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelNomeOficina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(panelUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(panelMaior, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(panelMaior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(panelData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelNomeOficina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClienteActionPerformed

        TelaCliente clie = new TelaCliente(this, true);
        clie.setVisible(true);

    }//GEN-LAST:event_menuClienteActionPerformed

    private void menuVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuVeiculosActionPerformed

        TelaVeiculos veic = new TelaVeiculos(this, true);
        veic.setVisible(true);


    }//GEN-LAST:event_menuVeiculosActionPerformed

    private void menuItemMecanicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemMecanicoActionPerformed
        TelaUsuário tm = new TelaUsuário(this, true);
        tm.setVisible(true);
    }//GEN-LAST:event_menuItemMecanicoActionPerformed

    private void meunuitemPecaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_meunuitemPecaActionPerformed
        TelaPecaServico tps = new TelaPecaServico(this, true);
        tps.setVisible(true);
    }//GEN-LAST:event_meunuitemPecaActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        TelaLogin login = new TelaLogin(this, true);
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

            login.setVisible(true);
            this.dispose();
            TelaPrincipal tP = new TelaPrincipal();
            tP.setVisible(true);
            if (tP.grupo.equals("Mecânico")) {
                TelaOsStatus tos = new TelaOsStatus(new java.awt.Frame(), true);
                tos.setVisible(true);

            }
        } else {
            if (evt.getKeyCode() == KeyEvent.VK_F1) {
                if (!grupo.equals("Mecânico")) {
                    TelaCliente cli = new TelaCliente(this, true);
                    cli.setVisible(true);

                }

            } else {
                if (evt.getKeyCode() == KeyEvent.VK_F2) {
                    if (!grupo.equals("Mecânico")) {
                        TelaModeloVeiculo tmveic = new TelaModeloVeiculo(this, true);
                        tmveic.setVisible(true);

                    }

                } else {
                    if (evt.getKeyCode() == KeyEvent.VK_F3) {
                        if (!grupo.equals("Mecânico")) {
                            TelaVeiculos tveic = new TelaVeiculos(this, true);
                            tveic.setVisible(true);

                        }

                    } else {
                        if (evt.getKeyCode() == KeyEvent.VK_F4) {
                            if (!grupo.equals("Mecânico")) {
                                TelaUsuário tm = new TelaUsuário(new java.awt.Frame(), true);
                                tm.setVisible(true);

                            }

                        } else {

                            if (evt.getKeyCode() == KeyEvent.VK_F5) {
                                if (!grupo.equals("Mecânico")) {
                                    TelaPecaServico tps = new TelaPecaServico(new java.awt.Frame(), true);
                                    tps.setVisible(true);
                                }

                            } else {

                                if (evt.getKeyCode() == KeyEvent.VK_F6) {
                                    if (!grupo.equals("Mecânico")) {
                                        TelaAbrirOs taos = new TelaAbrirOs(new java.awt.Frame(), true);
                                        taos.setVisible(true);
                                    }

                                } else {

                                    if (evt.getKeyCode() == KeyEvent.VK_F7) {
                                        TelaOsStatus taos = new TelaOsStatus(new java.awt.Frame(), true);
                                        taos.setVisible(true);
                                    } else {

                                        if (evt.getKeyCode() == KeyEvent.VK_F9) {
                                            TelaOsFinalizadas tosf = new TelaOsFinalizadas(new java.awt.Frame(), true);
                                            tosf.setVisible(true);
                                        } else {
                                            if (evt.getKeyCode() == KeyEvent.VK_F10) {
                                                atualizaInformativo();
                                            }

                                        }

                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_formKeyPressed

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSairActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A FINALIZAÇÃO DO SISTEMA", "Confirmação", JOptionPane.YES_NO_OPTION);
        if (resp == 0) {
            System.exit(0);
        }
    }//GEN-LAST:event_menuSairActionPerformed

    private void menuSairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuSairMouseClicked
        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A FINALIZAÇÃO DO SISTEMA", "Confirmação", JOptionPane.YES_NO_OPTION);
        if (resp == 0) {
            System.exit(0);
        }
    }//GEN-LAST:event_menuSairMouseClicked

    private void menuItemModeloVeiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemModeloVeiculoActionPerformed
        TelaModeloVeiculo tmv = new TelaModeloVeiculo(this, true);
        tmv.setVisible(true);
    }//GEN-LAST:event_menuItemModeloVeiculoActionPerformed

    private void MenuITemOsFinalizadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuITemOsFinalizadasActionPerformed
        TelaOsFinalizadas tosf = new TelaOsFinalizadas(this, true);
        tosf.setVisible(true);
    }//GEN-LAST:event_MenuITemOsFinalizadasActionPerformed

    private void menuitemOsAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemOsAbrirActionPerformed
        TelaAbrirOs tosf = new TelaAbrirOs(this, true);
        tosf.setVisible(true);
    }//GEN-LAST:event_menuitemOsAbrirActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        TelaOsStatus tosf = new TelaOsStatus(this, true);
        tosf.setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        TelaPreferencias tcf = new TelaPreferencias(this, true);
        tcf.setVisible(true);
        if (tcf.opcao == 1) {
            panelInformativo.setVisible(false);
        } else if (tcf.opcao == 2) {
            panelInformativo.setVisible(true);
        }
        panelData.setBackground(tcf.getCor());
        panelInformativo.setBackground(tcf.getCor());
        panelMaior.setBackground(tcf.getCor());
        panelNomeOficina.setBackground(tcf.getCor());
        panelUsuario.setBackground(tcf.getCor());
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenu6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu6MouseClicked
        TelaSobre tsb = new TelaSobre(this, true);
        tsb.setVisible(true);
    }//GEN-LAST:event_jMenu6MouseClicked

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        Connection conn = Conexao.getConexao();

        InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/clientesCNPJ.jasper");

        JasperPrint jasperPrint = null;
        try {

            jasperPrint = JasperFillManager.fillReport(src, null, conn);

        } catch (JRException ex) {

        }

        JasperViewer view = new JasperViewer(jasperPrint, false);
        view.setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        TelaBuscaMarca tbm = new TelaBuscaMarca(this, true);
        tbm.setVisible(true);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        Connection conn = Conexao.getConexao();

        InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/clientesCPF.jasper");

        JasperPrint jasperPrint = null;
        try {

            jasperPrint = JasperFillManager.fillReport(src, null, conn);

        } catch (JRException ex) {

        }

        JasperViewer view = new JasperViewer(jasperPrint, false);
        view.setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        Connection conn = Conexao.getConexao();

        InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/pecas.jasper");

        JasperPrint jasperPrint = null;
        try {

            jasperPrint = JasperFillManager.fillReport(src, null, conn);

        } catch (JRException ex) {

        }

        JasperViewer view = new JasperViewer(jasperPrint, false);
        view.setVisible(true);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        Connection conn = Conexao.getConexao();

        InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/usuarios.jasper");

        JasperPrint jasperPrint = null;
        try {

            jasperPrint = JasperFillManager.fillReport(src, null, conn);

        } catch (JRException ex) {

        }

        JasperViewer view = new JasperViewer(jasperPrint, false);
        view.setVisible(true);
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        TelaBuscaModelo tbmod = new TelaBuscaModelo(this, true);
        tbmod.setVisible(true);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        TelaBuscaAno tba = new TelaBuscaAno(this, true);
        tba.setVisible(true);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        TelaBuscaNumeroOS tno = new TelaBuscaNumeroOS(this, true);
        tno.setVisible(true);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        TelaBuscaStituacaoOS tso = new TelaBuscaStituacaoOS(this, true);
        tso.setVisible(true);
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        TelaBuscaDataInclusao tdi = new TelaBuscaDataInclusao(this, true);
        tdi.setVisible(true);
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
        atualizaInformativo();
    }//GEN-LAST:event_btnAtualizarActionPerformed

    private void btnOsStituacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOsStituacaoActionPerformed
        TelaOsStatus tosf = new TelaOsStatus(this, true);
        tosf.setVisible(true);
    }//GEN-LAST:event_btnOsStituacaoActionPerformed

    private void submenuTrocarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenuTrocarUsuarioActionPerformed
        TelaLogin login = new TelaLogin(this, true);
        login.setVisible(true);
        this.dispose();
        TelaPrincipal tP = new TelaPrincipal();
        tP.setVisible(true);
        if (tP.grupo.equals("Mecânico")) {
            TelaOsStatus tos = new TelaOsStatus(new java.awt.Frame(), true);
            tos.setVisible(true);

        }
    }//GEN-LAST:event_submenuTrocarUsuarioActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if (grupo.equals("Mecânico")) {

                if (jComboBox1.getSelectedIndex() != 0) {

                    int op = jComboBox1.getSelectedIndex();
                    switch (op) {

                        case 1:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsMecLog()));
                            break;
                        case 2:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsAbertasMegLog()));
                            break;
                        case 3:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsEmAndamentoMecLog()));
                            break;
                        case 4:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsAtrasadasMecLog()));
                            break;
                        case 5:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsFinalizadasMecLog()));
                            break;
                        case 6:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsPraHojeMecLog()));
                            break;

                    }
                }

            } else {

                if (jComboBox1.getSelectedIndex() != 0) {

                    int op = jComboBox1.getSelectedIndex();
                    switch (op) {

                        case 1:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOs()));
                            break;
                        case 2:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsAbertas()));
                            break;
                        case 3:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsEmAndamento()));
                            break;
                        case 4:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsAtrasadas()));
                            break;
                        case 5:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsFinalizadas()));
                            break;
                        case 6:
                            labelOpcao.setText(jComboBox1.getSelectedItem().toString());
                            labelResultado.setText(Integer.toString(osDAO.totalOsPraHoje()));
                            break;

                    }
                }

            }

        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    public void preencheTabelaMecLog() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableInformativo.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaMecanicoLogadoInformativo()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getStatus(),
                os.getDataPrevistaStr(),
                os.getHorarioPrevisto()

            });

        }

    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableInformativo.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaInformativo()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getStatus(),
                os.getDataPrevistaStr(),
                os.getHorarioPrevisto()

            });

        }

    }

    public void atualizaInformativo() {

        if (grupo.equals("Mecânico")) {
            jComboBox1.setSelectedIndex(0);
            preencheTabelaMecLog();
            osDAO.verificaAtraso();
            labelOpcao.setText("");
            labelResultado.setText("");

        } else {

            jComboBox1.setSelectedIndex(0);
            labelOpcao.setText("");
            labelResultado.setText("");
            osDAO.verificaAtraso();

            preencheTabela();
        }

    }

    public void painelInformativoMostrar() {

        panelInformativo.setVisible(true);
    }

    public void painelInformativoEsconder() {
        panelInformativo.setVisible(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem MenuITemOsFinalizadas;
    private javax.swing.JMenuBar barMenu;
    private javax.swing.JButton btnAtualizar;
    private javax.swing.JButton btnOsStituacao;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel labelIp;
    private javax.swing.JLabel labelOpcao;
    private javax.swing.JLabel labelResultado;
    private javax.swing.JLabel lbData;
    private javax.swing.JLabel lbUsuario;
    private javax.swing.JMenu menuCadastro;
    private javax.swing.JMenuItem menuCliente;
    private javax.swing.JMenuItem menuItemMecanico;
    private javax.swing.JMenuItem menuItemModeloVeiculo;
    private javax.swing.JMenu menuOS;
    private javax.swing.JMenu menuRelatório;
    private javax.swing.JMenu menuSair;
    private javax.swing.JMenuItem menuVeiculos;
    private javax.swing.JMenuItem menuitemOsAbrir;
    private javax.swing.JMenuItem meunuitemPeca;
    private javax.swing.JPanel panelData;
    private javax.swing.JPanel panelInformativo;
    private javax.swing.JPanel panelMaior;
    private javax.swing.JPanel panelNomeOficina;
    private javax.swing.JPanel panelUsuario;
    private javax.swing.JMenuItem submenuTrocarUsuario;
    private javax.swing.JTable tableInformativo;
    // End of variables declaration//GEN-END:variables
}
