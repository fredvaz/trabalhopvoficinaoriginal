package views;

import help.CampoTxt;
import help.ConverteData;
import help.Mascara;
import controllers.ClienteDAO;
import controllers.LoginDAO;
import help.DataSistema;
import help.LimitaNroCaracteres;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import models.Cliente;
import models.PessoaFisica;
import models.PessoaJuridica;

public final class TelaCliente extends javax.swing.JDialog {

    private final ClienteDAO cli;
    private final Mascara masc = new Mascara();
    private final ConverteData conv = new ConverteData();
    private LoginDAO log = new LoginDAO();
    TelaPrincipal tpr = new TelaPrincipal();
    private final CampoTxt ct = new CampoTxt();
    private MaskFormatter CNPJMask;
    private MaskFormatter CPFMask;
    private MaskFormatter dataMask;
    private MaskFormatter telefoneMask;
    private final DateTimeFormatter formatador;
    private final DataSistema dtSis;
    public boolean perm;
    public boolean edit;
    public boolean inserir;
    String user;
    String opItem;
    String clienteAtDt;
    int codigoCli;
    int opTipoPessoa;
    int controlBotao;
    Cliente pes;

    public TelaCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cli = new ClienteDAO();

        desativaComboBox();
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);
        try {
            CNPJMask = new MaskFormatter("##.###.###/####-##");
            CPFMask = new MaskFormatter("###.###.###-##");
            dataMask = new MaskFormatter("##/##/####");
            telefoneMask = new MaskFormatter("(##) # ####-####");
        } catch (ParseException ex) {
            Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        txtBairro.setDocument(new LimitaNroCaracteres(15));
        txtComplemento.setDocument(new LimitaNroCaracteres(14));
        txtMunicipio.setDocument(new LimitaNroCaracteres(14));
        txtNum.setDocument(new LimitaNroCaracteres(4));
        txtRua.setDocument(new LimitaNroCaracteres(38));
        txtNome.setDocument(new LimitaNroCaracteres(41));
        txtRzSoc.setDocument(new LimitaNroCaracteres(49));
        txtEmail.setDocument(new LimitaNroCaracteres(40));
        txtRgIe.setDocument(new LimitaNroCaracteres(13));

        opTipoPessoa = combTipoPessoa.getSelectedIndex();
        controlBotao = 0;
        DefaultTableModel modeloTab = (DefaultTableModel) tableCliente.getModel();
        tableCliente.setRowSorter(new TableRowSorter(modeloTab));
        preencheTabela();
        if (!cli.VerificaCliente()) {

            tableCliente.requestFocus();
            tableCliente.changeSelection(0, 0, false, false);
        } else if (!cli.VerificaCliente()) {

            btnPesquisar.setEnabled(false);
        }

        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
        btnAtivar.setVisible(false);

        pes = new Cliente();
        dtSis = new DataSistema();
        perm = log.permitirEx(tpr.acesso);
        edit = log.permitirAlt(tpr.acesso);
        inserir = log.permitirIns(tpr.acesso);

        if (tpr.grupo.equals("Administrador")) {

        } else if (tpr.grupo.equals("Operador")) {
            if (edit == false) {
                btnAlterar.setEnabled(false);
            }
            if (perm == false) {

                btnExcluir.setEnabled(false);
            }
            if (inserir == false) {

                btnAlterar.setEnabled(false);
            }

        } else {
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            btnPesquisar.setEnabled(false);

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrupoDtAt = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelCliente01 = new javax.swing.JPanel();
        combTipoPessoa = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtRzSoc = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        panelCliente02 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ftxtCnpjCpf = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtRgIe = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        fTxtDataNasc = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        fTxtTelefone = new javax.swing.JFormattedTextField();
        panelEnd = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtRua = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNum = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtBairro = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtMunicipio = new javax.swing.JTextField();
        comEstado = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtComplemento = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCliente = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnRestaurar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        radioDesativados = new javax.swing.JRadioButton();
        radioAtivados = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Clientes");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelCliente01.setBackground(new java.awt.Color(204, 204, 204));
        panelCliente01.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        combTipoPessoa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Pessoa Física", "Pessoa Juridica" }));
        combTipoPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combTipoPessoaActionPerformed(evt);
            }
        });
        combTipoPessoa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                combTipoPessoaKeyPressed(evt);
            }
        });

        jLabel1.setText("Fisica/Juridica*");

        jLabel2.setText("Nome*");

        txtNome.setEditable(false);
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeKeyPressed(evt);
            }
        });

        txtRzSoc.setEditable(false);
        txtRzSoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRzSocKeyPressed(evt);
            }
        });

        jLabel3.setText("Razão Social*");

        javax.swing.GroupLayout panelCliente01Layout = new javax.swing.GroupLayout(panelCliente01);
        panelCliente01.setLayout(panelCliente01Layout);
        panelCliente01Layout.setHorizontalGroup(
            panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCliente01Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(combTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCliente01Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtRzSoc))
                .addContainerGap())
        );
        panelCliente01Layout.setVerticalGroup(
            panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCliente01Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCliente01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRzSoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        panelCliente02.setBackground(new java.awt.Color(204, 204, 204));
        panelCliente02.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("CPF/CNPJ*");

        ftxtCnpjCpf.setEditable(false);
        ftxtCnpjCpf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ftxtCnpjCpfKeyPressed(evt);
            }
        });

        jLabel5.setText("RG/IE");

        txtRgIe.setEditable(false);
        txtRgIe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRgIeKeyPressed(evt);
            }
        });

        jLabel6.setText("Data Nasc*");

        fTxtDataNasc.setEditable(false);
        fTxtDataNasc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fTxtDataNascKeyPressed(evt);
            }
        });

        jLabel7.setText("Email*");

        txtEmail.setEditable(false);
        txtEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtEmailKeyPressed(evt);
            }
        });

        jLabel8.setText("Telefone*");

        fTxtTelefone.setEditable(false);
        fTxtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fTxtTelefoneKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelCliente02Layout = new javax.swing.GroupLayout(panelCliente02);
        panelCliente02.setLayout(panelCliente02Layout);
        panelCliente02Layout.setHorizontalGroup(
            panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCliente02Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(ftxtCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtRgIe, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(fTxtDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCliente02Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtEmail))
                .addGap(10, 10, 10)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fTxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addContainerGap())
        );
        panelCliente02Layout.setVerticalGroup(
            panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCliente02Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCliente02Layout.createSequentialGroup()
                        .addGroup(panelCliente02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ftxtCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fTxtDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRgIe))
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCliente02Layout.createSequentialGroup()
                        .addComponent(fTxtTelefone)
                        .addContainerGap())))
        );

        panelEnd.setBackground(new java.awt.Color(204, 204, 204));
        panelEnd.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setText("Logradouro*");

        txtRua.setEditable(false);
        txtRua.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRuaKeyPressed(evt);
            }
        });

        jLabel10.setText("Nº*");

        txtNum.setEditable(false);
        txtNum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumKeyPressed(evt);
            }
        });

        jLabel11.setText("Bairro*");

        txtBairro.setEditable(false);
        txtBairro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBairroKeyPressed(evt);
            }
        });

        jLabel12.setText("Município*");

        txtMunicipio.setEditable(false);
        txtMunicipio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMunicipioKeyPressed(evt);
            }
        });

        comEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO" }));
        comEstado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comEstadoKeyPressed(evt);
            }
        });

        jLabel13.setText("Estado*");

        jLabel14.setText("Complemento");

        txtComplemento.setEditable(false);
        txtComplemento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtComplementoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelEndLayout = new javax.swing.GroupLayout(panelEnd);
        panelEnd.setLayout(panelEndLayout);
        panelEndLayout.setHorizontalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEndLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(txtRua, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNum, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGap(10, 10, 10)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEndLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(17, 17, 17))
                    .addComponent(txtComplemento))
                .addContainerGap())
        );
        panelEndLayout.setVerticalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEndLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "CPF/CNPJ", "Nome/Razão Social", "Telefone"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableCliente.getTableHeader().setReorderingAllowed(false);
        tableCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableClienteMouseClicked(evt);
            }
        });
        tableCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableClienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableClienteKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableCliente);
        if (tableCliente.getColumnModel().getColumnCount() > 0) {
            tableCliente.getColumnModel().getColumn(0).setResizable(false);
            tableCliente.getColumnModel().getColumn(0).setPreferredWidth(2);
            tableCliente.getColumnModel().getColumn(1).setResizable(false);
            tableCliente.getColumnModel().getColumn(1).setPreferredWidth(10);
            tableCliente.getColumnModel().getColumn(2).setResizable(false);
            tableCliente.getColumnModel().getColumn(2).setPreferredWidth(290);
            tableCliente.getColumnModel().getColumn(3).setResizable(false);
            tableCliente.getColumnModel().getColumn(3).setPreferredWidth(6);
        }

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('t');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });
        btnRestaurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnRestaurarKeyPressed(evt);
            }
        });

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        btnGrupoDtAt.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("Desativados");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        btnGrupoDtAt.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("Ativos");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(radioDesativados)
                        .addGap(18, 18, 18)
                        .addComponent(radioAtivados))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRestaurar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRestaurar)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar))
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioDesativados)
                    .addComponent(radioAtivados))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(panelCliente02, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelCliente01, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelEnd, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 1, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCliente01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(panelCliente02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        btnIncluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnIncluirKeyPressed(evt);
            }
        });

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setMnemonic('A');
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnExcluir.setBackground(new java.awt.Color(204, 204, 204));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnExcluir.setMnemonic('E');
        btnExcluir.setText("Excluir");
        btnExcluir.setToolTipText("");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        btnExcluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExcluirKeyPressed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar (1).png"))); // NOI18N
        btnAtivar.setMnemonic('v');
        btnAtivar.setText("Ativar");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setMnemonic('d');
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnDesativar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAtivar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(157, 157, 157)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        controlBotao = 1;
        ct.campoEditavel(panelCliente01);
        ct.campoEditavel(panelCliente02);
        ct.campoEditavel(panelEnd);
        ct.clear(panelCliente01);
        ct.clear(panelCliente02);
        ct.clear(panelEnd);
        AtivaComboBox();

        tableCliente.setEnabled(false);
        tableCliente.setVisible(false);
        desativaBtnPrincipais();
        btnGravar.setEnabled(true);
        btnGravar.setVisible(true);
        btnCancelar.setVisible(true);
        btnCancelar.setEnabled(true);
        combTipoPessoa.requestFocus();
        btnDesativar.setEnabled(false);
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);

        fTxtDataNasc.setValue(null);
        fTxtDataNasc.setFormatterFactory(new DefaultFormatterFactory(dataMask));
        fTxtTelefone.setValue(null);
        fTxtTelefone.setFormatterFactory(new DefaultFormatterFactory(telefoneMask));
        ftxtCnpjCpf.setValue(null);
        ftxtCnpjCpf.setFormatterFactory(new DefaultFormatterFactory(CPFMask));
    }//GEN-LAST:event_btnIncluirActionPerformed

    private void combTipoPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combTipoPessoaActionPerformed
        opTipoPessoa = combTipoPessoa.getSelectedIndex();
        if (opTipoPessoa == 1) {

            txtRzSoc.setText("");
            txtNome.setEnabled(true);
            txtRzSoc.setEnabled(false);

            ftxtCnpjCpf.setValue(null);
            ftxtCnpjCpf.setFormatterFactory(new DefaultFormatterFactory(CPFMask));

        } else if (opTipoPessoa == 2) {

            txtRzSoc.setEnabled(true);
            txtNome.setEnabled(true);

            ftxtCnpjCpf.setValue(null);
            ftxtCnpjCpf.setFormatterFactory(new DefaultFormatterFactory(CNPJMask));

        }
    }//GEN-LAST:event_combTipoPessoaActionPerformed

    private void combTipoPessoaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_combTipoPessoaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            opTipoPessoa = combTipoPessoa.getSelectedIndex();
            if (opTipoPessoa == 1) {

                txtNome.setEnabled(true);
                txtRzSoc.setEnabled(false);

                ftxtCnpjCpf.setValue(null);
                ftxtCnpjCpf.setFormatterFactory(new DefaultFormatterFactory(CPFMask));
                txtNome.requestFocus();
            } else if (opTipoPessoa == 2) {

                txtRzSoc.setEnabled(true);
                txtNome.setEnabled(true);

                ftxtCnpjCpf.setValue(null);
                ftxtCnpjCpf.setFormatterFactory(new DefaultFormatterFactory(CNPJMask));
                txtNome.requestFocus();
            }
        }
    }//GEN-LAST:event_combTipoPessoaKeyPressed

    private void txtNomeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (opTipoPessoa == 1) {

                ftxtCnpjCpf.requestFocus();
            } else if (opTipoPessoa == 2) {

                txtRzSoc.requestFocus();
            }

        }
    }//GEN-LAST:event_txtNomeKeyPressed

    private void txtRzSocKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRzSocKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ftxtCnpjCpf.requestFocus();
        }

    }//GEN-LAST:event_txtRzSocKeyPressed

    private void btnIncluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnIncluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            controlBotao = 1;
            ct.campoEditavel(panelCliente01);
            ct.campoEditavel(panelCliente02);
            ct.campoEditavel(panelEnd);
            ct.clear(panelCliente01);
            ct.clear(panelCliente02);
            ct.clear(panelEnd);
            AtivaComboBox();
            tableCliente.setEnabled(false);
            tableCliente.setVisible(false);
            desativaBtnPrincipais();
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            combTipoPessoa.requestFocus();
        }
    }//GEN-LAST:event_btnIncluirKeyPressed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:
                switch (opTipoPessoa) {
                    case 1:
                        if (!conv.isValid(fTxtDataNasc.getText())) {
                            JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                            fTxtDataNasc.requestFocus();
                        } else {
                            if ((combTipoPessoa.getSelectedIndex() == 0) || txtNome.getText().trim().isEmpty() || masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                                    || txtRgIe.getText().trim().isEmpty() || masc.removeMascaraData(fTxtDataNasc.getText()).trim().isEmpty() || txtEmail.getText().trim().isEmpty()
                                    || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                                    || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty()
                                    || txtMunicipio.getText().trim().isEmpty() || (comEstado.getSelectedIndex() == 0)) {
                                JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                                combTipoPessoa.requestFocus();

                            } else {
                                cadastroP();

                            }

                        }
                        break;
                    case 2:
                        if (!conv.isValid(fTxtDataNasc.getText())) {
                            JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                            fTxtDataNasc.requestFocus();
                        } else {
                            if ((combTipoPessoa.getSelectedIndex() == 0) || txtRzSoc.getText().trim().isEmpty() || masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                                    || txtRgIe.getText().trim().isEmpty() || masc.removeMascaraData(fTxtDataNasc.getText()).trim().isEmpty() || txtEmail.getText().trim().isEmpty()
                                    || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                                    || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty()
                                    || txtMunicipio.getText().trim().isEmpty() || (comEstado.getSelectedIndex() == 0)) {
                                JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                                combTipoPessoa.requestFocus();

                            } else {
                                cadastroPj();

                            }
                        }
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "INFORME O TIPO DE PESSOA");
                        combTipoPessoa.requestFocus();
                        break;
                }
                break;

            case 2:
                alteraCliente();
                break;
        }
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);

        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        fTxtDataNasc.setText(null);
        ct.clear(panelCliente01);
        ct.clear(panelCliente02);
        ct.clear(panelEnd);
        tableCliente.setVisible(true);
        tableCliente.setEnabled(true);
        ct.campoNaoEditavel(panelCliente01);
        ct.campoNaoEditavel(panelCliente02);
        ct.campoNaoEditavel(panelEnd);
        desativaComboBox();
        preencheTabela();
        SelecionaPrimeiraLinha();
        btnIncluir.requestFocus();
        btnDesativar.setEnabled(true);


    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtComplementoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtComplementoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnGravar.requestFocus();
        }
    }//GEN-LAST:event_txtComplementoKeyPressed

    private void tableClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableClienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int row = tableCliente.getSelectedRow();
            String valor = tableCliente.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tableClienteKeyPressed

    private void tableClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableClienteMouseClicked
        listaTextFiel();
    }//GEN-LAST:event_tableClienteMouseClicked

    private void tableClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableClienteKeyReleased
        listaTextFiel();
    }//GEN-LAST:event_tableClienteKeyReleased

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed
        ct.clear(panelCliente01);
        ct.clear(panelCliente02);
        ct.clear(panelEnd);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        preencheTabela();
        desativaComboBox();
        btnAtivar.setVisible(false);
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
        btnIncluir.setEnabled(true);
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        ct.clear(panelCliente01);
        ct.clear(panelCliente02);
        ct.clear(panelEnd);
        txtPesquisa.setText("");
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.requestFocus();
        btnIncluir.setEnabled(false);
        desativaComboBox();
        radioDesativados.setVisible(true);
        radioAtivados.setVisible(true);
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnRestaurarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnRestaurarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelCliente01);
            ct.clear(panelCliente02);
            ct.clear(panelEnd);
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);
            preencheTabela();
            AtivaComboBox();

        }
    }//GEN-LAST:event_btnRestaurarKeyPressed

    private void ftxtCnpjCpfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxtCnpjCpfKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtRgIe.requestFocus();
        }
    }//GEN-LAST:event_ftxtCnpjCpfKeyPressed

    private void txtRgIeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRgIeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fTxtDataNasc.requestFocus();
        }
    }//GEN-LAST:event_txtRgIeKeyPressed

    private void fTxtDataNascKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fTxtDataNascKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            try {
                LocalDate datenc = conv.converterStringtoLocalDate(fTxtDataNasc.getText());
                String dataAtual = dtSis.dataSis();

                LocalDate dtAt = conv.converterStringtoLocalDate(dataAtual);
                int anoNasc = datenc.getYear();
                int anoAt = dtAt.getYear();
                int result = anoAt - anoNasc;

                if (datenc.isAfter(dtAt)) {
                    JOptionPane.showMessageDialog(null, "DATA INFORMADA É INVALIDA - MAIOR QUE DATA ATUAL");
                    fTxtDataNasc.setText("");
                    fTxtDataNasc.requestFocus();
                } else {
                    txtEmail.requestFocus();
                }
            } catch (DateTimeParseException e) {
                JOptionPane.showMessageDialog(null, "DATA INFORMADA É INVALIDA");
                fTxtDataNasc.setText("");
                fTxtDataNasc.requestFocus();
            }

        }
    }//GEN-LAST:event_fTxtDataNascKeyPressed

    private void txtEmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (ct.validarEmail(txtEmail.getText())) {
                fTxtTelefone.requestFocus();
            } else {
                txtEmail.requestFocus();

                JOptionPane.showMessageDialog(null, "E-MAIL INFORMADO É INVALIDO");
            }

        }
    }//GEN-LAST:event_txtEmailKeyPressed

    private void fTxtTelefoneKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fTxtTelefoneKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtRua.requestFocus();
        }
    }//GEN-LAST:event_fTxtTelefoneKeyPressed

    private void txtRuaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRuaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtNum.requestFocus();
        }
    }//GEN-LAST:event_txtRuaKeyPressed

    private void txtNumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtBairro.requestFocus();
        }
    }//GEN-LAST:event_txtNumKeyPressed

    private void txtBairroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBairroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtMunicipio.requestFocus();
        }
    }//GEN-LAST:event_txtBairroKeyPressed

    private void txtMunicipioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMunicipioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comEstado.requestFocus();
        }
    }//GEN-LAST:event_txtMunicipioKeyPressed

    private void comEstadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comEstadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtComplemento.requestFocus();
        }
    }//GEN-LAST:event_comEstadoKeyPressed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (radioAtivados.isSelected()) {
                preencheTabelaPesquisa();
            } else if (radioDesativados.isSelected()) {

                preencheTabelaPesquisaDesativados();
            }
        }
    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            switch (controlBotao) {
                case 1:
                    switch (opTipoPessoa) {
                        case 1:
                            if (!conv.isValid(fTxtDataNasc.getText())) {
                                JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                                fTxtDataNasc.requestFocus();
                            } else {
                                if ((combTipoPessoa.getSelectedIndex() == 0) || txtNome.getText().trim().isEmpty() || masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                                        || masc.removeMascaraData(fTxtDataNasc.getText()).trim().isEmpty() || txtEmail.getText().trim().isEmpty()
                                        || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                                        || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty()
                                        || txtMunicipio.getText().trim().isEmpty() || (comEstado.getSelectedIndex() == 0)) {
                                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                                    combTipoPessoa.requestFocus();

                                } else {
                                    cadastroP();

                                }

                            }
                            break;

                        case 2:
                            if (!conv.isValid(fTxtDataNasc.getText())) {
                                JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                                fTxtDataNasc.requestFocus();
                            } else {
                                if ((combTipoPessoa.getSelectedIndex() == 0) || txtRzSoc.getText().trim().isEmpty() || masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                                        || masc.removeMascaraData(fTxtDataNasc.getText()).trim().isEmpty() || txtEmail.getText().trim().isEmpty()
                                        || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                                        || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty()
                                        || txtMunicipio.getText().trim().isEmpty() || (comEstado.getSelectedIndex() == 0)) {
                                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                                    combTipoPessoa.requestFocus();

                                } else {
                                    cadastroPj();
                                }
                            }
                            break;
                        default:
                            JOptionPane.showMessageDialog(null, "INFORME O TIPO DE PESSOA");
                            combTipoPessoa.requestFocus();
                            break;
                    }
                    break;

                case 2:
                    alteraCliente();
                    break;
            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);

            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setVisible(false);
            btnCancelar.setEnabled(false);
            fTxtDataNasc.setText(null);
            ct.clear(panelCliente01);
            ct.clear(panelCliente02);
            ct.clear(panelEnd);
            tableCliente.setVisible(true);
            tableCliente.setEnabled(true);
            ct.campoNaoEditavel(panelCliente01);
            ct.campoNaoEditavel(panelCliente02);
            ct.campoNaoEditavel(panelEnd);
            desativaComboBox();
            preencheTabela();
            SelecionaPrimeiraLinha();
            btnIncluir.requestFocus();

        }
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed

        try {
            codigoCli = Integer.parseInt(itemTabela());

            controlBotao = 2;
            ct.campoEditavel(panelCliente01);
            ct.campoEditavel(panelCliente02);
            ct.campoEditavel(panelEnd);
            comEstado.setEnabled(true);
            combTipoPessoa.setEnabled(true);
            tableCliente.setEnabled(false);
            tableCliente.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);

            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setVisible(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            combTipoPessoa.requestFocus();

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
        }

    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            codigoCli = Integer.parseInt(itemTabela());
            if (!cli.consultaStatusCliente(codigoCli)) {
                tableCliente.setEnabled(false);
                tableCliente.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);

                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                radioAtivados.setVisible(false);
                radioDesativados.setVisible(false);
                excluiCliente();
            } else {
                JOptionPane.showMessageDialog(null, "DESATIVE O CLIENTE ANTES DE EXCLUIR!");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnExcluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExcluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            try {
                codigoCli = Integer.parseInt(itemTabela());
                if (!cli.consultaStatusCliente(codigoCli)) {
                    tableCliente.setEnabled(false);
                    tableCliente.setVisible(false);
                    btnIncluir.setEnabled(false);
                    btnAlterar.setEnabled(false);
                    btnExcluir.setEnabled(false);

                    btnRestaurar.setEnabled(false);
                    btnRestaurar.setVisible(false);
                    btnPesquisar.setVisible(false);
                    btnPesquisar.setEnabled(false);
                    txtPesquisa.setVisible(false);
                    radioAtivados.setVisible(false);
                    radioDesativados.setVisible(false);

                    excluiCliente();
                } else {
                    JOptionPane.showMessageDialog(null, "DESATIVE O CLIENTE ANTES DE EXCLUIR!");
                }

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnExcluirKeyPressed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            try {
                codigoCli = Integer.parseInt(itemTabela());

                controlBotao = 2;
                ct.campoEditavel(panelCliente01);
                ct.campoEditavel(panelCliente02);
                ct.campoEditavel(panelEnd);
                AtivaComboBox();
                tableCliente.setEnabled(false);
                tableCliente.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);

                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                btnGravar.setEnabled(true);
                btnGravar.setVisible(true);
                btnCancelar.setVisible(true);
                btnCancelar.setEnabled(true);
                combTipoPessoa.requestFocus();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        clienteAtDt = "ativado";
        try {
            int row = tableCliente.getSelectedRow();
            String valor = tableCliente.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                cli.AtivaDesativa(clienteAtDt, cod);
                JOptionPane.showMessageDialog(null, "CLIENTE FOI ATIVADO COM SUCESSO");
            }

            if (!valor.equals("")) {

                preencheTabelaDesativados();
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed
        if (cli.VerificaCliente()) {

            btnDesativar.setEnabled(false);
        } else {
            preencheTabelaDesativados();
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
        }
    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed
        preencheTabela();
        btnAtivar.setVisible(false);
        btnAtivar.setEnabled(false);
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_radioAtivadosActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        clienteAtDt = "desativado";
        try {
            int row = tableCliente.getSelectedRow();
            String valor = tableCliente.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            if (cli.pendenciaCliente(cod)) {
                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    cli.AtivaDesativa(clienteAtDt, cod);
                    JOptionPane.showMessageDialog(null, "CLIENTE FOI DESATIVADO COM SUCESSO");
                }

                if (!valor.equals("")) {

                    preencheTabela();
                }
            } else {
                JOptionPane.showMessageDialog(null, "CLIENTE NÃO PODE SER DESATIVADO! ELE ESTÁ SENDO UTILIZADO");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM CLIENTE PRIMEIRO!");
        }
    }//GEN-LAST:event_btnDesativarActionPerformed

    public void listaTextFiel() {

        int row = tableCliente.getSelectedRow();
        String strCod = tableCliente.getValueAt(row, 0).toString();
        int codCliente;
        codCliente = Integer.parseInt(strCod);
        Cliente c;
        c = cli.consultaCliente(codCliente);
        if (cli.buscaCliente(codCliente)) {

            if (c instanceof PessoaFisica) {
                ct.clear(panelCliente01);
                ct.clear(panelCliente02);
                ct.clear(panelEnd);
                int num = ((PessoaFisica) c).getTipoPessoa();
                combTipoPessoa.setSelectedIndex(num);
                ftxtCnpjCpf.setText(((PessoaFisica) c).getCpfCnpj().toUpperCase());
                txtNome.setText(((PessoaFisica) c).getNome().toUpperCase());
                txtRgIe.setText(((PessoaFisica) c).getRgIe().toUpperCase());
                fTxtDataNasc.setText(formatador.format(((PessoaFisica) c).getDataNascimento()));
                txtEmail.setText(((PessoaFisica) c).getEmail().toUpperCase());
                fTxtTelefone.setText(((PessoaFisica) c).getTelefone().toUpperCase());
                txtRua.setText(((PessoaFisica) c).getLogradouro().toUpperCase());
                txtNum.setText(((PessoaFisica) c).getNumero().toUpperCase());
                txtBairro.setText(((PessoaFisica) c).getBairro().toUpperCase());
                txtMunicipio.setText(((PessoaFisica) c).getMunicipio().toUpperCase());
                comEstado.setSelectedItem(((PessoaFisica) c).getEstado().toUpperCase());
                txtComplemento.setText(((PessoaFisica) c).getComplemento().toUpperCase());

            } else if (c instanceof PessoaJuridica) {
                ct.clear(panelCliente01);
                ct.clear(panelCliente02);
                ct.clear(panelEnd);
                int num = ((PessoaJuridica) c).getTipoPessoa();
                combTipoPessoa.setSelectedIndex(num);
                txtRzSoc.setText(((PessoaJuridica) c).getRazaoSocial().toUpperCase());
                txtNome.setText(((PessoaJuridica) c).getNomeFantasia().toUpperCase());
                ftxtCnpjCpf.setText(((PessoaJuridica) c).getCpfCnpj().toUpperCase());
                txtRgIe.setText(((PessoaJuridica) c).getRgIe().toUpperCase());
                fTxtDataNasc.setText(formatador.format(((PessoaJuridica) c).getDataNascimento()));
                txtEmail.setText(((PessoaJuridica) c).getEmail().toUpperCase());
                fTxtTelefone.setText(((PessoaJuridica) c).getTelefone().toUpperCase());
                txtRua.setText(((PessoaJuridica) c).getLogradouro().toUpperCase());
                txtNum.setText(((PessoaJuridica) c).getNumero().toUpperCase());
                txtBairro.setText(((PessoaJuridica) c).getBairro().toUpperCase());
                txtMunicipio.setText(((PessoaJuridica) c).getMunicipio().toUpperCase());
                comEstado.setSelectedItem(((PessoaJuridica) c).getEstado().toUpperCase());
                txtComplemento.setText(((PessoaJuridica) c).getComplemento().toUpperCase());

            }

        }
    }

    public String itemTabela() {

        int row = tableCliente.getSelectedRow();
        String valor = tableCliente.getValueAt(row, 0).toString();

        return valor;
    }

    public void preencheTabelaPesquisa() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableCliente.getModel();
        modeloTab.setNumRows(0);

        for (Cliente c : cli.buscaClienteTabela(txtPesquisa.getText().toUpperCase())) {

            if (c.getTipoPessoa() == 1) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaFisica) c).getCpfCnpj(),
                    ((PessoaFisica) c).getNome().toUpperCase(),
                    c.getTelefone()

                });
            } else if (c.getTipoPessoa() == 2) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaJuridica) c).getCpfCnpj(),
                    ((PessoaJuridica) c).getRazaoSocial().toUpperCase(),
                    c.getTelefone()

                });
            }

        }

    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableCliente.getModel();
        modeloTab.setNumRows(0);

        for (Cliente c : cli.preencheTabela()) {

            if (c.getTipoPessoa() == 1) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaFisica) c).getCpfCnpj(),
                    ((PessoaFisica) c).getNome().toUpperCase(),
                    c.getTelefone()

                });
            } else if (c.getTipoPessoa() == 2) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaJuridica) c).getCpfCnpj(),
                    ((PessoaJuridica) c).getRazaoSocial().toUpperCase(),
                    c.getTelefone()

                });
            }

        }

    }

    public void preencheTabelaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableCliente.getModel();
        modeloTab.setNumRows(0);

        for (Cliente c : cli.preencheTabelaDesativados()) {

            if (c.getTipoPessoa() == 1) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaFisica) c).getCpfCnpj(),
                    ((PessoaFisica) c).getNome().toUpperCase(),
                    c.getTelefone()

                });
            } else if (c.getTipoPessoa() == 2) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaJuridica) c).getCpfCnpj(),
                    ((PessoaJuridica) c).getRazaoSocial().toUpperCase(),
                    c.getTelefone()

                });
            }

        }

    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableCliente.getModel();
        modeloTab.setNumRows(0);

        for (Cliente c : cli.buscaClienteTabelaDesativados(txtPesquisa.getText().toUpperCase())) {

            if (c.getTipoPessoa() == 1) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaFisica) c).getCpfCnpj(),
                    ((PessoaFisica) c).getNome().toUpperCase(),
                    c.getTelefone()

                });
            } else if (c.getTipoPessoa() == 2) {

                modeloTab.addRow(new Object[]{
                    c.getCodigo(),
                    ((PessoaJuridica) c).getCpfCnpj(),
                    ((PessoaJuridica) c).getRazaoSocial().toUpperCase(),
                    c.getTelefone()

                });
            }

        }

    }

    private void cadastroP() {
        boolean flag;

        if (opTipoPessoa == 1) {
            PessoaFisica pf = new PessoaFisica();
            pf.setNome(txtNome.getText());
            pf.setCpfCnpj(masc.removeMascaraCpf(ftxtCnpjCpf.getText()));

            pf.setRgIe(txtRgIe.getText());
            pf.setTipoPessoa(combTipoPessoa.getSelectedIndex());
            pf.setDataNascimento(conv.converterStringtoLocalDate(fTxtDataNasc.getText()));
            pf.setEmail(txtEmail.getText());
            pf.setTelefone(fTxtTelefone.getText());
            pf.setLogradouro(txtRua.getText());
            pf.setNumero(txtNum.getText());
            pf.setBairro(txtBairro.getText());
            pf.setMunicipio(txtMunicipio.getText());
            pf.setEstado(comEstado.getSelectedItem().toString());
            pf.setComplemento(txtComplemento.getText());

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = cli.addClientePessoaFisica(pf);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "CLIENTE CADASTRADO COM SUCESSO");

                        ct.clear(panelCliente01);
                        ct.clear(panelCliente02);
                        ct.clear(panelEnd);
                        desativaComboBox();
                        ct.campoNaoEditavel(panelCliente01);
                        ct.campoNaoEditavel(panelCliente02);
                        ct.campoNaoEditavel(panelEnd);
                        tableCliente.setEnabled(true);
                        tableCliente.setVisible(true);
                        preencheTabela();
                        SelecionaPrimeiraLinha();
                        ativaBtnPrincipais();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnDesativar.setEnabled(true);

                    } else if (flag == false) {

                        btnIncluir.setEnabled(false);
                        btnAlterar.setEnabled(false);
                        btnExcluir.setEnabled(false);

                        btnGravar.setEnabled(true);
                        btnGravar.setVisible(true);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

                }

            } else if (resp == 1) {
                ct.clear(panelCliente01);
                ct.clear(panelCliente02);
                ct.clear(panelEnd);
                ct.campoNaoEditavel(panelCliente01);
                ct.campoNaoEditavel(panelCliente02);
                ct.campoNaoEditavel(panelEnd);
                desativaComboBox();
                tableCliente.setEnabled(true);
                tableCliente.setVisible(true);
                preencheTabela();
                SelecionaPrimeiraLinha();
                ativaBtnPrincipais();
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);
                btnDesativar.setEnabled(true);
            }
        }
    }

    private void cadastroPj() {
        boolean flag;

        if (opTipoPessoa == 2) {
            PessoaJuridica pj = new PessoaJuridica();

            pj.setCpfCnpj(ftxtCnpjCpf.getText());
            pj.setRazaoSocial(txtRzSoc.getText());
            pj.setNomeFantasia(txtNome.getText());
            pj.setCpfCnpj(masc.removeMascaraCpf(ftxtCnpjCpf.getText()));
            pj.setRgIe(txtRgIe.getText());
            pj.setTipoPessoa(combTipoPessoa.getSelectedIndex());
            pj.setDataNascimento(conv.converterStringtoLocalDate(fTxtDataNasc.getText()));
            pj.setEmail(txtEmail.getText());
            pj.setTelefone(fTxtTelefone.getText());
            pj.setLogradouro(txtRua.getText());
            pj.setNumero(txtNum.getText());
            pj.setBairro(txtBairro.getText());
            pj.setMunicipio(txtMunicipio.getText());
            pj.setEstado(comEstado.getSelectedItem().toString());
            pj.setComplemento(txtComplemento.getText());

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = cli.addClientePessoaJuridica(pj);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "CLIENTE CADASTRADO COM SUCESSO");

                        ct.clear(panelCliente01);
                        ct.clear(panelCliente02);
                        ct.clear(panelEnd);
                        desativaComboBox();
                        ct.campoNaoEditavel(panelCliente01);
                        ct.campoNaoEditavel(panelCliente02);
                        ct.campoNaoEditavel(panelEnd);
                        tableCliente.setEnabled(true);
                        tableCliente.setVisible(true);
                        preencheTabela();
                        SelecionaPrimeiraLinha();
                        ativaBtnPrincipais();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnDesativar.setEnabled(true);

                    } else if (flag == false) {
                        btnIncluir.setEnabled(false);
                        btnAlterar.setEnabled(false);
                        btnExcluir.setEnabled(false);

                        btnGravar.setEnabled(true);
                        btnGravar.setVisible(true);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

                }

            } else if (resp == 1) {

                ct.clear(panelCliente01);
                ct.clear(panelCliente02);
                ct.clear(panelEnd);
                desativaComboBox();
                ct.campoNaoEditavel(panelCliente01);
                ct.campoNaoEditavel(panelCliente02);
                ct.campoNaoEditavel(panelEnd);
                tableCliente.setEnabled(true);
                tableCliente.setVisible(true);
                preencheTabela();
                SelecionaPrimeiraLinha();
                ativaBtnPrincipais();
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setVisible(false);
                btnCancelar.setEnabled(false);

            }
        }
    }

    private void alteraCliente() {

        boolean flag;
        switch (opTipoPessoa) {
            case 1:
                if (txtNome.getText().trim().isEmpty() || masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                        || txtRgIe.getText().trim().isEmpty() || masc.removeMascaraData(fTxtDataNasc.getText()).trim().isEmpty() || txtEmail.getText().trim().isEmpty()
                        || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                        || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty()
                        || txtMunicipio.getText().trim().isEmpty() || (comEstado.getSelectedIndex() == 0)
                        || txtComplemento.getText().trim().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    txtNome.requestFocus();
                } else {

                    if (!conv.isValid(fTxtDataNasc.getText())) {
                        JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                        fTxtDataNasc.requestFocus();

                    } else {
                        PessoaFisica pf = new PessoaFisica();

                        pf.setNome(txtNome.getText());
                        pf.setCpfCnpj(masc.removeMascaraCpf(ftxtCnpjCpf.getText()));
                        pf.setRgIe(txtRgIe.getText());
                        pf.setTipoPessoa(combTipoPessoa.getSelectedIndex());
                        pf.setDataNascimento(conv.converterStringtoLocalDate(fTxtDataNasc.getText()));
                        pf.setEmail(txtEmail.getText());
                        pf.setTelefone(fTxtTelefone.getText());
                        pf.setLogradouro(txtRua.getText());
                        pf.setNumero(txtNum.getText());
                        pf.setBairro(txtBairro.getText());
                        pf.setMunicipio(txtMunicipio.getText());
                        pf.setEstado(comEstado.getSelectedItem().toString());
                        pf.setComplemento(txtComplemento.getText());
                        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {
                            try {
                                flag = cli.alterarClienteF(pf, codigoCli);
                                if (flag == true) {
                                    JOptionPane.showMessageDialog(null, "CLIENTE ALTERADO COM SUCESSO");

                                    ct.clear(panelCliente01);
                                    ct.clear(panelCliente02);
                                    ct.clear(panelEnd);
                                    desativaComboBox();
                                    ct.campoNaoEditavel(panelCliente01);
                                    ct.campoNaoEditavel(panelCliente02);
                                    ct.campoNaoEditavel(panelEnd);
                                    tableCliente.setEnabled(true);
                                    tableCliente.setVisible(true);
                                    preencheTabela();
                                    SelecionaPrimeiraLinha();
                                    ativaBtnPrincipais();
                                    btnGravar.setEnabled(false);
                                    btnGravar.setVisible(false);
                                    btnCancelar.setVisible(false);
                                    btnCancelar.setEnabled(false);
                                    btnDesativar.setEnabled(true);
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (resp == 1) {

                            ct.clear(panelCliente01);
                            ct.clear(panelCliente02);
                            ct.clear(panelEnd);
                            desativaComboBox();
                            ct.campoNaoEditavel(panelCliente01);
                            ct.campoNaoEditavel(panelCliente02);
                            ct.campoNaoEditavel(panelEnd);
                            tableCliente.setEnabled(true);
                            tableCliente.setVisible(true);
                            preencheTabela();
                            SelecionaPrimeiraLinha();
                            ativaBtnPrincipais();
                            btnGravar.setEnabled(false);
                            btnGravar.setVisible(false);
                            btnCancelar.setVisible(false);
                            btnCancelar.setEnabled(false);
                            btnDesativar.setEnabled(true);

                        }
                        desativaComboBox();
                        preencheTabela();
                        SelecionaPrimeiraLinha();
                        tableCliente.setEnabled(true);
                        tableCliente.setVisible(true);
                        ativaBtnPrincipais();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);

                    }

                }
                break;
            case 2:
                if (!conv.isValid(fTxtDataNasc.getText())) {
                    JOptionPane.showMessageDialog(null, "DATA INVALIDA");
                    fTxtDataNasc.requestFocus();
                } else {
                    if (masc.removeMascaraCpf(ftxtCnpjCpf.getText()).trim().isEmpty()
                            || txtRzSoc.getText().trim().isEmpty() || txtRgIe.getText().trim().isEmpty()
                            || txtEmail.getText().trim().isEmpty() || masc.removeMascaraTelefone(fTxtTelefone.getText()).trim().isEmpty()
                            || txtRua.getText().trim().isEmpty() || txtNum.getText().trim().isEmpty() || txtMunicipio.getText().trim().isEmpty()
                            || (comEstado.getSelectedIndex() == 0) || txtComplemento.getText().trim().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");

                    } else {
                        PessoaJuridica pj = new PessoaJuridica();

                        pj.setCpfCnpj(masc.removeMascaraCpf(ftxtCnpjCpf.getText()));
                        pj.setRgIe(txtRgIe.getText());
                        pj.setRazaoSocial(txtRzSoc.getText());
                        pj.setTipoPessoa(combTipoPessoa.getSelectedIndex());
                        pj.setDataNascimento(conv.converterStringtoLocalDate(fTxtDataNasc.getText()));
                        pj.setEmail(txtEmail.getText());
                        pj.setTelefone(fTxtTelefone.getText());
                        pj.setLogradouro(txtRua.getText());
                        pj.setNumero(txtNum.getText());
                        pj.setBairro(txtBairro.getText());
                        pj.setMunicipio(txtMunicipio.getText());
                        pj.setEstado(comEstado.getSelectedItem().toString());
                        pj.setComplemento(txtComplemento.getText());
                        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {
                            try {
                                flag = cli.alterarClienteJ(pj, codigoCli);
                                if (flag == true) {
                                    JOptionPane.showMessageDialog(null, "CLIENTE ALTERADO COM SUCESSO");

                                    ct.clear(panelCliente01);
                                    ct.clear(panelCliente02);
                                    ct.clear(panelEnd);
                                    desativaComboBox();
                                    ct.campoNaoEditavel(panelCliente01);
                                    ct.campoNaoEditavel(panelCliente02);
                                    ct.campoNaoEditavel(panelEnd);
                                    tableCliente.setEnabled(true);
                                    tableCliente.setVisible(true);
                                    preencheTabela();
                                    SelecionaPrimeiraLinha();
                                    ativaBtnPrincipais();
                                    btnGravar.setEnabled(false);
                                    btnGravar.setVisible(false);
                                    btnCancelar.setVisible(false);
                                    btnCancelar.setEnabled(false);

                                }
                            } catch (Exception ex) {
                                Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (resp == 1) {

                            ct.clear(panelCliente01);
                            ct.clear(panelCliente02);
                            ct.clear(panelEnd);
                            desativaComboBox();
                            ct.campoNaoEditavel(panelCliente01);
                            ct.campoNaoEditavel(panelCliente02);
                            ct.campoNaoEditavel(panelEnd);
                            tableCliente.setEnabled(true);
                            tableCliente.setVisible(true);
                            preencheTabela();
                            SelecionaPrimeiraLinha();
                            ativaBtnPrincipais();
                            btnGravar.setEnabled(false);
                            btnGravar.setVisible(false);
                            btnCancelar.setVisible(false);
                            btnCancelar.setEnabled(false);

                        }

                        preencheTabela();
                        SelecionaPrimeiraLinha();
                        tableCliente.setEnabled(true);
                        tableCliente.setVisible(true);
                        ativaBtnPrincipais();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);

                    }

                }
                break;
            default:
                JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                txtNome.requestFocus();
                break;
        }

    }

    public void excluiCliente() {
        boolean flag;
        if (cli.pendenciaCliente(codigoCli)) {
            if (cli.buscaCliente(codigoCli)) {
                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DO CLIENTE", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {

                    flag = cli.excluirCliente(codigoCli);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "CLIENTE EXCLUÍDO COM SUCESSO");

                        ct.clear(panelCliente01);
                        ct.clear(panelCliente02);
                        ct.clear(panelEnd);
                        desativaComboBox();
                        ct.campoNaoEditavel(panelCliente01);
                        ct.campoNaoEditavel(panelCliente02);
                        ct.campoNaoEditavel(panelEnd);
                        tableCliente.setEnabled(true);
                        tableCliente.setVisible(true);
                        preencheTabela();
                        SelecionaPrimeiraLinha();
                        ativaBtnPrincipais();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setVisible(false);
                        btnCancelar.setEnabled(false);

                    }
                } else if (resp == 1) {

                    ct.clear(panelCliente01);
                    ct.clear(panelCliente02);
                    ct.clear(panelEnd);
                    desativaComboBox();
                    ct.campoNaoEditavel(panelCliente01);
                    ct.campoNaoEditavel(panelCliente02);
                    ct.campoNaoEditavel(panelEnd);
                    tableCliente.setEnabled(true);
                    tableCliente.setVisible(true);
                    preencheTabela();
                    SelecionaPrimeiraLinha();
                    ativaBtnPrincipais();

                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setVisible(false);
                    btnCancelar.setEnabled(false);

                }

            } else {
                JOptionPane.showMessageDialog(null, "NÃO EXISTE CLIENTE COM ESTE CÓDIGO");

                ct.clear(panelCliente01);
                ct.clear(panelCliente02);
                ct.clear(panelEnd);
                desativaComboBox();
                ct.campoNaoEditavel(panelCliente01);
                ct.campoNaoEditavel(panelCliente02);
                ct.campoNaoEditavel(panelEnd);
                tableCliente.setEnabled(true);
                tableCliente.setVisible(true);
                preencheTabela();
                SelecionaPrimeiraLinha();
                ativaBtnPrincipais();
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setVisible(false);
                btnCancelar.setEnabled(false);

            }
        } else {

            JOptionPane.showMessageDialog(null, "CLIENTE POSSUI VEÍCULO CADASTRADO E NÃO PODE SER EXCLUÍDO");
            ct.clear(panelCliente01);
            ct.clear(panelCliente02);
            ct.clear(panelEnd);
            ct.campoNaoEditavel(panelCliente01);
            ct.campoNaoEditavel(panelCliente02);
            ct.campoNaoEditavel(panelEnd);
            tableCliente.setEnabled(true);
            tableCliente.setVisible(true);
            preencheTabela();
            SelecionaPrimeiraLinha();
            ativaBtnPrincipais();
        }

    }

    public void desativaBtnPrincipais() {
        btnIncluir.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);

        btnDesativar.setEnabled(false);
        btnAtivar.setEnabled(false);
        btnPesquisar.setVisible(false);
        btnPesquisar.setEnabled(false);
        btnRestaurar.setEnabled(false);
        btnRestaurar.setVisible(false);
        txtPesquisa.setVisible(false);
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);
    }

    public void ativaBtnPrincipais() {
        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);

        btnDesativar.setEnabled(true);
        btnAtivar.setEnabled(true);
        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);

    }

    public void desativaComboBox() {

        combTipoPessoa.setEnabled(false);
        comEstado.setEnabled(false);
        combTipoPessoa.setSelectedIndex(0);
        comEstado.setSelectedIndex(0);
    }

    public void AtivaComboBox() {
        combTipoPessoa.setEnabled(true);
        comEstado.setEnabled(true);
        combTipoPessoa.setSelectedIndex(0);
        comEstado.setSelectedIndex(0);
    }

    public void SelecionaPrimeiraLinha() {
        tableCliente.clearSelection();
        tableCliente.changeSelection(0, 0, false, false);
        tableCliente.requestFocus();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.ButtonGroup btnGrupoDtAt;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.JComboBox<String> comEstado;
    private javax.swing.JComboBox<String> combTipoPessoa;
    private javax.swing.JFormattedTextField fTxtDataNasc;
    private javax.swing.JFormattedTextField fTxtTelefone;
    private javax.swing.JFormattedTextField ftxtCnpjCpf;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelCliente01;
    private javax.swing.JPanel panelCliente02;
    private javax.swing.JPanel panelEnd;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tableCliente;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JTextField txtComplemento;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtMunicipio;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNum;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JTextField txtRgIe;
    private javax.swing.JTextField txtRua;
    private javax.swing.JTextField txtRzSoc;
    // End of variables declaration//GEN-END:variables
}
