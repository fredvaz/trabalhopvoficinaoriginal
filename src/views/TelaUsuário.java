package views;

import controllers.LoginDAO;

import help.CampoTxt;
import help.LimitaNroCaracteres;
import help.Mascara;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import models.Usuario;

public final class TelaUsuário extends javax.swing.JDialog {

    private final LoginDAO logDAO;
    private final CampoTxt ct;
    private final Mascara masc = new Mascara();
    private int controlBotao;
    private MaskFormatter CPFMask;
    int codUsuario;
    String opItem;
    String userAtDt;
    LoginDAO log = new LoginDAO();

    public TelaUsuário(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ct = new CampoTxt();

        try {
            CPFMask = new MaskFormatter("###.###.###-##");
            ftxCpf.setFormatterFactory(new DefaultFormatterFactory(CPFMask));
        } catch (ParseException ex) {
            Logger.getLogger(TelaUsuário.class.getName()).log(Level.SEVERE, null, ex);
        }
        ftxCpf.setValue(null);

        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);
        controlBotao = 0;
        txtNome.setDocument(new LimitaNroCaracteres(38));
        desativaComboBox();
        codUsuario = 0;
        DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();
        tabelaUsuario.setRowSorter(new TableRowSorter(modeloTab));
        logDAO = new LoginDAO();
        preencheTabela();

        if (log.VerificaUsuario() == false) {
            btnLogin.setEnabled(false);
            btnLogin.setVisible(false);
        }
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);

        
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
        btnAtivar.setVisible(false);

        if (!logDAO.VerificaUsuario()) {

            tabelaUsuario.requestFocus();
            tabelaUsuario.changeSelection(0, 0, false, false);
        } else if (!logDAO.VerificaUsuario()) {

            btnPesquisar.setEnabled(false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupAtDt = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelprincipal = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        ftxCpf = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtConfSenha = new javax.swing.JPasswordField();
        txtSenha = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaUsuario = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        btnRestaurar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        radioDesativados = new javax.swing.JRadioButton();
        radioAtivados = new javax.swing.JRadioButton();
        panelGrupo = new javax.swing.JPanel();
        jcomboGrupo = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Cadastro de Usuários");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelprincipal.setBackground(new java.awt.Color(204, 204, 204));
        panelprincipal.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Nome*");

        txtNome.setEditable(false);
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeKeyPressed(evt);
            }
        });

        jLabel3.setText("CPF*");

        ftxCpf.setEditable(false);
        ftxCpf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ftxCpfKeyPressed(evt);
            }
        });

        jLabel4.setText("Senha*");

        jLabel1.setText("Confirmação Senha*");

        txtConfSenha.setEditable(false);
        txtConfSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtConfSenhaKeyPressed(evt);
            }
        });

        txtSenha.setEditable(false);
        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSenhaKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelprincipalLayout = new javax.swing.GroupLayout(panelprincipal);
        panelprincipal.setLayout(panelprincipalLayout);
        panelprincipalLayout.setHorizontalGroup(
            panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelprincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtNome))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(ftxCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtConfSenha))
                .addContainerGap())
        );
        panelprincipalLayout.setVerticalGroup(
            panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelprincipalLayout.createSequentialGroup()
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelprincipalLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelprincipalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtConfSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ftxCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tabelaUsuario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Cpf", "Grupo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaUsuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaUsuarioMouseClicked(evt);
            }
        });
        tabelaUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaUsuarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaUsuarioKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaUsuario);
        if (tabelaUsuario.getColumnModel().getColumnCount() > 0) {
            tabelaUsuario.getColumnModel().getColumn(0).setResizable(false);
            tabelaUsuario.getColumnModel().getColumn(0).setPreferredWidth(8);
            tabelaUsuario.getColumnModel().getColumn(1).setResizable(false);
            tabelaUsuario.getColumnModel().getColumn(1).setPreferredWidth(120);
            tabelaUsuario.getColumnModel().getColumn(2).setResizable(false);
            tabelaUsuario.getColumnModel().getColumn(2).setPreferredWidth(60);
            tabelaUsuario.getColumnModel().getColumn(3).setResizable(false);
            tabelaUsuario.getColumnModel().getColumn(3).setPreferredWidth(30);
        }

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('T');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });
        btnRestaurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnRestaurarKeyPressed(evt);
            }
        });

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        btnPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPesquisarKeyPressed(evt);
            }
        });

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroupAtDt.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("Desativados");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroupAtDt.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("Ativos");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(radioDesativados)
                        .addGap(18, 18, 18)
                        .addComponent(radioAtivados))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addGap(10, 10, 10)
                        .addComponent(btnRestaurar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPesquisa)
                        .addGap(10, 10, 10))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisar)
                    .addComponent(btnRestaurar)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioDesativados)
                    .addComponent(radioAtivados))
                .addGap(10, 10, 10))
        );

        panelGrupo.setBackground(new java.awt.Color(204, 204, 204));
        panelGrupo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jcomboGrupo.setBackground(new java.awt.Color(204, 204, 204));
        jcomboGrupo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione um Grupo", "Mecânico", "Administrador" }));
        jcomboGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcomboGrupoActionPerformed(evt);
            }
        });
        jcomboGrupo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jcomboGrupoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelGrupoLayout = new javax.swing.GroupLayout(panelGrupo);
        panelGrupo.setLayout(panelGrupoLayout);
        panelGrupoLayout.setHorizontalGroup(
            panelGrupoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGrupoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jcomboGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(437, 437, 437))
        );
        panelGrupoLayout.setVerticalGroup(
            panelGrupoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGrupoLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jcomboGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelprincipal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelGrupo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(panelGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setMnemonic('A');
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnExcluir.setBackground(new java.awt.Color(204, 204, 204));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnExcluir.setMnemonic('E');
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        btnExcluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExcluirKeyPressed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnLogin.setBackground(new java.awt.Color(204, 204, 204));
        btnLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/login.png"))); // NOI18N
        btnLogin.setMnemonic('L');
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar (1).png"))); // NOI18N
        btnAtivar.setMnemonic('T');
        btnAtivar.setText("Ativar");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setMnemonic('D');
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnIncluir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtivar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesativar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(93, 93, 93)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {

            codUsuario = Integer.parseInt(itemTabela());
            AtivaComboBox();
            controlBotao = 2;
            ct.campoEditavel(panelprincipal);
            tabelaUsuario.setEnabled(false);
            tabelaUsuario.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);

            btnRestaurar.setEnabled(false);

            btnPesquisar.setEnabled(false);
            btnPesquisar.setEnabled(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            txtNome.requestFocus();

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            codUsuario = Integer.parseInt(itemTabela());
            tabelaUsuario.setEnabled(false);
            tabelaUsuario.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);

            btnRestaurar.setEnabled(false);

            btnPesquisar.setEnabled(false);
            if (logDAO.ConsultaUsuarioTipo(codUsuario)) {

                if (logDAO.VerificaUsuarioDesativa()) {

                    JOptionPane.showMessageDialog(null, "DEVE HAVER AO MENOS UM USUÁRIO ADMINISTRADOR ATIVO");
                    tabelaUsuario.setEnabled(true);
                    tabelaUsuario.setVisible(true);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    btnRestaurar.setEnabled(true);
                    btnPesquisar.setEnabled(true);
                } else {

                    if (!logDAO.consultaStatusUsuario(codUsuario)) {
                        excluirUsuario();
                    } else {
                        JOptionPane.showMessageDialog(null, "DESATIVE O USUÁRIO ANTES DE EXCLUIR!");
                        tabelaUsuario.setEnabled(true);
                        tabelaUsuario.setVisible(true);
                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        btnRestaurar.setEnabled(true);
                        btnPesquisar.setEnabled(true);
                    }

                }

            } else {
                if (!logDAO.consultaStatusUsuario(codUsuario)) {
                    excluirUsuario();
                } else {
                    JOptionPane.showMessageDialog(null, "DESATIVE O USUÁRIO ANTES DE EXCLUIR!");
                    tabelaUsuario.setEnabled(true);
                    tabelaUsuario.setVisible(true);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    btnRestaurar.setEnabled(true);
                    btnPesquisar.setEnabled(true);
                }
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed

        if (logDAO.VerificaUsuario()) {

            jcomboGrupo.setSelectedIndex(2);
            jcomboGrupo.setEnabled(false);
            btnPesquisar.setEnabled(false);
            btnDesativar.setEnabled(false);
            controlBotao = 1;
            ct.clear(panelprincipal);
            ct.campoEditavel(panelprincipal);
            tabelaUsuario.setVisible(false);
            tabelaUsuario.setEnabled(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setEnabled(true);
            btnCancelar.setVisible(true);
            txtNome.requestFocus();
            btnRestaurar.setEnabled(false);
            btnRestaurar.setEnabled(false);
            btnPesquisar.setEnabled(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            btnDesativar.setEnabled(false);
            radioAtivados.setVisible(false);
            radioDesativados.setVisible(false);
        } else {
            controlBotao = 1;
            ct.clear(panelprincipal);
            ct.campoEditavel(panelprincipal);
            tabelaUsuario.setVisible(false);
            tabelaUsuario.setEnabled(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            AtivaComboBox();
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setEnabled(true);
            btnCancelar.setVisible(true);
            txtNome.requestFocus();
            btnRestaurar.setEnabled(false);
            btnRestaurar.setEnabled(false);
            btnPesquisar.setEnabled(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setEnabled(false);
        }


    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);

        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        desativaComboBox();
        ct.clear(panelprincipal);
        ct.campoNaoEditavel(panelprincipal);
        btnIncluir.requestFocus();
        tabelaUsuario.setVisible(true);
        tabelaUsuario.setEnabled(true);
        preencheTabela();
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:

                if (txtNome.getText().trim().isEmpty() || masc.removeMascaraData(ftxCpf.getText()).trim().isEmpty()
                        || txtSenha.getText().trim().isEmpty()
                        || jcomboGrupo.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    txtNome.requestFocus();

                } else {
                    cadastroUsuario();

                }

                break;

            case 2:
                alteraUsuario();
                break;

        }
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            switch (controlBotao) {
                case 1:

                    if (txtNome.getText().trim().isEmpty() || masc.removeMascaraData(ftxCpf.getText()).trim().isEmpty()
                            || txtSenha.getText().trim().isEmpty()
                            || jcomboGrupo.getSelectedIndex() == 0) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                        txtNome.requestFocus();

                    } else {
                        cadastroUsuario();

                    }

                    break;

                case 2:
                    alteraUsuario();
                    break;

            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        ct.clear(panelprincipal);
        txtPesquisa.setText("");
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.requestFocus();
        radioDesativados.setVisible(true);
        radioAtivados.setVisible(true);
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnRestaurarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnRestaurarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelprincipal);
            txtPesquisa.setText("");
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);
            preencheTabela();
        }
    }//GEN-LAST:event_btnRestaurarKeyPressed

    private void btnPesquisarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPesquisarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelprincipal);
            txtPesquisa.setText("");
            btnRestaurar.setVisible(true);
            btnRestaurar.setEnabled(true);
            txtPesquisa.setEnabled(true);
            txtPesquisa.setVisible(true);
            txtPesquisa.requestFocus();
        }
    }//GEN-LAST:event_btnPesquisarKeyPressed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed
        ct.clear(panelprincipal);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        preencheTabela();
        btnAtivar.setVisible(false);
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (radioAtivados.isSelected()) {
                preencheTabelaPesquisa();
            } else if (radioDesativados.isSelected()) {

                preencheTabelaPesquisaDesativados();
            }
        }

    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void txtNomeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ftxCpf.requestFocus();
        }
    }//GEN-LAST:event_txtNomeKeyPressed

    private void ftxCpfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxCpfKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtSenha.requestFocus();
        }
    }//GEN-LAST:event_ftxCpfKeyPressed

    private void txtConfSenhaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConfSenhaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (logDAO.VerificaUsuario()) {
                btnGravar.requestFocus();
            } else {
                jcomboGrupo.requestFocus();
            }

        }
    }//GEN-LAST:event_txtConfSenhaKeyPressed

    private void txtSenhaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtConfSenha.requestFocus();
        }
    }//GEN-LAST:event_txtSenhaKeyPressed

    private void tabelaUsuarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaUsuarioKeyReleased
        listaTextFiel();
    }//GEN-LAST:event_tabelaUsuarioKeyReleased

    private void tabelaUsuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaUsuarioMouseClicked
        listaTextFiel();
    }//GEN-LAST:event_tabelaUsuarioMouseClicked

    private void tabelaUsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaUsuarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();

            int row = tabelaUsuario.getSelectedRow();
            String valor = tabelaUsuario.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tabelaUsuarioKeyPressed

    private void btnExcluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExcluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codUsuario = Integer.parseInt(itemTabela());
                tabelaUsuario.setEnabled(false);
                tabelaUsuario.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);

                btnRestaurar.setEnabled(false);

                btnPesquisar.setEnabled(false);
                excluirUsuario();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnExcluirKeyPressed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codUsuario = Integer.parseInt(itemTabela());
                AtivaComboBox();
                controlBotao = 2;
                ct.campoEditavel(panelprincipal);
                tabelaUsuario.setEnabled(false);
                tabelaUsuario.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);

                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                btnGravar.setEnabled(true);
                btnGravar.setVisible(true);
                btnCancelar.setVisible(true);
                btnCancelar.setEnabled(true);
                txtNome.requestFocus();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);

            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setVisible(false);
            btnCancelar.setEnabled(false);

            ct.clear(panelprincipal);
            ct.campoNaoEditavel(panelprincipal);
            btnIncluir.requestFocus();
            tabelaUsuario.setVisible(true);
            tabelaUsuario.setEnabled(true);
            preencheTabela();
        }
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        if (log.VerificaUsuario() == true) {
            System.exit(0);
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        if (log.VerificaUsuario() == false) {

            this.hide();
        }
    }//GEN-LAST:event_btnLoginActionPerformed

    private void jcomboGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcomboGrupoActionPerformed
        if (jcomboGrupo.getSelectedIndex() != 0) {

            btnGravar.requestFocus();

        }
    }//GEN-LAST:event_jcomboGrupoActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        userAtDt = "ativado";
        try {
            int row = tabelaUsuario.getSelectedRow();
            String valor = tabelaUsuario.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇÃO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                logDAO.AtivaDesativa(userAtDt, cod);
                JOptionPane.showMessageDialog(null, "USUÁRIO FOI ATIVADO COM SUCESSO");
            }

            if (!valor.equals("")) {

                preencheTabelaDesativados();
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed

        userAtDt = "desativado";
        try {
            int row = tabelaUsuario.getSelectedRow();
            String valor = tabelaUsuario.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            if (logDAO.pendenciaUsuário(cod)) {
                if (logDAO.ConsultaUsuarioTipo(cod)) {

                    if (logDAO.VerificaUsuarioDesativa()) {

                        JOptionPane.showMessageDialog(null, "DEVE HAVER AO MENOS UM USUÁRIO ADMINISTRADOR ATIVO");
                    } else {

                        int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {
                            logDAO.AtivaDesativa(userAtDt, cod);
                            JOptionPane.showMessageDialog(null, "USUÁRIO FOI DESATIVADO COM SUCESSO");
                        }

                        if (!valor.equals("")) {

                            preencheTabela();
                        }

                    }

                } else {
                    int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);
                    if (resp == 0) {
                        logDAO.AtivaDesativa(userAtDt, cod);
                        JOptionPane.showMessageDialog(null, "USUÁRIO FOI DESATIVADO COM SUCESSO");
                    }

                    if (!valor.equals("")) {

                        preencheTabela();
                    }

                }
            } else {
                JOptionPane.showMessageDialog(null, "USUÁRIO NÃO PODE SER DESATIVADO! ELE ESTÁ SENDO UTILIZADO");
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UM USUÁRIO PRIMEIRO!");
        }


    }//GEN-LAST:event_btnDesativarActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed
        if (logDAO.VerificaUsuario()) {

            btnDesativar.setEnabled(false);
        } else {
            preencheTabelaDesativados();
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
        }


    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed
        preencheTabela();
        btnAtivar.setVisible(false);
        btnAtivar.setEnabled(false);
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_radioAtivadosActionPerformed

    private void jcomboGrupoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jcomboGrupoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (jcomboGrupo.getSelectedIndex() != 0) {

                btnGravar.requestFocus();

            }

        }
    }//GEN-LAST:event_jcomboGrupoKeyPressed

    private void cadastroUsuario() {
        boolean flag;

        Usuario user = new Usuario();
        if (txtSenha.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "INFORME A SENHA DO USUÁRIO");
            txtSenha.requestFocus();
            return;
        }
        if (txtSenha.getText().equals(txtConfSenha.getText())) {

            user.setNome(txtNome.getText());
            user.setCpfCnpj(masc.removeMascaraCpf(ftxCpf.getText()));
            user.setSenha(txtSenha.getText());
            user.setGrupo(jcomboGrupo.getSelectedItem().toString());
         

            if (jcomboGrupo.getSelectedIndex() == 0) {
                JOptionPane.showMessageDialog(null, "INFORME UM GRUPO DE USUÁRIO");
                jcomboGrupo.requestFocus();
                return;

            }
            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);

            if (resp == 0) {
                try {
                    flag = logDAO.adicionarUsuario(user);

                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "USUÁRIO CADASTRADO COM SUCESSO");

                        ct.clear(panelprincipal);

                        ct.campoNaoEditavel(panelprincipal);
                        txtSenha.setEditable(false);
                        txtConfSenha.setEditable(false);
                        preencheTabela();
                        tabelaUsuario.setVisible(true);
                        tabelaUsuario.setEnabled(true);
                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        desativaComboBox();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setVisible(false);
                        btnCancelar.setEnabled(false);

                        btnPesquisar.setEnabled(true);
                        btnDesativar.setEnabled(true);
                    } else if (flag == false) {

                        btnIncluir.setEnabled(false);
                        btnAlterar.setEnabled(false);
                        btnExcluir.setEnabled(false);

                        btnGravar.setEnabled(true);
                        btnGravar.setVisible(true);
                    }
                } catch (HeadlessException ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

                }

            } else if (resp == 1) {

                ct.clear(panelprincipal);
                ct.campoNaoEditavel(panelprincipal);
                desativaComboBox();
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
            }

        } else {
            JOptionPane.showMessageDialog(null, "SENHA DIGITADA NÃO CONFERE");
            txtSenha.requestFocus();
        }

    }

    private void alteraUsuario() {

        boolean flag;

        if (txtNome.getText().trim().isEmpty() || masc.removeMascaraData(ftxCpf.getText()).trim().isEmpty()
                || txtSenha.getText().trim().isEmpty()
                || jcomboGrupo.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
            txtNome.requestFocus();
        } else {

            if (txtSenha.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null, "INFORME A NOVA SENHA DO USUÁRIO");
                txtSenha.requestFocus();
                return;
            }
            if (txtSenha.getText().equals(txtConfSenha.getText())) {

                Usuario user = new Usuario();
                user.setNome(txtNome.getText());
                user.setCpfCnpj(masc.removeMascaraCpf(ftxCpf.getText()));
                user.setSenha(txtSenha.getText());
                user.setGrupo(jcomboGrupo.getSelectedItem().toString());
             

                if (jcomboGrupo.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "INFORME UM GRUPO DE USUÁRIO");
                    jcomboGrupo.requestFocus();
                    return;

                }

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {
                    try {
                        flag = false;

                        flag = logDAO.alterarUsuario(user, codUsuario);

                        if (flag == true) {
                            JOptionPane.showMessageDialog(null, "USUÁRIO ALTERADO COM SUCESSO");
                            ct.clear(panelprincipal);
                            ct.campoNaoEditavel(panelprincipal);
                            txtPesquisa.setVisible(true);
                            btnGravar.setEnabled(false);
                            btnGravar.setVisible(false);
                            btnCancelar.setEnabled(false);
                            btnCancelar.setVisible(false);
                        }
                    } catch (HeadlessException ex) {
                        Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else if (resp == 1) {
                    ct.clear(panelprincipal);
                    ct.campoNaoEditavel(panelprincipal);
                    tabelaUsuario.setEnabled(true);
                    tabelaUsuario.setVisible(true);
                    preencheTabela();
                    txtPesquisa.setVisible(true);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);

                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);
                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setEnabled(false);
                    btnCancelar.setVisible(false);
                }

                preencheTabela();
                tabelaUsuario.setEnabled(true);
                tabelaUsuario.setVisible(true);
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);

                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);

            } else {
                JOptionPane.showMessageDialog(null, "SENHA DIGITADA NÃO CONFERE");
                txtSenha.requestFocus();
            }

        }

    }

    public void excluirUsuario() {
        boolean flag;

        if (logDAO.buscaUsuario(codUsuario)) {

            if (logDAO.pendenciaUsuário(codUsuario)) {

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DO USUÁRIO", "Confirmação", JOptionPane.YES_NO_OPTION);

                if (resp == 0) {

                    flag = logDAO.excluirUsuario(codUsuario);
                    if (flag == true) {
                        JOptionPane.showMessageDialog(null, "USUÁRIO EXCLUÍDO COM SUCESSO");

                        preencheTabela();
                        tabelaUsuario.setEnabled(true);
                        tabelaUsuario.setVisible(true);

                        ct.clear(panelprincipal);

                        ct.campoNaoEditavel(panelprincipal);

                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        btnRestaurar.setEnabled(true);
                        btnPesquisar.setVisible(true);
                        btnPesquisar.setEnabled(true);
                        btnRestaurar.setVisible(false);
                        txtPesquisa.setVisible(false);
                        radioAtivados.setVisible(false);
                        radioDesativados.setVisible(false);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);

                    }
                } else if (resp == 1) {

                    ct.clear(panelprincipal);

                    ct.campoNaoEditavel(panelprincipal);
                    tabelaUsuario.setEnabled(true);
                    tabelaUsuario.setVisible(true);
                    preencheTabela();
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    btnRestaurar.setEnabled(true);
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);
                    btnRestaurar.setVisible(false);
                    txtPesquisa.setVisible(false);
                    radioAtivados.setVisible(false);
                    radioDesativados.setVisible(false);
                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setEnabled(false);
                    btnCancelar.setVisible(false);

                }

            } else {

                JOptionPane.showMessageDialog(null, "USUÁRIO ESTÁ SENDO UTILIZADO E NÃO PODE SER EXCLUIDO");
                ct.clear(panelprincipal);

                ct.campoNaoEditavel(panelprincipal);
                tabelaUsuario.setEnabled(true);
                tabelaUsuario.setVisible(true);
                preencheTabela();
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
                btnRestaurar.setEnabled(true);
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
                btnRestaurar.setVisible(false);
                txtPesquisa.setVisible(false);
                radioAtivados.setVisible(false);
                radioDesativados.setVisible(false);
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);
            }
        } else {

            ct.clear(panelprincipal);

            JOptionPane.showMessageDialog(null, "NÃO EXISTE USUÁRIO COM ESTE CÓDIGO");
            ct.campoNaoEditavel(panelprincipal);
            tabelaUsuario.setEnabled(true);
            tabelaUsuario.setVisible(true);
            preencheTabela();
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);

            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
        }

    }

    public void listaTextFiel() {

        int row = tabelaUsuario.getSelectedRow();
        String strCod = tabelaUsuario.getValueAt(row, 0).toString();
        int codUser = Integer.parseInt(strCod);
        Usuario user;

        if (logDAO.buscaUsuario(codUser)) {
            user = logDAO.consultaUsuario(codUser);

            ct.clear(panelprincipal);

            txtNome.setText(user.getNome());
            ftxCpf.setText(user.getCpfCnpj());

        }
    }

    public String itemTabela() {

        int row = tabelaUsuario.getSelectedRow();
        String valor = tabelaUsuario.getValueAt(row, 0).toString();

        return valor;
    }

    public void preencheTabelaPesquisa() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();
        modeloTab.setNumRows(0);

        for (Usuario user : logDAO.preencheTabelaPesquisaAtivados(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                user.getCodigousuario(),
                user.getNome(),
                user.getCpfCnpj(),
                user.getGrupo()

            });

        }

    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();
        modeloTab.setNumRows(0);

        for (Usuario user : logDAO.preencheTabelaPesquisaDesativados(txtPesquisa.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                user.getCodigousuario(),
                user.getNome(),
                user.getCpfCnpj(),
                user.getGrupo()

            });

        }

    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();
        modeloTab.setNumRows(0);

        for (Usuario user : logDAO.preencheTabela()) {

            modeloTab.addRow(new Object[]{
                user.getCodigousuario(),
                user.getNome(),
                user.getCpfCnpj(),
                user.getGrupo()

            });

        }

    }

    public void preencheTabelaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaUsuario.getModel();
        modeloTab.setNumRows(0);

        for (Usuario user : logDAO.preencheTabelaDesativados()) {

            modeloTab.addRow(new Object[]{
                user.getCodigousuario(),
                user.getNome(),
                user.getCpfCnpj(),
                user.getGrupo()

            });

        }

    }

    public void desativaComboBox() {

        jcomboGrupo.setEnabled(false);

        jcomboGrupo.setSelectedIndex(0);

    }

    public void AtivaComboBox() {
        jcomboGrupo.setEnabled(true);

        jcomboGrupo.setSelectedIndex(0);

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.ButtonGroup buttonGroupAtDt;
    private javax.swing.JFormattedTextField ftxCpf;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcomboGrupo;
    private javax.swing.JPanel panelGrupo;
    private javax.swing.JPanel panelprincipal;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tabelaUsuario;
    private javax.swing.JPasswordField txtConfSenha;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JPasswordField txtSenha;
    // End of variables declaration//GEN-END:variables
}
