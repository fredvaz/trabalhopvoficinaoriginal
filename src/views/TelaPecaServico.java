package views;

import javax.swing.table.DefaultTableModel;
import models.PecaServico;
import controllers.ServicoPecaDAO;
import help.CampoTxt;
import help.LimitaNroCaracteres;
import help.Mascara;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.TableRowSorter;

public final class TelaPecaServico extends javax.swing.JDialog {

    private final CampoTxt ct;
    private final Mascara masc = new Mascara();
    private int controlBotao;
    private final ServicoPecaDAO psDAO;
    int codPecaServico;
    String opItem;
    private String pecaservicoDtAt;
    TelaPrincipal tpr = new TelaPrincipal();

    public TelaPecaServico(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ct = new CampoTxt();
        psDAO = new ServicoPecaDAO();
        desativaComboBox();
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);
        controlBotao = 0;
        txtNome.setDocument(new LimitaNroCaracteres(38));

        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
        btnAtivar.setVisible(false);

        codPecaServico = 0;
        DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();
        tablePecaServico.setRowSorter(new TableRowSorter(modeloTab));
        preencheTabela();

        if (!psDAO.VerificaPecaServico()) {
            tablePecaServico.requestFocus();
            tablePecaServico.changeSelection(0, 0, false, false);
        } else if (!psDAO.VerificaPecaServico()) {

            btnPesquisar.setEnabled(false);
        }

        if (tpr.grupo.equals("Mecânico")) {
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            btnPesquisar.setEnabled(false);
           

        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelPecaServico = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        combPecaServico = new javax.swing.JComboBox<>();
        txtNome = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablePecaServico = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnPesquisar = new javax.swing.JButton();
        btnRestaurar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        radioDesativados = new javax.swing.JRadioButton();
        radioAtivados = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        btnIncluir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Peças/Serviços");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelPecaServico.setBackground(new java.awt.Color(204, 204, 204));
        panelPecaServico.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Peça/Serviço");

        jLabel3.setText("Descrição*");

        combPecaServico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Peça", "Serviço" }));
        combPecaServico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                combPecaServicoKeyPressed(evt);
            }
        });

        txtNome.setEditable(false);
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeKeyPressed(evt);
            }
        });

        jLabel4.setText("Valor");

        txtValor.setEditable(false);
        txtValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtValorKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelPecaServicoLayout = new javax.swing.GroupLayout(panelPecaServico);
        panelPecaServico.setLayout(panelPecaServicoLayout);
        panelPecaServicoLayout.setHorizontalGroup(
            panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPecaServicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(combPecaServico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(txtNome))
                .addGap(10, 10, 10)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap())
        );
        panelPecaServicoLayout.setVerticalGroup(
            panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPecaServicoLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPecaServicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combPecaServico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablePecaServico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Peca/Servico", "Descrição", "Valor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablePecaServico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablePecaServicoMouseClicked(evt);
            }
        });
        tablePecaServico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablePecaServicoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablePecaServicoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablePecaServico);
        if (tablePecaServico.getColumnModel().getColumnCount() > 0) {
            tablePecaServico.getColumnModel().getColumn(0).setResizable(false);
            tablePecaServico.getColumnModel().getColumn(0).setPreferredWidth(5);
            tablePecaServico.getColumnModel().getColumn(1).setResizable(false);
            tablePecaServico.getColumnModel().getColumn(1).setPreferredWidth(15);
            tablePecaServico.getColumnModel().getColumn(2).setResizable(false);
            tablePecaServico.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablePecaServico.getColumnModel().getColumn(3).setResizable(false);
            tablePecaServico.getColumnModel().getColumn(3).setPreferredWidth(10);
        }

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        btnPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPesquisarKeyPressed(evt);
            }
        });

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('T');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });
        btnRestaurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnRestaurarKeyPressed(evt);
            }
        });

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("Desativados");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("Ativos");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(radioDesativados)
                        .addGap(18, 18, 18)
                        .addComponent(radioAtivados))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRestaurar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa)))
                .addGap(10, 10, 10))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisar)
                    .addComponent(btnRestaurar)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioDesativados)
                    .addComponent(radioAtivados))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelPecaServico, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(panelPecaServico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnIncluir.setBackground(new java.awt.Color(204, 204, 204));
        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Check_32x32.png"))); // NOI18N
        btnIncluir.setMnemonic('I');
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        btnIncluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnIncluirKeyPressed(evt);
            }
        });

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setMnemonic('A');
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnExcluir.setBackground(new java.awt.Color(204, 204, 204));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Delete_32x32.png"))); // NOI18N
        btnExcluir.setMnemonic('E');
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        btnExcluir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExcluirKeyPressed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Cancel_12x12.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        btnGravar.setBackground(new java.awt.Color(204, 204, 204));
        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravar.setMnemonic('G');
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        btnGravar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGravarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setMnemonic('D');
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar (1).png"))); // NOI18N
        btnAtivar.setMnemonic('V');
        btnAtivar.setText("Ativar");
        btnAtivar.setToolTipText("");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesativar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnAtivar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(99, 99, 99)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        ct.clear(panelPecaServico);
        txtPesquisa.setText("");
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.requestFocus();
        desativaComboBox();
        radioDesativados.setVisible(true);
        radioAtivados.setVisible(true);
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed
        ct.clear(panelPecaServico);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        preencheTabela();
        btnAtivar.setVisible(false);
        radioDesativados.setVisible(false);
        radioAtivados.setVisible(false);
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void btnRestaurarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnRestaurarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelPecaServico);
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);
            preencheTabela();
            radioAtivados.setVisible(true);
            radioDesativados.setVisible(true);

        }
    }//GEN-LAST:event_btnRestaurarKeyPressed

    private void btnPesquisarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPesquisarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ct.clear(panelPecaServico);
            txtPesquisa.setText("");
            btnRestaurar.setVisible(true);
            btnRestaurar.setEnabled(true);
            txtPesquisa.setEnabled(true);
            txtPesquisa.setVisible(true);
            txtPesquisa.requestFocus();
            desativaComboBox();
            radioAtivados.setVisible(true);
            radioDesativados.setVisible(true);
        }
    }//GEN-LAST:event_btnPesquisarKeyPressed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        controlBotao = 1;
        AtivaComboBox();
        ct.clear(panelPecaServico);
        ct.campoEditavel(panelPecaServico);
        combPecaServico.setEnabled(true);
        tablePecaServico.setVisible(false);
        tablePecaServico.setEnabled(false);
        btnIncluir.setEnabled(false);
        btnAlterar.setEnabled(false);
        btnExcluir.setEnabled(false);
        
        btnGravar.setEnabled(true);
        btnGravar.setVisible(true);
        btnCancelar.setEnabled(true);
        btnCancelar.setVisible(true);
        combPecaServico.requestFocus();
        btnRestaurar.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnPesquisar.setVisible(false);
        btnPesquisar.setEnabled(false);
        txtPesquisa.setEnabled(false);
        txtPesquisa.setVisible(false);
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);
        btnDesativar.setEnabled(false);
    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnIncluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnIncluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            controlBotao = 1;
            AtivaComboBox();
            ct.clear(panelPecaServico);
            ct.campoEditavel(panelPecaServico);
            combPecaServico.setEditable(true);
            tablePecaServico.setVisible(false);
            tablePecaServico.setEnabled(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
           
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setEnabled(true);
            btnCancelar.setVisible(true);
            combPecaServico.requestFocus();
            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setEnabled(false);
            txtPesquisa.setVisible(false);

        }
    }//GEN-LAST:event_btnIncluirKeyPressed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {
            codPecaServico = Integer.parseInt(itemTabela());
            AtivaComboBox();
            controlBotao = 2;
            ct.campoEditavel(panelPecaServico);
            tablePecaServico.setEnabled(false);
            tablePecaServico.setVisible(false);
            btnIncluir.setEnabled(false);
            btnAlterar.setEnabled(false);
            btnExcluir.setEnabled(false);
            
            btnRestaurar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnPesquisar.setVisible(false);
            btnPesquisar.setEnabled(false);
            txtPesquisa.setVisible(false);
            btnGravar.setEnabled(true);
            btnGravar.setVisible(true);
            btnCancelar.setVisible(true);
            btnCancelar.setEnabled(true);
            combPecaServico.requestFocus();

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codPecaServico = Integer.parseInt(itemTabela());
                AtivaComboBox();
                controlBotao = 2;
                ct.campoEditavel(panelPecaServico);
                tablePecaServico.setEnabled(false);
                tablePecaServico.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
                
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                txtPesquisa.setVisible(false);
                btnGravar.setEnabled(true);
                btnGravar.setVisible(true);
                btnCancelar.setVisible(true);
                btnCancelar.setEnabled(true);
                combPecaServico.requestFocus();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            codPecaServico = Integer.parseInt(itemTabela());
            int resp = 9;
            PecaServico p;
            p = psDAO.consultaPecaServico(codPecaServico);
            if (!psDAO.consultaStatusPecaServico(codPecaServico)) {
                tablePecaServico.setEnabled(false);
                tablePecaServico.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
                
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                excluiPecaServico();
            } else {

                if (p.getTipo() == 1) {
                    resp = JOptionPane.showConfirmDialog(null, "DESATIVE A PEÇA ANTES DE EXCLUIR", "Confirmação", JOptionPane.YES_NO_OPTION);
                } else if (p.getTipo() == 2) {
                    resp = JOptionPane.showConfirmDialog(null, "DESATIVE O SERVIÇO ANTES DE EXCLUIR", "Confirmação", JOptionPane.YES_NO_OPTION);
                }
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnExcluirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExcluirKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                codPecaServico = Integer.parseInt(itemTabela());
                tablePecaServico.setEnabled(false);
                tablePecaServico.setVisible(false);
                btnIncluir.setEnabled(false);
                btnAlterar.setEnabled(false);
                btnExcluir.setEnabled(false);
                
                btnRestaurar.setEnabled(false);
                btnRestaurar.setVisible(false);
                btnPesquisar.setVisible(false);
                btnPesquisar.setEnabled(false);
                excluiPecaServico();

            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
            }
        }
    }//GEN-LAST:event_btnExcluirKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        btnIncluir.setEnabled(true);
        btnAlterar.setEnabled(true);
        btnExcluir.setEnabled(true);
        
        btnPesquisar.setVisible(true);
        btnPesquisar.setEnabled(true);
        btnDesativar.setEnabled(true);
        btnGravar.setEnabled(false);
        btnGravar.setVisible(false);
        btnCancelar.setVisible(false);
        btnCancelar.setEnabled(false);
        desativaComboBox();
        ct.clear(panelPecaServico);
        ct.campoNaoEditavel(panelPecaServico);
        btnIncluir.requestFocus();
        tablePecaServico.setVisible(true);
        tablePecaServico.setEnabled(true);
        preencheTabela();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
           
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setVisible(false);
            btnCancelar.setEnabled(false);
            desativaComboBox();
            ct.clear(panelPecaServico);
            ct.campoNaoEditavel(panelPecaServico);
            btnIncluir.requestFocus();
            tablePecaServico.setVisible(true);
            tablePecaServico.setEnabled(true);
            preencheTabela();
        }
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        switch (controlBotao) {
            case 1:

                if (txtNome.getText().trim().isEmpty() || txtValor.getText().trim().isEmpty()
                        || combPecaServico.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                    txtNome.requestFocus();

                } else {
                    cadastroPecaServico();

                }

                break;

            case 2:
                alteraPecaServico();
                break;

        }
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnGravarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGravarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            switch (controlBotao) {
                case 1:

                    if (txtNome.getText().trim().isEmpty() || txtValor.getText().trim().isEmpty()
                            || combPecaServico.getSelectedIndex() == 0) {
                        JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
                        txtNome.requestFocus();

                    } else {
                        cadastroPecaServico();

                    }

                    break;

                case 2:
                    alteraPecaServico();
                    break;

            }
        }
    }//GEN-LAST:event_btnGravarKeyPressed

    private void combPecaServicoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_combPecaServicoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtNome.requestFocus();
        }
    }//GEN-LAST:event_combPecaServicoKeyPressed

    private void txtNomeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtValor.requestFocus();
        }
    }//GEN-LAST:event_txtNomeKeyPressed

    private void txtValorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnGravar.requestFocus();
        }
    }//GEN-LAST:event_txtValorKeyPressed

    private void tablePecaServicoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablePecaServicoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();

            int row = tablePecaServico.getSelectedRow();
            String valor = tablePecaServico.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tablePecaServicoKeyPressed

    private void tablePecaServicoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablePecaServicoKeyReleased
        listaTextFiel();
    }//GEN-LAST:event_tablePecaServicoKeyReleased

    private void tablePecaServicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablePecaServicoMouseClicked
        listaTextFiel();
    }//GEN-LAST:event_tablePecaServicoMouseClicked

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            preencheTabelaPesquisa();
        }
    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed
        if (psDAO.VerificaPecaServico()) {

            btnDesativar.setEnabled(false);
        } else {
            preencheTabelaDesativados();
            btnDesativar.setEnabled(false);
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
        }
    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed
        preencheTabela();
        btnAtivar.setVisible(false);
        btnAtivar.setEnabled(false);
        btnDesativar.setEnabled(true);
    }//GEN-LAST:event_radioAtivadosActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        int resp = 9;
        pecaservicoDtAt = "desativado";
        try {
            int row = tablePecaServico.getSelectedRow();
            String valor = tablePecaServico.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            PecaServico p;
            p = psDAO.consultaPecaServico(cod);
            if (psDAO.pendenciaPecaServico(cod)) {

                if (p.getTipo() == 1) {
                    resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DA PEÇA", "Confirmação", JOptionPane.YES_NO_OPTION);
                } else if (p.getTipo() == 2) {
                    resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DO SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
                }

                if (resp == 0) {

                    psDAO.AtivaDesativa(pecaservicoDtAt, cod);

                    if (p.getTipo() == 1) {
                        JOptionPane.showMessageDialog(null, "PEÇA FOI DESATIVADA COM SUCESSO");
                    } else if (p.getTipo() == 2) {
                        JOptionPane.showMessageDialog(null, "SERVIÇO FOI DESATIVADO COM SUCESSO");
                    }

                }

                if (!valor.equals("")) {

                    preencheTabela();
                }
            } else {
                if (p.getTipo() == 1) {
                    JOptionPane.showMessageDialog(null, "PEÇA NÃO PODE SER DESATIVADA! ELA ESTÁ SENDO UTILIZADA");
                } else if (p.getTipo() == 2) {
                    JOptionPane.showMessageDialog(null, "SERVIÇO NÃO PODE SER DESATIVADO! ELA ESTÁ SENDO UTILIZADO");
                }

            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        pecaservicoDtAt = "ativado";
        int resp = 9;
        try {
            int row = tablePecaServico.getSelectedRow();
            String valor = tablePecaServico.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);
            PecaServico p;
            p = psDAO.consultaPecaServico(cod);
            if (p.getTipo() == 1) {
                resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇAO DA PEÇA", "Confirmação", JOptionPane.YES_NO_OPTION);
            } else if (p.getTipo() == 2) {
                resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇAO DO SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
            }
            if (resp == 0) {
                psDAO.AtivaDesativa(pecaservicoDtAt, cod);
                if (p.getTipo() == 1) {
                    JOptionPane.showMessageDialog(null, "PEÇA FOI ATIVADA COM SUCESSO");
                } else if (p.getTipo() == 2) {
                    JOptionPane.showMessageDialog(null, "SERVIÇO FOI ATIVADO COM SUCESSO");
                }
            }

            if (!valor.equals("")) {

                preencheTabelaDesativados();
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA PEÇA OU SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed
    private void cadastroPecaServico() {
        boolean flag;
        int resp = 4;
        PecaServico ps = new PecaServico();

        ps.setDescricao(txtNome.getText());
        ps.setTipo(combPecaServico.getSelectedIndex());
        ps.setValor(Double.parseDouble(masc.removeMascaraDouble(txtValor.getText())));

        int tipo = combPecaServico.getSelectedIndex();

        if (tipo == 1) {
            resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DA PEÇA", "Confirmação", JOptionPane.YES_NO_OPTION);
        } else if (tipo == 2) {
            resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A INCLUSÃO DO SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
        }
        if (resp == 0) {
            try {
                flag = psDAO.addPecaServico(ps);
                if (flag == true) {
                    if (tipo == 1) {
                        JOptionPane.showMessageDialog(null, "PEÇA CADASTRADA COM SUCESSO");
                    } else if (tipo == 2) {
                        JOptionPane.showMessageDialog(null, "SERVIÇO CADASTRADO COM SUCESSO");
                    }

                    ct.clear(panelPecaServico);
                    ct.campoNaoEditavel(panelPecaServico);
                    preencheTabela();
                    desativaComboBox();
                    tablePecaServico.setVisible(true);
                    tablePecaServico.setEnabled(true);
                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                    
                    btnGravar.setEnabled(false);
                    btnGravar.setVisible(false);
                    btnCancelar.setVisible(false);
                    btnCancelar.setEnabled(false);
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);
                    btnDesativar.setEnabled(true);
                } else if (flag == false) {

                    btnIncluir.setEnabled(false);
                    btnAlterar.setEnabled(false);
                    btnExcluir.setEnabled(false);
                    
                    btnGravar.setEnabled(true);
                    btnGravar.setVisible(true);
                    btnDesativar.setEnabled(true);
                }
            } catch (Exception ex) {
                Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);

            }

        } else if (resp == 1) {

            ct.clear(panelPecaServico);
            ct.campoNaoEditavel(panelPecaServico);
            desativaComboBox();
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
        }

    }

    private void alteraPecaServico() {

        boolean flag;
        int resp = 4;

        if (txtNome.getText().trim().isEmpty() || txtValor.getText().trim().isEmpty()
                || combPecaServico.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "OS CAMPOS COM * SÃO OBRIGATÓRIOS");
            combPecaServico.requestFocus();
        } else {
            int tipo = combPecaServico.getSelectedIndex();
            PecaServico ps = new PecaServico();

            ps.setDescricao(txtNome.getText());
            ps.setTipo(combPecaServico.getSelectedIndex());
            ps.setValor(Double.parseDouble(txtValor.getText()));

            if (tipo == 1) {
                resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DA PEÇA", "Confirmação", JOptionPane.YES_NO_OPTION);
            } else if (tipo == 2) {
                resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DO SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
            }

            if (resp == 0) {
                try {
                    flag = psDAO.alterarPecaServico(ps, codPecaServico);
                    if (flag == true) {
                        if (tipo == 1) {
                            JOptionPane.showMessageDialog(null, "PEÇA ALTERADA COM SUCESSO");
                        } else if (tipo == 2) {
                            JOptionPane.showMessageDialog(null, "SERVIÇO ALTERADO COM SUCESSO");
                        }
                        txtPesquisa.setVisible(true);
                        ct.clear(panelPecaServico);
                        ct.campoNaoEditavel(panelPecaServico);
                        desativaComboBox();
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(TelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (resp == 1) {
                ct.clear(panelPecaServico);
                ct.campoNaoEditavel(panelPecaServico);
                tablePecaServico.setEnabled(true);
                tablePecaServico.setVisible(true);
                preencheTabela();
                desativaComboBox();
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
               
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
                txtPesquisa.setVisible(true);
                btnGravar.setEnabled(false);
                btnGravar.setVisible(false);
                btnCancelar.setEnabled(false);
                btnCancelar.setVisible(false);
                btnDesativar.setEnabled(true);
            }
            desativaComboBox();
            preencheTabela();
            tablePecaServico.setEnabled(true);
            tablePecaServico.setVisible(true);
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
           
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
            btnGravar.setEnabled(false);
            btnGravar.setVisible(false);
            btnCancelar.setEnabled(false);
            btnCancelar.setVisible(false);

        }

    }

    public void excluiPecaServico() {
        boolean flag;
        int resp = 4;
        PecaServico p;
        p = psDAO.consultaPecaServico(codPecaServico);

        if (psDAO.pendenciaPecaServico(codPecaServico)) {
            if (psDAO.buscaPecaServico(codPecaServico)) {

                if (p.getTipo() == 1) {
                    resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DA PEÇA", "Confirmação", JOptionPane.YES_NO_OPTION);
                } else if (p.getTipo() == 2) {
                    resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A EXCLUSÃO DO SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
                }

                if (resp == 0) {

                    flag = psDAO.excluirPecaServico(codPecaServico);
                    if (flag == true) {

                        if (p.getTipo() == 1) {
                            JOptionPane.showMessageDialog(null, "PEÇA EXCLUÍDA COM SUCESSO");
                        } else if (p.getTipo() == 2) {
                            JOptionPane.showMessageDialog(null, "SERVIÇO EXCLUÍDO COM SUCESSO");
                        }

                        preencheTabela();
                        tablePecaServico.setEnabled(true);
                        tablePecaServico.setVisible(true);

                        ct.clear(panelPecaServico);

                        ct.campoNaoEditavel(panelPecaServico);

                        btnIncluir.setEnabled(true);
                        btnAlterar.setEnabled(true);
                        btnExcluir.setEnabled(true);
                        
                        btnPesquisar.setVisible(true);
                        btnPesquisar.setEnabled(true);
                        btnGravar.setEnabled(false);
                        btnGravar.setVisible(false);
                        btnCancelar.setEnabled(false);
                        btnCancelar.setVisible(false);
                        txtPesquisa.setVisible(false);
                        radioAtivados.setVisible(false);
                        radioDesativados.setVisible(false);
                        

                    }
                } else if (resp == 1) {

                    ct.clear(panelPecaServico);

                    ct.campoNaoEditavel(panelPecaServico);
                    tablePecaServico.setEnabled(true);
                    tablePecaServico.setVisible(true);
                    preencheTabela();

                    btnIncluir.setEnabled(true);
                    btnAlterar.setEnabled(true);
                    btnExcluir.setEnabled(true);
                  
                    btnPesquisar.setVisible(true);
                    btnPesquisar.setEnabled(true);

                }

            } else {
                if (p.getTipo() == 1) {
                    JOptionPane.showMessageDialog(null, "NÃO EXISTE PEÇA ESTE CÓDIGO");
                } else if (p.getTipo() == 2) {
                    JOptionPane.showMessageDialog(null, "NÃO SERVIÇO COM ESTE CÓDIGO");
                }

                ct.clear(panelPecaServico);

                ct.campoNaoEditavel(panelPecaServico);
                tablePecaServico.setEnabled(true);
                tablePecaServico.setVisible(true);
                preencheTabela();
                btnIncluir.setEnabled(true);
                btnAlterar.setEnabled(true);
                btnExcluir.setEnabled(true);
                
                btnPesquisar.setVisible(true);
                btnPesquisar.setEnabled(true);
            }
        } else {

            ct.clear(panelPecaServico);
            if (p.getTipo() == 1) {
                JOptionPane.showMessageDialog(null, "PEÇA ESTÁ SENDO UTILIZADA E NÃO PODE SER EXCLUIDA");
            } else if (p.getTipo() == 2) {
                JOptionPane.showMessageDialog(null, "SERVIÇO ESTÁ SENDO UTILIZADO E NÃO PODE SER EXCLUIDO");
            }

            ct.campoNaoEditavel(panelPecaServico);
            tablePecaServico.setEnabled(true);
            tablePecaServico.setVisible(true);
            preencheTabela();
            desativaComboBox();
            btnIncluir.setEnabled(true);
            btnAlterar.setEnabled(true);
            btnExcluir.setEnabled(true);
            
            btnPesquisar.setVisible(true);
            btnPesquisar.setEnabled(true);
        }

    }

    public void listaTextFiel() {

        int row = tablePecaServico.getSelectedRow();
        String strCod = tablePecaServico.getValueAt(row, 0).toString();
        int codPcSrv = Integer.parseInt(strCod);
        PecaServico ps;

        if (psDAO.buscaPecaServico(codPcSrv)) {
            ps = psDAO.consultaPecaServico(codPcSrv);

            ct.clear(panelPecaServico);

            txtNome.setText(ps.getDescricao());
            combPecaServico.setSelectedIndex(ps.getTipo());
            txtValor.setText(Double.toString(ps.getValor()));

        }
    }

    public String itemTabela() {

        int row = tablePecaServico.getSelectedRow();
        String valor = tablePecaServico.getValueAt(row, 0).toString();

        return valor;
    }

    public void preencheTabelaPesquisa() {

        DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();
        modeloTab.setNumRows(0);
        String str = txtPesquisa.getText();

        if (ct.verificaSeEhNumero(str)) {
            for (PecaServico ps : psDAO.buscaPecaServicoTabela(txtPesquisa.getText().replace(",", "."))) {

                modeloTab.addRow(new Object[]{
                    ps.getCodigo(),
                    ps.getTipo(),
                    ps.getDescricao(),
                    ps.getValor()

                });

            }
        } else {

            for (PecaServico ps : psDAO.buscaPecaServicoTabela(txtPesquisa.getText().toUpperCase())) {

                modeloTab.addRow(new Object[]{
                    ps.getCodigo(),
                    ps.getTipo(),
                    ps.getDescricao(),
                    ps.getValor()

                });

            }
        }

    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();
        modeloTab.setNumRows(0);

        for (PecaServico ps : psDAO.preencheTabela()) {

            modeloTab.addRow(new Object[]{
                ps.getCodigo(),
                ps.getTipo(),
                ps.getDescricao(),
                ps.getValor()

            });

        }

    }

    public void preencheTabelaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();
        modeloTab.setNumRows(0);

        for (PecaServico ps : psDAO.preencheTabelaDesativados()) {

            modeloTab.addRow(new Object[]{
                ps.getCodigo(),
                ps.getTipo(),
                ps.getDescricao(),
                ps.getValor()

            });

        }

    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tablePecaServico.getModel();
        modeloTab.setNumRows(0);
        String str = txtPesquisa.getText();

        if (ct.verificaSeEhNumero(str)) {
            for (PecaServico ps : psDAO.buscaPecaServicoTabelaDesativados(txtPesquisa.getText().replace(",", "."))) {

                modeloTab.addRow(new Object[]{
                    ps.getCodigo(),
                    ps.getTipo(),
                    ps.getDescricao(),
                    ps.getValor()

                });

            }
        } else {

            for (PecaServico ps : psDAO.buscaPecaServicoTabela(txtPesquisa.getText().toUpperCase())) {

                modeloTab.addRow(new Object[]{
                    ps.getCodigo(),
                    ps.getTipo(),
                    ps.getDescricao(),
                    ps.getValor()

                });

            }
        }

    }

    public void desativaComboBox() {

        combPecaServico.setEnabled(false);
        combPecaServico.setSelectedIndex(0);

    }

    public void AtivaComboBox() {
        combPecaServico.setEnabled(true);
        combPecaServico.setSelectedIndex(0);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> combPecaServico;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelPecaServico;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tablePecaServico;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
