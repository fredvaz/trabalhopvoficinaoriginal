package views;

import javax.swing.table.DefaultTableModel;
import models.OrdemServico;
import controllers.OrdemServicoDAO;
import help.CampoTxt;
import help.ConverteData;
import help.DataSistema;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.TableRowSorter;

public final class TelaOsStatus extends javax.swing.JDialog {

    private final OrdemServicoDAO osDAO;
    private final ConverteData conv;
    private final CampoTxt ct;
    private final DateTimeFormatter formatador;
    private final TelaPrincipal tp;
    String opcao;

    String OsAtDt;
    String StatusAndamento;
    private String datafinal;
    String horaAtual;
    String minutoAtual;

    private final DataSistema dtSis;

    public TelaOsStatus(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        osDAO = new OrdemServicoDAO();
        conv = new ConverteData();
        ct = new CampoTxt();
        tp = new TelaPrincipal();
        opcao = "";
        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        tableStatusOs.setRowSorter(new TableRowSorter(modeloTab));

        btnAtivar.setVisible(false);
        radioAtivados.setVisible(false);
        radioDesativados.setVisible(false);
        txtPesquisar.setVisible(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        dtSis = new DataSistema();
        osDAO.verificaAtraso();

        if (tp.grupo.equals("Mecânico")) {
            btnDesativar.setEnabled(false);
            btnAtivar.setEnabled(false);
            preencheTabelaMecanicoLog();
        } else {
            preencheTabela();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAtivaDesativa = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnAlterar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnAtivar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableStatusOs = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnPesquisa = new javax.swing.JButton();
        txtPesquisar = new javax.swing.JTextField();
        btnRestaurar = new javax.swing.JButton();
        radioAtivados = new javax.swing.JRadioButton();
        radioDesativados = new javax.swing.JRadioButton();
        panelStatus = new javax.swing.JPanel();
        jcomboStatusServico = new javax.swing.JComboBox<>();
        btnGravarSatus = new javax.swing.JButton();
        panelData = new javax.swing.JPanel();
        txtDataEntrada = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtHorarioEntrada = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtDataPrevista = new javax.swing.JTextField();
        txtHorarioPrevisto = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Status O.S.");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAlterar.setBackground(new java.awt.Color(204, 204, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Edit_32x32.png"))); // NOI18N
        btnAlterar.setText("Alterar/Detalhar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        btnAlterar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAlterarKeyPressed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(204, 204, 204));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Log Out_32x32.png"))); // NOI18N
        btnSair.setMnemonic('S');
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnDesativar.setBackground(new java.awt.Color(204, 204, 204));
        btnDesativar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/desativa (1).png"))); // NOI18N
        btnDesativar.setText("Desativar");
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        btnAtivar.setBackground(new java.awt.Color(204, 204, 204));
        btnAtivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/ativar (1).png"))); // NOI18N
        btnAtivar.setText("Ativar");
        btnAtivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDesativar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAtivar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAlterar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAtivar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDesativar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSair)
                .addContainerGap())
        );

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tableStatusOs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Placa", "Modelo", "Cor", "Cliente", "Mecanico", "Situação", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableStatusOs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableStatusOsMouseClicked(evt);
            }
        });
        tableStatusOs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableStatusOsKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableStatusOsKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableStatusOs);
        if (tableStatusOs.getColumnModel().getColumnCount() > 0) {
            tableStatusOs.getColumnModel().getColumn(0).setPreferredWidth(3);
            tableStatusOs.getColumnModel().getColumn(1).setPreferredWidth(6);
            tableStatusOs.getColumnModel().getColumn(2).setPreferredWidth(10);
            tableStatusOs.getColumnModel().getColumn(3).setPreferredWidth(10);
            tableStatusOs.getColumnModel().getColumn(4).setPreferredWidth(180);
            tableStatusOs.getColumnModel().getColumn(5).setPreferredWidth(60);
            tableStatusOs.getColumnModel().getColumn(6).setPreferredWidth(40);
            tableStatusOs.getColumnModel().getColumn(7).setPreferredWidth(8);
        }

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setPreferredSize(new java.awt.Dimension(402, 118));

        btnPesquisa.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisa.setMnemonic('P');
        btnPesquisa.setText("Pesquisar");
        btnPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisaActionPerformed(evt);
            }
        });

        txtPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisarKeyPressed(evt);
            }
        });

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('R');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });

        radioAtivados.setBackground(new java.awt.Color(204, 204, 204));
        btnGroupAtivaDesativa.add(radioAtivados);
        radioAtivados.setMnemonic('O');
        radioAtivados.setText("O.S. Ativas");
        radioAtivados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAtivadosActionPerformed(evt);
            }
        });

        radioDesativados.setBackground(new java.awt.Color(204, 204, 204));
        btnGroupAtivaDesativa.add(radioDesativados);
        radioDesativados.setMnemonic('I');
        radioDesativados.setText("O.S. Desativadas");
        radioDesativados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDesativadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnPesquisa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRestaurar))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(radioAtivados)
                        .addGap(18, 18, 18)
                        .addComponent(radioDesativados)))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisa)
                    .addComponent(btnRestaurar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioAtivados)
                    .addComponent(radioDesativados))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        panelStatus.setBackground(new java.awt.Color(204, 204, 204));
        panelStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jcomboStatusServico.setBackground(new java.awt.Color(204, 204, 204));
        jcomboStatusServico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Informe a Situação", "Em andamento", "Aguardando Peças", "Finalizado" }));
        jcomboStatusServico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcomboStatusServicoActionPerformed(evt);
            }
        });
        jcomboStatusServico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jcomboStatusServicoKeyPressed(evt);
            }
        });

        btnGravarSatus.setBackground(new java.awt.Color(204, 204, 204));
        btnGravarSatus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Save_12x12.png"))); // NOI18N
        btnGravarSatus.setMnemonic('G');
        btnGravarSatus.setText("Gravar");
        btnGravarSatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarSatusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelStatusLayout = new javax.swing.GroupLayout(panelStatus);
        panelStatus.setLayout(panelStatusLayout);
        panelStatusLayout.setHorizontalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatusLayout.createSequentialGroup()
                .addGroup(panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStatusLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jcomboStatusServico, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelStatusLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnGravarSatus)))
                .addGap(10, 10, 10))
        );
        panelStatusLayout.setVerticalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatusLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jcomboStatusServico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(btnGravarSatus)
                .addContainerGap())
        );

        panelData.setBackground(new java.awt.Color(204, 204, 204));
        panelData.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtDataEntrada.setEditable(false);

        jLabel1.setText("Data Entrada");

        jLabel2.setText("Horario Entrada");

        txtHorarioEntrada.setEditable(false);

        jLabel3.setText("Data Prevista");

        jLabel4.setText("Horario Previsto");

        txtDataPrevista.setEditable(false);

        txtHorarioPrevisto.setEditable(false);

        javax.swing.GroupLayout panelDataLayout = new javax.swing.GroupLayout(panelData);
        panelData.setLayout(panelDataLayout);
        panelDataLayout.setHorizontalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDataLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(txtDataEntrada)
                    .addComponent(txtHorarioEntrada, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                .addGap(29, 29, 29)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtDataPrevista)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtHorarioPrevisto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
        );
        panelDataLayout.setVerticalGroup(
            panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDataLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDataPrevista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addGap(10, 10, 10)
                .addGroup(panelDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHorarioEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtHorarioPrevisto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(panelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 857, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(panelData, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tableStatusOsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableStatusOsKeyReleased
        listaTextFiel();
    }//GEN-LAST:event_tableStatusOsKeyReleased

    private void tableStatusOsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableStatusOsMouseClicked
        listaTextFiel();
    }//GEN-LAST:event_tableStatusOsMouseClicked

    private void tableStatusOsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableStatusOsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

        }
    }//GEN-LAST:event_tableStatusOsKeyPressed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {
            int row = tableStatusOs.getSelectedRow();
            String valor = tableStatusOs.getValueAt(row, 0).toString();

            opcao = valor;

            TelaAbrirOs tabos = new TelaAbrirOs((Frame) getParent(), true);

            tabos.setOpItem(opcao);
            tabos.ativaAlterarOs();
            tabos.listaTextFiel();
            tabos.setVisible(true);

            if (!valor.equals("")) {
                if (tp.grupo.equals("Mecânico")) {
                    preencheTabelaMecanicoLog();
                } else {
                    preencheTabela();
                }

            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA ORDEM DE SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnAlterarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAlterarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            try {
                int row = tableStatusOs.getSelectedRow();
                String valor = tableStatusOs.getValueAt(row, 0).toString();

                opcao = valor;

                TelaAbrirOs tabos = new TelaAbrirOs((Frame) getParent(), true);

                tabos.setOpItem(opcao);
                tabos.ativaAlterarOs();
                tabos.listaTextFiel();
                tabos.setVisible(true);

                if (!valor.equals("")) {

                    if (tp.grupo.equals("Mecânico")) {
                        preencheTabelaMecanicoLog();
                    } else {
                        preencheTabela();
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "SELECIONE UMA ORDEM DE SERVIÇO PRIMEIRO!");
            }

        }
    }//GEN-LAST:event_btnAlterarKeyPressed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        OsAtDt = "desativado";
        try {
            int row = tableStatusOs.getSelectedRow();
            String valor = tableStatusOs.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A DESATIVAÇAO DA ORDEM DE SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                osDAO.AtivaDesativa(OsAtDt, cod);
                JOptionPane.showMessageDialog(null, "ORDEM DE SERVIÇO FOI DESATIVADA COM SUCESSO");
            }

            if (!valor.equals("")) {

                if (tp.grupo.equals("Mecânico")) {
                    preencheTabelaMecanicoLog();
                } else {
                    preencheTabela();
                }
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA ORDEM DE SERVIÇO PRIMEIRO!");
        }

    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnAtivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarActionPerformed
        OsAtDt = "ativado";
        try {
            int row = tableStatusOs.getSelectedRow();
            String valor = tableStatusOs.getValueAt(row, 0).toString();

            int cod = Integer.parseInt(valor);

            int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ATIVAÇÃO DA ORDEM DE SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                osDAO.AtivaDesativa(OsAtDt, cod);
                JOptionPane.showMessageDialog(null, "ORDEM DE SERVIÇO FOI ATIVADA COM SUCESSO");
            }

            if (!valor.equals("")) {

                if (tp.grupo.equals("Mecânico")) {
                    preencheTabelaDesativadosMecLog();
                } else {
                    preencheTabelaDesativados();
                }
            }
        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA ORDEM DE SERVIÇO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnAtivarActionPerformed

    private void btnPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisaActionPerformed
        radioAtivados.setVisible(true);
        radioDesativados.setVisible(true);
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisar.setVisible(true);
        txtPesquisar.setEnabled(true);
        txtPesquisar.requestFocus();
        radioAtivados.setSelected(true);
    }//GEN-LAST:event_btnPesquisaActionPerformed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed

        if (tp.grupo.equals("Mecânico")) {
            txtPesquisar.setText("");
            btnDesativar.setEnabled(false);
            btnAtivar.setEnabled(false);
            radioAtivados.setVisible(false);
            radioDesativados.setVisible(false);

            txtPesquisar.setVisible(false);
            txtPesquisar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisar.requestFocus();
            preencheTabelaMecanicoLog();
        } else {
            txtPesquisar.setText("");
            preencheTabela();
            btnAtivar.setVisible(false);
            btnAtivar.setEnabled(false);
            radioAtivados.setVisible(false);
            radioDesativados.setVisible(false);

            txtPesquisar.setVisible(false);
            txtPesquisar.setEnabled(false);
            btnRestaurar.setVisible(false);
            btnRestaurar.setEnabled(false);
            txtPesquisar.requestFocus();

        }

    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void txtPesquisarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnRestaurar.setVisible(true);
            btnRestaurar.setEnabled(true);
            if (radioAtivados.isSelected()) {

                if (tp.grupo.equals("Mecânico")) {
                    preencheTabelaPesquisaAtivadosMecLog();
                } else {
                    preencheTabelaPesquisaAtivados();
                }

            } else if (radioDesativados.isSelected()) {

                if (tp.grupo.equals("Mecânico")) {
                    preencheTabelaPesquisaDesativadosMecLog();
                } else {
                    preencheTabelaPesquisaDesativados();
                }

            }

        }
    }//GEN-LAST:event_txtPesquisarKeyPressed

    private void radioAtivadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAtivadosActionPerformed

        if (tp.grupo.equals("Mecânico")) {
            txtPesquisar.setText("");
            btnDesativar.setEnabled(false);
            btnAtivar.setEnabled(false);
            preencheTabelaMecanicoLog();
            txtPesquisar.requestFocus();
        } else {
            txtPesquisar.setText("");
            preencheTabela();
            btnAtivar.setVisible(false);
            btnAtivar.setEnabled(false);
            btnDesativar.setEnabled(true);
             txtPesquisar.requestFocus();
        }

    }//GEN-LAST:event_radioAtivadosActionPerformed

    private void radioDesativadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDesativadosActionPerformed

        if (tp.grupo.equals("Mecânico")) {
            txtPesquisar.setText("");
            btnDesativar.setEnabled(false);
            btnAtivar.setEnabled(false);
            preencheTabelaDesativadosMecLog();
             txtPesquisar.requestFocus();
        } else {
            txtPesquisar.setText("");
            preencheTabelaDesativados();
            btnAtivar.setVisible(true);
            btnAtivar.setEnabled(true);
             txtPesquisar.requestFocus();
        }

    }//GEN-LAST:event_radioDesativadosActionPerformed

    private void btnGravarSatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarSatusActionPerformed

        try {

            if (jcomboStatusServico.getSelectedIndex() == 0) {

                JOptionPane.showMessageDialog(null, "SELECIONE UM STATUS PRIMEIRO!");
                jcomboStatusServico.requestFocus();

            } else if (jcomboStatusServico.getSelectedIndex() == 3) {

                datafinal = dtSis.dataSis();
                LocalDate datasaida = conv.converterStringtoLocalDate(datafinal);
                LocalTime horariosaida = LocalTime.of(dtSis.horaSis(), dtSis.minutoSis());
                int row = tableStatusOs.getSelectedRow();
                String valor = tableStatusOs.getValueAt(row, 0).toString();

                int cod = Integer.parseInt(valor);

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DE STATUS DA ORDEM DE SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    try {
                        osDAO.alteraStatusAndamentoFinalizado(StatusAndamento, cod, datasaida, horariosaida);
                        JOptionPane.showMessageDialog(null, "STATUS DA ORDEM DE SERVIÇO FOI ALTERADA COM SUCESSO");
                        jcomboStatusServico.setSelectedIndex(0);
                        tp.atualizaInformativo();
                    } catch (Exception ex) {
                        Logger.getLogger(TelaOsStatus.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

                if (!valor.equals("")) {

                    if (tp.grupo.equals("Mecânico")) {
                        preencheTabelaMecanicoLog();
                    } else {
                        preencheTabela();
                    }
                }

            } else {

                int row = tableStatusOs.getSelectedRow();
                String valor = tableStatusOs.getValueAt(row, 0).toString();

                int cod = Integer.parseInt(valor);

                int resp = JOptionPane.showConfirmDialog(null, "CONFIRMA A ALTERAÇÃO DE STATUS DA ORDEM DE SERVIÇO", "Confirmação", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    osDAO.alteraStatusAndamento(StatusAndamento, cod);
                    JOptionPane.showMessageDialog(null, "STATUS DA ORDEM DE SERVIÇO FOI ALTERADA COM SUCESSO");
                }

                if (!valor.equals("")) {

                    if (tp.grupo.equals("Mecânico")) {
                        btnDesativar.setEnabled(false);
                        btnAtivar.setEnabled(false);
                        preencheTabelaMecanicoLog();
                    } else {
                        preencheTabela();
                    }

                }
            }

        } catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA ORDEM DE SERVIÇO PRIMEIRO!");
        }


    }//GEN-LAST:event_btnGravarSatusActionPerformed

    private void jcomboStatusServicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcomboStatusServicoActionPerformed
        if (jcomboStatusServico.getSelectedIndex() != 0) {
            StatusAndamento = jcomboStatusServico.getSelectedItem().toString();
        }
    }//GEN-LAST:event_jcomboStatusServicoActionPerformed

    private void jcomboStatusServicoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jcomboStatusServicoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (jcomboStatusServico.getSelectedIndex() != 0) {
                StatusAndamento = jcomboStatusServico.getSelectedItem().toString();
            }
        }
    }//GEN-LAST:event_jcomboStatusServicoKeyPressed

    public String itemTabela() {

        int row = tableStatusOs.getSelectedRow();
        String valor = tableStatusOs.getValueAt(row, 0).toString();

        return valor;
    }

    public void listaTextFiel() {

        int row = tableStatusOs.getSelectedRow();
        String strCod = tableStatusOs.getValueAt(row, 0).toString();
        int codOs;
        codOs = Integer.parseInt(strCod);
        OrdemServico os;
        os = osDAO.consultaOrdemServico(codOs);

        if (osDAO.buscaOs(codOs)) {

            ct.clear(panelData);
            ct.clear(panelStatus);

            txtDataEntrada.setText(formatador.format(os.getDataEntrada()));
            txtDataPrevista.setText(formatador.format(os.getDataPrevista()));
            int horapr = os.getHorarioPrevisto().getHour();
            int minpr = os.getHorarioPrevisto().getMinute();
            int horat = os.getHorarioEntrada().getHour();
            int minat = os.getHorarioEntrada().getMinute();
            txtHorarioEntrada.setText(os.getHorarioEntrada().toString());

            txtHorarioPrevisto.setText(os.getHorarioPrevisto().toString());

        }
    }

    public void preencheTabelaPesquisaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaPesquisaDesativados(txtPesquisar.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
               os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaPesquisaDesativadosMecLog() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaPesquisaDesativadosMecLog(txtPesquisar.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaPesquisaAtivados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaPesquisaAtiva(txtPesquisar.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaPesquisaAtivadosMecLog() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaPesquisaAtivaMecLog(txtPesquisar.getText().toUpperCase())) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabela()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaMecanicoLog() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaMecanicoLogado()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaDesativados() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaDesativados()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                os.getValor_totalStr()

            });

        }

    }

    public void preencheTabelaDesativadosMecLog() {

        DefaultTableModel modeloTab = (DefaultTableModel) tableStatusOs.getModel();
        modeloTab.setNumRows(0);

        for (OrdemServico os : osDAO.preencheTabelaDesativadosMecLog()) {

            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getPlacaVeiculo(),
                os.getNomeModelo(),
                os.getCorVeiculo(),
                os.getNomeCliente(),
                os.getNomeMecanico(),
                os.getStatus(),
                 os.getValor_totalStr()

            });

        }

    }

    public void verificaAtraso() {

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtivar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnGravarSatus;
    private javax.swing.ButtonGroup btnGroupAtivaDesativa;
    private javax.swing.JButton btnPesquisa;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcomboStatusServico;
    private javax.swing.JPanel panelData;
    private javax.swing.JPanel panelStatus;
    private javax.swing.JRadioButton radioAtivados;
    private javax.swing.JRadioButton radioDesativados;
    private javax.swing.JTable tableStatusOs;
    private javax.swing.JTextField txtDataEntrada;
    private javax.swing.JTextField txtDataPrevista;
    private javax.swing.JTextField txtHorarioEntrada;
    private javax.swing.JTextField txtHorarioPrevisto;
    private javax.swing.JTextField txtPesquisar;
    // End of variables declaration//GEN-END:variables

}
