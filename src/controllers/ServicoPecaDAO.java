package controllers;

import bankConnection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.PecaServico;

public class ServicoPecaDAO {

    String sql;
    Connection con;
    Connection conEnd;
    PreparedStatement stm;
    private PecaServico pecaServico;
    int cod;

    public ServicoPecaDAO() {

        sql = "";
        con = null;
        stm = null;
        cod = 0;

    }

    public boolean VerificaPecaServico() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from pecaServico ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

    public boolean addPecaServico(PecaServico ps) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {

            sql = "insert into pecaServico (descricao, valor,tipo,pecaServicoStatus)"
                    + " values (?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, ps.getDescricao().toUpperCase());
            stm.setDouble(2, ps.getValor());
            stm.setInt(3, ps.getTipo());
            stm.setString(4, "ativado");

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSÍVEL EFETUAR O CADASTRO NA BASE DE DADOS");
        }
        return flag;

    }

    public boolean alterarPecaServico(PecaServico pecaServico, int cod) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        try {

            sql = "update pecaServico set descricao = ?, valor = ?,tipo = ? where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, pecaServico.getDescricao().toUpperCase());
            stm.setDouble(2, pecaServico.getValor());
            stm.setInt(3, pecaServico.getTipo());
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;

        }
        return flag;
    }

    public boolean excluirPecaServico(int cod) {

        boolean flag = false;
        try {
            sql = "delete from pecaServico where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();

            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "PEÇA OU SERVIÇO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }
        return flag;
    }

    public ArrayList<PecaServico> buscaPecaServicoTabela(String pesquisa) {

        ArrayList<PecaServico> pslist = new ArrayList();
        ResultSet rs;

        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from(select * from pecaServico  where pecaServicoStatus = 'desativado') as p where p.descricao like " + pesquisa2 + "or cast(p.valor as varchar)like " + pesquisa2 + "or cast(p.tipo as varchar) like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    PecaServico ps = new PecaServico();
                    ps.setCodigo(rs.getInt("codigo"));
                    ps.setDescricao(rs.getString("descricao"));
                    ps.setValor(rs.getDouble("valor"));
                    ps.setTipo(rs.getInt("tipo"));

                    pslist.add(ps);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return pslist;
    }

    public boolean buscaPecaServico(int cod) {
        boolean flag = false;
        try {
            sql = "select * from pecaServico";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public ArrayList<PecaServico> preencheTabela() {

        ArrayList<PecaServico> pslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from pecaServico where pecaServicoStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    PecaServico ps = new PecaServico();

                    ps.setCodigo(rs.getInt("codigo"));
                    ps.setDescricao(rs.getString("descricao"));
                    ps.setValor(rs.getDouble("valor"));
                    ps.setTipo(rs.getInt("tipo"));

                    pslist.add(ps);

                } catch (SQLException e) {
                    rs.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return pslist;
    }

    public boolean pendenciaPecaServico(int cod) {
        boolean flag = false;
        int cont = 0;
        try {
            sql = "select * from ospecasservicos where cod_peca_servico = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }
           
            stm.close();
             con.close();
            if (cont >= 1) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "PEÇA OU SERVIÇO NÃO FOI ENCONTRADO NA BASE DE DADOS");

        }

        return flag;
    }

    public PecaServico consultaPecaServico(int cod) {

        try {
            sql = "select * from pecaServico where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("codigo") == cod) {

                    pecaServico = new PecaServico();

                    pecaServico.setCodigo(rs.getInt("codigo"));
                    pecaServico.setDescricao(rs.getString("descricao"));
                    pecaServico.setValor(rs.getDouble("valor"));
                    pecaServico.setTipo(rs.getInt("tipo"));

                }
            }
           
            stm.close();
             con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return pecaServico;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update pecaServico set pecaServicoStatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean consultaStatusPecaServico(int codigo) {
        boolean retorna = false;
        try {
            sql = "select * from pecaServico where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("pecaServicoStatus").equals("ativado")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public ArrayList<PecaServico> preencheTabelaDesativados() {

        ArrayList<PecaServico> pslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from pecaServico where pecaServicoStatus = 'desativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    PecaServico ps = new PecaServico();

                    ps.setCodigo(rs.getInt("codigo"));
                    ps.setDescricao(rs.getString("descricao"));
                    ps.setValor(rs.getDouble("valor"));
                    ps.setTipo(rs.getInt("tipo"));

                    pslist.add(ps);

                } catch (SQLException e) {
                    rs.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return pslist;
    }

    public ArrayList<PecaServico> buscaPecaServicoTabelaDesativados(String pesquisa) {

        ArrayList<PecaServico> pslist = new ArrayList();
        ResultSet rs;

        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from(select * from pecaServico  where pecaServicoStatus = 'desativado') as p where p.descricao like " + pesquisa2 + "or cast(p.valor as varchar)like " + pesquisa2 + "or cast(p.tipo as varchar) like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    PecaServico ps = new PecaServico();
                    ps.setCodigo(rs.getInt("codigo"));
                    ps.setDescricao(rs.getString("descricao"));
                    ps.setValor(rs.getDouble("valor"));
                    ps.setTipo(rs.getInt("tipo"));

                    pslist.add(ps);

                } catch (SQLException e) {
                    rs.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return pslist;
    }

}
