package controllers;

import bankConnection.Conexao;
import help.ConverteData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.Cliente;
import models.PessoaFisica;
import models.PessoaJuridica;

public class ClienteDAO {

    String sql;
    String sql2;
    String sqlEnd;
    Connection con;
    Connection conEnd;
    PreparedStatement stm;
    PreparedStatement stmEnd;
    private Cliente pessoa;
    int cod;
    private final ConverteData conv = new ConverteData();

    public ClienteDAO() {
        sql = "";
        sql2 = "";
        con = null;
        stm = null;
        cod = 0;
    }

    @SuppressWarnings("FinallyDiscardsException")
    public boolean VerificaCliente() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from clientes ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean addClientePessoaFisica(PessoaFisica pf) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {
            sql = "insert into clientes (nome, cpf_cnpj, tipo_pessoa, rg, dataNasc, email, telefone, rua, numest, bairro, municipio, estado,complemento,clienteStatus)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, pf.getNome().toUpperCase());
            stm.setString(2, pf.getCpfCnpj().toUpperCase());
            stm.setInt(3, pf.getTipoPessoa());
            stm.setString(4, pf.getRgIe().toUpperCase().toUpperCase());
            stm.setDate(5, conv.converteLocalDatetoDate(pf.getDataNascimento()));
            stm.setString(6, pf.getEmail().toUpperCase());
            stm.setString(7, pf.getTelefone().toUpperCase());
            stm.setString(8, pf.getLogradouro().toUpperCase());
            stm.setString(9, pf.getNumero().toUpperCase());
            stm.setString(10, pf.getBairro().toUpperCase());
            stm.setString(11, pf.getMunicipio().toUpperCase());
            stm.setString(12, pf.getEstado().toUpperCase());
            stm.setString(13, pf.getComplemento().toUpperCase());
            stm.setString(14, "ativado");

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "e");
        }
        return flag;

    }

    public boolean addClientePessoaJuridica(PessoaJuridica pj) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {
            sql = "insert into clientes ( razaosocial, cpf_cnpj, tipo_pessoa, rg, dataNasc, email, telefone, rua, numest, bairro, municipio, estado,complemento,nome,clienteStatus) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, pj.getRazaoSocial().toUpperCase());
            stm.setString(2, pj.getCpfCnpj().toUpperCase());
            stm.setInt(3, pj.getTipoPessoa());
            stm.setString(4, pj.getRgIe().toUpperCase());
            stm.setDate(5, conv.converteLocalDatetoDate(pj.getDataNascimento()));
            stm.setString(6, pj.getEmail().toUpperCase());
            stm.setString(7, pj.getTelefone().toUpperCase());
            stm.setString(8, pj.getLogradouro().toUpperCase());
            stm.setString(9, pj.getNumero().toUpperCase());
            stm.setString(10, pj.getBairro().toUpperCase());
            stm.setString(11, pj.getMunicipio().toUpperCase());
            stm.setString(12, pj.getEstado().toUpperCase());
            stm.setString(13, pj.getComplemento().toUpperCase());
            stm.setString(14, pj.getNomeFantasia().toUpperCase());
            stm.setString(15, "ativado");
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "O CNPJ JÁ ESTÁ CADASTRADO");
        }
        return flag;

    }

    public boolean buscaCliente(int cod) {
        boolean flag = false;
        try {
            sql = "select * from clientes";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public Cliente consultaCliente(int cod) {

        try {
            sql = "select * from clientes where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("codigo") == cod) {

                    if (rs.getInt("tipo_pessoa") == 1) {
                        pessoa = new PessoaFisica();
                        ((PessoaFisica) pessoa).setCodigo(rs.getInt("codigo"));
                        ((PessoaFisica) pessoa).setNome(rs.getString("nome"));
                        ((PessoaFisica) pessoa).setCpfCnpj(rs.getString("cpf_cnpj"));
                        ((PessoaFisica) pessoa).setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaFisica) pessoa).setRgIe(rs.getString("rg"));
                        ((PessoaFisica) pessoa).setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        ((PessoaFisica) pessoa).setEmail(rs.getString("email"));
                        ((PessoaFisica) pessoa).setTelefone(rs.getString("telefone"));
                        ((PessoaFisica) pessoa).setLogradouro(rs.getString("rua"));
                        ((PessoaFisica) pessoa).setNumero(rs.getString("numest"));
                        ((PessoaFisica) pessoa).setBairro(rs.getString("bairro"));
                        ((PessoaFisica) pessoa).setMunicipio(rs.getString("municipio"));
                        ((PessoaFisica) pessoa).setEstado(rs.getString("estado"));
                        ((PessoaFisica) pessoa).setComplemento(rs.getString("complemento"));

                    } else if (rs.getInt("tipo_pessoa") == 2) {
                        pessoa = new PessoaJuridica();
                        ((PessoaJuridica) pessoa).setCodigo(rs.getInt("codigo"));
                        ((PessoaJuridica) pessoa).setCpfCnpj(rs.getString("cpf_cnpj"));
                        ((PessoaJuridica) pessoa).setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaJuridica) pessoa).setRazaoSocial(rs.getString("razaosocial"));
                        ((PessoaJuridica) pessoa).setNomeFantasia(rs.getString("nome"));
                        ((PessoaJuridica) pessoa).setRgIe(rs.getString("rg"));
                        ((PessoaJuridica) pessoa).setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        ((PessoaJuridica) pessoa).setEmail(rs.getString("email"));
                        ((PessoaJuridica) pessoa).setTelefone(rs.getString("telefone"));
                        ((PessoaJuridica) pessoa).setLogradouro(rs.getString("rua"));
                        ((PessoaJuridica) pessoa).setNumero(rs.getString("numest"));
                        ((PessoaJuridica) pessoa).setBairro(rs.getString("bairro"));
                        ((PessoaJuridica) pessoa).setMunicipio(rs.getString("municipio"));
                        ((PessoaJuridica) pessoa).setEstado(rs.getString("estado"));
                        ((PessoaJuridica) pessoa).setComplemento(rs.getString("complemento"));

                    }

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return pessoa;
    }

    public PessoaJuridica consultaClienteJ(int cod) {
        try {
            sql = "select * from clientes where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                pessoa = new PessoaJuridica();
                if (rs.getInt("codigo") == cod) {
                    pessoa.setCodigo(rs.getInt("codigo"));
                    ((PessoaJuridica) pessoa).setCpfCnpj(rs.getString("cpf_cnpj"));
                    pessoa.setTipoPessoa(rs.getInt("tipo_pessoa"));
                    ((PessoaJuridica) pessoa).setRazaoSocial(rs.getString("razaosocial"));
                    ((PessoaJuridica) pessoa).setNomeFantasia(rs.getString("nome"));
                    ((PessoaJuridica) pessoa).setRgIe(rs.getString("rg"));
                    pessoa.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                    pessoa.setEmail(rs.getString("email"));
                    pessoa.setTelefone(rs.getString("telefone"));
                    pessoa.setLogradouro(rs.getString("rua"));
                    pessoa.setNumero(rs.getString("numest"));
                    pessoa.setBairro(rs.getString("bairro"));
                    pessoa.setMunicipio(rs.getString("municipio"));
                    pessoa.setEstado(rs.getString("estado"));
                    pessoa.setComplemento(rs.getString("complemento"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return (PessoaJuridica) pessoa;
    }

    public boolean alterarClienteF(PessoaFisica pessoa, int cod) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        try {
            sql = "update clientes set nome = ?, cpf_cnpj = ?,tipo_pessoa = ?, rg = ?,"
                    + "dataNasc = ?,email = ?,telefone = ?,rua = ?,numest = ?, bairro = ?,municipio = ?,estado = ?,"
                    + "complemento = ? where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, pessoa.getNome().toUpperCase());
            stm.setString(2, pessoa.getCpfCnpj().toUpperCase());
            stm.setInt(3, pessoa.getTipoPessoa());
            stm.setString(4, pessoa.getRgIe().toUpperCase());
            stm.setDate(5, conv.converteLocalDatetoDate(pessoa.getDataNascimento()));
            stm.setString(6, pessoa.getEmail().toUpperCase());
            stm.setString(7, pessoa.getTelefone().toUpperCase());
            stm.setString(8, pessoa.getLogradouro().toUpperCase());
            stm.setString(9, pessoa.getNumero().toUpperCase());
            stm.setString(10, pessoa.getBairro().toUpperCase());
            stm.setString(11, pessoa.getMunicipio().toUpperCase());
            stm.setString(12, pessoa.getEstado().toUpperCase());
            stm.setString(13, pessoa.getComplemento().toUpperCase());

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            if (pessoa.getTipoPessoa() == 1) {
                JOptionPane.showMessageDialog(null, "O CPF JÁ ESTÁ CADASTRADO");
            } else if (pessoa.getTipoPessoa() == 2) {
                JOptionPane.showMessageDialog(null, "O CNPJ JÁ ESTÁ CADASTRADO");
            }

        }
        return flag;
    }

    public boolean alterarClienteJ(PessoaJuridica pessoa, int cod) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        try {
            sql = "update clientes set  cpf_cnpj = ?,tipo_pessoa = ?,razaosocial = ?, rg = ?,"
                    + "dataNasc = ?,email = ?,telefone = ?,rua = ?,numest = ?, bairro = ?,municipio = ?,estado = ?,"
                    + "complemento = ?,nome = ?"
                    + " where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, pessoa.getCpfCnpj().toUpperCase());
            stm.setInt(2, pessoa.getTipoPessoa());
            stm.setString(3, pessoa.getRazaoSocial().toUpperCase());
            stm.setString(4, pessoa.getRgIe().toUpperCase());
            stm.setDate(5, conv.converteLocalDatetoDate(pessoa.getDataNascimento()));
            stm.setString(6, pessoa.getEmail().toUpperCase());
            stm.setString(7, pessoa.getTelefone().toUpperCase());
            stm.setString(8, pessoa.getLogradouro().toUpperCase());
            stm.setString(9, pessoa.getNumero().toUpperCase());
            stm.setString(10, pessoa.getBairro().toUpperCase());
            stm.setString(11, pessoa.getMunicipio().toUpperCase());
            stm.setString(12, pessoa.getEstado().toUpperCase());
            stm.setString(13, pessoa.getComplemento().toUpperCase());
            stm.setString(14, pessoa.getNomeFantasia().toUpperCase());

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            if (pessoa.getTipoPessoa() == 1) {
                JOptionPane.showMessageDialog(null, "O CPF JÁ ESTÁ CADASTRADO");
            } else if (pessoa.getTipoPessoa() == 2) {
                JOptionPane.showMessageDialog(null, "O CNPJ JÁ ESTÁ CADASTRADO");
            }

        }
        return flag;
    }

    public boolean pendenciaCliente(int cod) {
        boolean flag = false;
        int cont = 0;
        try {
            sql = "select * from veiculos where cod_cliente = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {

                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "CLIENTE NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }

        return flag;
    }

    public boolean excluirCliente(int cod) {

        boolean flag = false;
        try {
            sql = "delete from clientes where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();
            con.close();
            stm.close();

            flag = true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, e);
        }
        return flag;
    }

    public boolean existeCliente(String cpf) {
        boolean flag = false;
        String cpf2 = "'" + cpf + "'";
        try {
            sql = "select * from clientes where cpf_cnpj = " + cpf2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {

                if (rs.getString("cpf_cnpj").equals(cpf)) {

                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return flag;
    }

    @SuppressWarnings("FinallyDiscardsException")
    public boolean existeClienteCad() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from clientes ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public ArrayList<Cliente> preencheTabela() {

        ArrayList<Cliente> clientesList = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from clientes where clienteStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {
                    if (rs.getInt("tipo_pessoa") == 1) {
                        Cliente c = new PessoaFisica();

                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaFisica) c).setNome(rs.getString("nome"));
                        ((PessoaFisica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaFisica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    } else if (rs.getInt("tipo_pessoa") == 2) {
                        Cliente c = new PessoaJuridica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaJuridica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaJuridica) c).setRazaoSocial(rs.getString("razaosocial"));
                        ((PessoaJuridica) c).setNomeFantasia(rs.getString("nome"));
                        ((PessoaJuridica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return clientesList;
    }

    public ArrayList<Cliente> buscaClienteTabela(String pesquisa) {

        ArrayList<Cliente> clientesList = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from (select * from clientes  where clientestatus = 'ativado') as c where c.nome  like" + pesquisa2 + "or c.razaosocial like" + pesquisa2 + " or c.cpf_cnpj like" + pesquisa2 + "or c.telefone like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {
                    if (rs.getInt("tipo_pessoa") == 1) {
                        Cliente c = new PessoaFisica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaFisica) c).setNome(rs.getString("nome"));
                        ((PessoaFisica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaFisica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    } else if (rs.getInt("tipo_pessoa") == 2) {
                        Cliente c = new PessoaJuridica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaJuridica) c).setCpfCnpj(rs.getString("cpf_cnpj"));

                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaJuridica) c).setRazaoSocial(rs.getString("razaosocial"));
                        ((PessoaJuridica) c).setNomeFantasia(rs.getString("nome"));
                        ((PessoaJuridica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return clientesList;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update clientes set clienteStatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean consultaStatusCliente(int codigo) {
        boolean retorna = false;
        try {
            sql = "select * from clientes where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("clienteStatus").equals("ativado")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }
            con.close();
            stm.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public ArrayList<Cliente> preencheTabelaDesativados() {

        ArrayList<Cliente> clientesList = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from clientes where clienteStatus = 'desativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {
                    if (rs.getInt("tipo_pessoa") == 1) {
                        Cliente c = new PessoaFisica();

                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaFisica) c).setNome(rs.getString("nome"));
                        ((PessoaFisica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaFisica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    } else if (rs.getInt("tipo_pessoa") == 2) {
                        Cliente c = new PessoaJuridica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaJuridica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaJuridica) c).setRazaoSocial(rs.getString("razaosocial"));
                        ((PessoaJuridica) c).setNomeFantasia(rs.getString("nome"));
                        ((PessoaJuridica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return clientesList;
    }

    public ArrayList<Cliente> buscaClienteTabelaDesativados(String pesquisa) {

        ArrayList<Cliente> clientesList = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from (select * from clientes  where clientestatus = 'desativado') as c where c.nome  like" + pesquisa2 + "or c.razaosocial like" + pesquisa2 + " or c.cpf_cnpj like" + pesquisa2 + "or c.telefone like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {
                    if (rs.getInt("tipo_pessoa") == 1) {
                        Cliente c = new PessoaFisica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaFisica) c).setNome(rs.getString("nome"));
                        ((PessoaFisica) c).setCpfCnpj(rs.getString("cpf_cnpj"));
                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaFisica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    } else if (rs.getInt("tipo_pessoa") == 2) {
                        Cliente c = new PessoaJuridica();
                        c.setCodigo(rs.getInt("codigo"));
                        ((PessoaJuridica) c).setCpfCnpj(rs.getString("cpf_cnpj"));

                        c.setTipoPessoa(rs.getInt("tipo_pessoa"));
                        ((PessoaJuridica) c).setRazaoSocial(rs.getString("razaosocial"));
                        ((PessoaJuridica) c).setNomeFantasia(rs.getString("nome"));
                        ((PessoaJuridica) c).setRgIe(rs.getString("rg"));
                        c.setDataNascimento(conv.converteDatetoLocalDate(rs.getDate("dataNasc")));
                        c.setEmail(rs.getString("email"));
                        c.setTelefone(rs.getString("telefone"));
                        c.setLogradouro(rs.getString("rua"));
                        c.setNumero(rs.getString("numest"));
                        c.setBairro(rs.getString("bairro"));
                        c.setMunicipio(rs.getString("municipio"));
                        c.setEstado(rs.getString("estado"));
                        c.setComplemento(rs.getString("complemento"));
                        clientesList.add(c);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return clientesList;
    }

}
