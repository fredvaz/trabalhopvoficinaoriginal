package controllers;

import bankConnection.Conexao;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import models.Usuario;

public class LoginDAO {

    String sql;
    Connection con;
    PreparedStatement stm;
    ResultSet rs;
    
    private Usuario usuario;
    int cod;
    public int codigoUser;

    public LoginDAO() {
        sql = "";
        con = null;
        stm = null;
        rs = null;
    }

    public boolean adicionarUsuario(Usuario user) {
        boolean ok = false;
        try {
            MessageDigest md5 = MessageDigest.getInstance("SHA-256");
            byte arrayByte[] = md5.digest(user.getSenha().getBytes("UTF-8"));
            StringBuilder str = new StringBuilder();

            for (byte arrayB : arrayByte) {

                str.append(String.format("%02X", 0xFF & arrayB));

            }
            String password = str.toString();

            sql = "insert into usuario (usuario,cpf, senha,tipo,editar,inserir,excluir,usuarioStatus) values (?,?,?,?,?,?,?,?)";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user.getNome().toUpperCase());
            stm.setString(2, user.getCpfCnpj());
            stm.setString(3, password);

            stm.setString(4, user.getGrupo());
            stm.setBoolean(5, user.isAlterar());
            stm.setBoolean(6, user.isInserir());
            stm.setBoolean(7, user.isExcluir());
            stm.setString(8, "ativado");
            stm.execute();
            stm.close();
            con.close();
            ok = true;
        } catch (SQLException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            JOptionPane.showMessageDialog(null, "ESTE USUÁRIO JÁ ESTÁ CADASTRADO");

        }
        return ok;

    }

    public boolean alterarUsuario(Usuario user, int cod) {
        boolean flag = false;
        try {
            MessageDigest md5 = MessageDigest.getInstance("SHA-256");
            byte arrayByte[] = md5.digest(user.getSenha().getBytes("UTF-8"));
            StringBuilder str = new StringBuilder();

            for (byte arrayB : arrayByte) {

                str.append(String.format("%02X", 0xFF & arrayB));

            }
            String password = str.toString();

            sql = "update usuario set usuario = ? , senha = ? ,cpf=? , tipo = ?,editar = ?,inserir = ?, excluir = ? where codigo = " + cod;

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user.getNome());
            stm.setString(2, password);
            stm.setString(3, user.getCpfCnpj());
            stm.setString(4, user.getGrupo());
            stm.setBoolean(5, user.isAlterar());
            stm.setBoolean(6, user.isInserir());
            stm.setBoolean(7, user.isExcluir());

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return flag;

    }

    public boolean login(String usuario, String senha) {
        boolean valido = false;
        try {

            MessageDigest md5 = MessageDigest.getInstance("SHA-256");
            byte arrayByte[] = md5.digest(senha.getBytes("UTF-8"));
            StringBuilder str = new StringBuilder();

            for (byte arrayB : arrayByte) {

                str.append(String.format("%02X", 0xFF & arrayB));

            }
            String password = str.toString();

            sql = "select * from usuario where usuario = ? and senha = ? and usuarioStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, usuario);
            stm.setString(2, password);
            rs = stm.executeQuery();

            if (rs.next()) {
                valido = true;
                codigoUser = rs.getInt("codigo");
            }
            
            stm.close();
            rs.close();
            con.close();
        } catch (SQLException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valido;

    }

    public void usuarioAtual(String usuario, int cod) {
        try {
            sql = "insert into user_atual (nome,codigo) values (?,?)";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, usuario);
            stm.setInt(2, cod);
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public String buscarUltimoLogin() {
        String usu = null;
        try {
            sql = "select * from user_atual where id in (select max (id) from user_atual)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                usu = rs.getString("nome");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return usu;

    }

    public int buscarUltimoLoginCodigo() {
        int codigo = 0;
        try {
            sql = "select * from user_atual where id in (select max (id) from user_atual)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                codigo = rs.getInt("codigo");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return codigo;

    }

    public void truncateUser() {
        try {
            sql = "truncate user_atual";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }

    public boolean buscaUsuario(int cod) {
        boolean flag = false;
        try {
            sql = "select * from usuario";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public int buscarUsuarioNome(String nome) {
        int codigo = 0;
        try {
            sql = "select * from usuario";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getString("usuario").equalsIgnoreCase(nome)) {
                    codigo = rs.getInt("codigo");

                }
            }

            
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return codigo;
    }

    public boolean pendenciaUsuário(int cod) {
        boolean flag = false;
        int cont = 0;
        try {
            sql = "select * from os where cod_mecanico = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                cont++;
            }
            
            stm.close();
            con.close();
            if (cont >= 1) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "USUÁRIO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }

        return flag;
    }

    public Usuario consultaUsuario(int cod) {

        try {
            sql = "select * from usuario where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();

            while (rst.next()) {

                if (rst.getInt("codigo") == cod) {

                    usuario = new Usuario();

                    usuario.setCodigousuario(rst.getInt("codigo"));
                    usuario.setNome(rst.getString("usuario"));
                    usuario.setCpfCnpj(rst.getString("cpf"));
                    usuario.setGrupo(rst.getString("tipo"));
                    usuario.setStatusUsuario(rst.getString("usuarioStatus"));

                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return usuario;
    }

    public String permitir(String user) {
        String tipo = "";
        try {
            sql = "select * from usuario where usuario = ?";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user);
            rs = stm.executeQuery();

            while (rs.next()) {
                tipo = rs.getString("tipo");
            }
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return tipo;

    }

    public boolean VerificaUsuario() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from usuario";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                cont++;
            }
            
            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else if (cont <= 0) {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean VerificaUsuarioDesativa() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from (select * from usuario  where tipo = 'Administrador') as u where usuarioStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();

            while (rst.next()) {
                cont++;
            }

            
            stm.close();
            con.close();

            if (cont >= 2) {
                retorna = false;
            } else if (cont <= 1) {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean ConsultaUsuarioTipo(int cod) {

        boolean retorna = false;
        try {
            sql = "select * from usuario where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("tipo").equals("Administrador")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }
            
            stm.close();
            con.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean consultaStatusUsuario(int codigo) {
        boolean retorna = false;
        try {
            sql = "select * from usuario where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("usuarioStatus").equals("ativado")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }
           
            stm.close();
             con.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean VerificaUserAtual() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from user_atual";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                cont++;
            }
          
            stm.close();
              con.close();
            if (cont >= 1) {
                retorna = false;
            } else if (cont <= 0) {
                retorna = true;

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public boolean excluirUsuario(int cod) {
        boolean flag = false;
        try {
            sql = "delete from usuario where codigo = ?";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setInt(1, cod);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update usuario set usuarioStatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean permitirEx(String user) {
        boolean perm = false;
        try {
            sql = "select * from usuario where usuario = ?";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user);
            rs = stm.executeQuery();

            while (rs.next()) {
                perm = rs.getBoolean("excluir");

            }
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return perm;

    }

    public boolean permitirAlt(String user) {
        boolean perm = false;
        try {
            sql = "select * from usuario where usuario = ?";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user);
            rs = stm.executeQuery();

            while (rs.next()) {
                perm = rs.getBoolean("editar");

            }
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return perm;

    }

    public boolean permitirIns(String user) {
        boolean perm = false;
        try {
            sql = "select * from usuario where usuario = ?";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, user);
            rs = stm.executeQuery();

            while (rs.next()) {
                perm = rs.getBoolean("inserir");

            }
            stm.execute();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return perm;

    }

    public ArrayList<Usuario> buscaUsuarioTabela(String pesquisa) {

        ArrayList<Usuario> userlist = new ArrayList();
        ResultSet rst;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from usuario as u where u.usuario like " + pesquisa2 + "or u.cpf like " + pesquisa2 + "or u.tipo like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rst = stm.executeQuery();

            while (rst.next()) {

                try {

                    Usuario user = new Usuario();
                    user.setCodigousuario(rst.getInt("codigo"));
                    user.setCpfCnpj(rst.getString("cpf"));
                    user.setNome(rst.getString("usuario"));
                    user.setGrupo(rst.getString("tipo"));
                    userlist.add(user);

                } catch (SQLException e) {
                    rst.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
           
              JOptionPane.showMessageDialog(null, ex);
        }

        return userlist;
    }

    public ArrayList<Usuario> preencheTabela() {

        ArrayList<Usuario> userlist = new ArrayList();
        ResultSet rst;

        try {
            sql = "select * from usuario where usuarioStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rst = stm.executeQuery();

            while (rst.next()) {

                try {

                    Usuario user = new Usuario();

                    user.setCodigousuario(rst.getInt("codigo"));
                    user.setCpfCnpj(rst.getString("cpf"));
                    user.setNome(rst.getString("usuario"));
                    user.setGrupo(rst.getString("tipo"));
                    userlist.add(user);

                } catch (SQLException e) {
                    rst.close();
                }
            }
           
            stm.close();
             con.close();
        } catch (SQLException ex) {
           
            JOptionPane.showMessageDialog(null, ex);
        }

        return userlist;
    }

    public ArrayList<Usuario> preencheTabelaDesativados() {

        ArrayList<Usuario> userlist = new ArrayList();
        ResultSet rst;

        try {
            sql = "select * from usuario where usuarioStatus = 'desativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rst = stm.executeQuery();

            while (rst.next()) {

                try {

                    Usuario user = new Usuario();

                    user.setCodigousuario(rst.getInt("codigo"));
                    user.setCpfCnpj(rst.getString("cpf"));
                    user.setNome(rst.getString("usuario"));
                    user.setGrupo(rst.getString("tipo"));
                    userlist.add(user);

                } catch (SQLException e) {
                    rst.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null, ex);
        }

        return userlist;
    }

    public ArrayList<Usuario> preencheTabelaPesquisaAtivados(String pesquisa) {

        ArrayList<Usuario> userlist = new ArrayList();
        ResultSet rst;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {
            sql = "select * from (select * from usuario where usuarioStatus = 'ativado') as u where u.usuario like " + pesquisa2 + "or u.tipo like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rst = stm.executeQuery();

            while (rst.next()) {

                try {

                    Usuario user = new Usuario();

                    user.setCodigousuario(rst.getInt("codigo"));
                    user.setCpfCnpj(rst.getString("cpf"));
                    user.setNome(rst.getString("usuario"));
                    user.setGrupo(rst.getString("tipo"));
                    userlist.add(user);

                } catch (SQLException e) {
                    rst.close();
                }
            }
           
            stm.close();
             con.close();
        } catch (SQLException ex) {
           
             JOptionPane.showMessageDialog(null, ex);
        }

        return userlist;
    }

    public ArrayList<Usuario> preencheTabelaPesquisaDesativados(String pesquisa) {

        ArrayList<Usuario> userlist = new ArrayList();
        ResultSet rst;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {
            sql = "select * from (select * from usuario where usuarioStatus = 'desativado') as u where u.usuario like " + pesquisa2 + "or u.tipo like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rst = stm.executeQuery();

            while (rst.next()) {

                try {

                    Usuario user = new Usuario();

                    user.setCodigousuario(rst.getInt("codigo"));
                    user.setCpfCnpj(rst.getString("cpf"));
                    user.setNome(rst.getString("usuario"));
                    user.setGrupo(rst.getString("tipo"));
                    userlist.add(user);

                } catch (SQLException e) {
                    rst.close();
                }
            }
           
            stm.close();
             con.close();
        } catch (SQLException ex) {
          
            JOptionPane.showMessageDialog(null, ex);
        }

        return userlist;
    }

}
