package controllers;

import bankConnection.Conexao;
import help.ConverteData;
import help.DataSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import javax.swing.JOptionPane;
import models.OrdemServico;
import models.PecaServico;

public class OrdemServicoDAO {

    String sql;
    String sql2;
    String sql3;
    Connection con;
    Connection conEnd;
    PreparedStatement stm, stm1, stm2;
    private OrdemServico ordemServico;
    private final ConverteData conv;
    private final DateTimeFormatter formatador;
    int cod;
    int codOsAlteracao;
    int codigoOs = 0;
    java.sql.Time horarioVenda;
    java.sql.Time horarioPrevisto;
    java.sql.Time horarioSaida;

    Locale localeBR = new Locale("pt", "BR");
    NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);

    public OrdemServicoDAO() {

        sql = "";
        sql2 = "";
        sql3 = "";
        con = null;
        stm = null;
        stm1 = null;
        stm2 = null;
        cod = 0;
        codOsAlteracao = 0;
        conv = new ConverteData();
        formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    }

    public boolean buscaOrdemServico(int cod) {
        boolean flag = false;
        try {
            sql = "select * from os";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }
            con.close();
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public boolean VerificaOrdemServico() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from os ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

    public boolean addOrdemServico(OrdemServico os, ArrayList<PecaServico> listPs) throws Exception {
        ResultSet rs;
        horarioVenda = conv.ConverteLocalTimer(os.getHorarioEntrada());

        horarioPrevisto = conv.ConverteLocalTimer(os.getHorarioPrevisto());

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {
            sql = "insert into os (data_inclusao, previsao_entrega, horarioInclusao, horarioPrevisto, desc_problema, cod_cliente, cod_veiculo, cod_mecanico,status,valor_total,osStatus)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);

            stm.setDate(1, conv.converteLocalDatetoDate(os.getDataEntrada()));
            stm.setDate(2, conv.converteLocalDatetoDate(os.getDataPrevista()));
            stm.setTime(3, horarioVenda);
            stm.setTime(4, horarioPrevisto);
            stm.setString(5, os.getDescProblema());
            stm.setInt(6, os.getCod_cliente());
            stm.setInt(7, os.getCod_veiculo());
            stm.setInt(8, os.getCod_mecanico());
            stm.setString(9, os.getStatus());
            stm.setDouble(10, os.getValor_total());
            stm.setString(11, "ativado");
            stm.execute();
            stm.close();
            con.close();

            flag = addPecaServicoNaOs(os.getPecaServicos());

        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSÍVEL EFETUAR O CADASTRO NA BASE DE DADOS");

        }
        return flag;

    }

    public int buscaUltimoCodigoOs() {

        int ultimoCodigo = 0;

        sql = "select codigo from os order by codigo desc limit 1";

        try {
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs;
            rs = stm.executeQuery();

            if (rs.next()) {
                ultimoCodigo = rs.getInt("codigo");
            }
            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return ultimoCodigo;
    }

    public boolean addPecaServicoNaOs(ArrayList<PecaServico> listPs) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        codigoOs = buscaUltimoCodigoOs();
        try {
            sql = "insert into osPecasServicos (cod_os, cod_peca_servico,quantidade, valorUn, valortotalPecaServ) values(?,?,?,?,?)";
            con = Conexao.getConexao();

            for (PecaServico p : listPs) {

                stm = con.prepareStatement(sql);
                stm.setInt(1, codigoOs);
                stm.setInt(2, p.getCodigo());
                stm.setInt(3, p.getQuantidade());
                stm.setDouble(4, p.getValor());
                stm.setDouble(5, p.getSubtotal());
                stm.execute();
            }

            stm.close();
            con.close();
            flag = true;
            return flag;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return flag;

    }

    public boolean alterarOrdemServico(OrdemServico os, int codigo) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        horarioPrevisto = conv.ConverteLocalTimer(os.getHorarioPrevisto());
        codOsAlteracao = codigo;

        try {

            sql = "update os set  previsao_entrega = ?,horarioPrevisto = ?, desc_problema = ?, cod_cliente = ?, cod_veiculo=?, cod_mecanico = ?, status = ?, valor_total = ?"
                    + " where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);

            stm.setDate(1, conv.converteLocalDatetoDate(os.getDataPrevista()));

            stm.setTime(2, horarioPrevisto);
            stm.setString(3, os.getDescProblema());
            stm.setInt(4, os.getCod_cliente());
            stm.setInt(5, os.getCod_veiculo());
            stm.setInt(6, os.getCod_mecanico());
            stm.setString(7, os.getStatus());
            stm.setDouble(8, os.getValor_total());

            stm.execute();
            stm.close();
            con.close();
            flag = atualizaPecasOs(os.getPecaServicos(), codigo);
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }
        return flag;
    }

    public boolean alteraStatusAndamentoFinalizado(String str, int codigo, LocalDate date, LocalTime horario) throws Exception {
        boolean flag = false;

        horarioSaida = conv.ConverteLocalTimer(horario);
        try {

            sql = "update os set status = ?, horarioSaida = ?, data_saida = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.setTime(2, horarioSaida);
            stm.setDate(3, conv.converteLocalDatetoDate(date));
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean alteraStatusAndamento(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update os set status = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean excluirOrdemServico(int cod) {

        boolean flag = false;
        try {
            sql = "delete from os where codigo = " + cod + " cascade";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();

            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "VEÍCULO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }
        return flag;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update os set osStatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean atualizaPecasOs(ArrayList<PecaServico> listPs, int codOs) throws Exception {
        boolean flag = false;

        try {
            sql2 = "delete from osPecasServicos where cod_os  =" + codOs;
            con = Conexao.getConexao();
            stm2 = con.prepareStatement(sql2);
            stm2.execute();

            stm2.close();
            con.close();
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, e);
        }

        try {
            sql = "insert into osPecasServicos (cod_os, cod_peca_servico,quantidade, valorUn, valortotalPecaServ) values(?,?,?,?,?)";
            con = Conexao.getConexao();

            for (PecaServico p : listPs) {

                stm = con.prepareStatement(sql);
                stm.setInt(1, codOs);
                stm.setInt(2, p.getCodigo());
                stm.setInt(3, p.getQuantidade());
                stm.setDouble(4, p.getValor());
                stm.setDouble(5, p.getSubtotal());
                stm.execute();
            }

            stm.close();
            con.close();
            flag = true;
            return flag;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return flag;

    }

    public boolean buscaOs(int cod) {
        boolean flag = false;
        try {
            sql = "select * from os";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public OrdemServico consultaOrdemServico(int cod) {

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo  where o.codigo = " + cod;

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("codigo") == cod) {

                    int num = rs.getInt("tipo_pessoa");

                    ordemServico = new OrdemServico();
                    ordemServico.setCodigo(rs.getInt("codigo"));
                    ordemServico.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    ordemServico.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    ordemServico.setDataPrevistaStr(formatador.format(ordemServico.getDataPrevista()));
                    ordemServico.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    ordemServico.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    ordemServico.setDataInclusaoStr(formatador.format(ordemServico.getDataEntrada()));
                    ordemServico.setCod_cliente(rs.getInt("cod_cliente"));
                    ordemServico.setCod_mecanico(rs.getInt("cod_mecanico"));
                    ordemServico.setCod_veiculo(rs.getInt("cod_veiculo"));
                    ordemServico.setDescProblema(rs.getString("desc_problema"));
                    if (num == 1) {
                        ordemServico.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        ordemServico.setNomeCliente(rs.getString("razaosocial"));
                    }
                    ordemServico.setNomeMecanico(rs.getString("usuario"));
                    ordemServico.setPlacaVeiculo(rs.getString("placa"));
                    ordemServico.setNomeModelo(rs.getString("nomeModelo"));
                    ordemServico.setCorVeiculo(rs.getString("cor"));
                    ordemServico.setStatus(rs.getString("status"));
                    ordemServico.setValor_total(rs.getDouble("valor_total"));
                    ordemServico.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    ordemServico.setPecaServicos(preencheTabelaPecas(cod));
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return ordemServico;
    }

    public ArrayList<OrdemServico> preencheTabelaPesquisaDesativados(String pesquisa) {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";

        try {
            sql = "select * from (select * from os where osstatus = 'desativado') as o"
                    + " join veiculos as v on o.cod_veiculo = v.codigo "
                    + " join modveiculo as mo on v.cod_modveic = mo.codigo"
                    + " join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo"
                    + " where v.placa like " + pesquisa2 + " or c.nome like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaPesquisaAtiva(String pesquisa) {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";

        try {
            sql = "select * from (select * from os where osstatus = 'ativado') as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo"
                    + " where v.placa like " + pesquisa2 + "or mo.nomemodelo like" + pesquisa2 + " or c.nome like" + pesquisa2 + "or cast(o.data_inclusao as varchar) like " + pesquisa2 + " or us.usuario like" + pesquisa2 + " order by previsao_entrega asc, horarioPrevisto asc";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaDesativados() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'desativado' ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabela() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' order by previsao_entrega asc, horarioPrevisto asc ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaFinalizadas() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' and status = 'Finalizado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDataSaida(conv.converteDatetoLocalDate(rs.getDate("data_saida")));
                    os.setDataConclusaoStr(formatador.format(os.getDataSaida()));
                    os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                    os.setHorarioSaida(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioSaida")));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<PecaServico> preencheTabelaPecas(int cod) {

        ArrayList<PecaServico> pslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from osPecasServicos where cod_os = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    PecaServico ps = new PecaServico();

                    ps.setCodigo(rs.getInt("cod_peca_servico"));
                    ServicoPecaDAO spDAO = new ServicoPecaDAO();
                    PecaServico p = spDAO.consultaPecaServico(rs.getInt("cod_peca_servico"));

                    ps.setDescricao(p.getDescricao());
                    ps.setQuantidade(rs.getInt("quantidade"));
                    ps.setValor(rs.getDouble("valorUn"));
                    ps.setSubtotal(rs.getDouble("valortotalPecaServ"));
                    ps.setValorStr(dinheiroBR.format(rs.getDouble("valorUn")));
                    ps.setSubtotalStr(dinheiroBR.format(rs.getDouble("valortotalPecaServ")));
                    pslist.add(ps);
                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return pslist;
    }

    public ArrayList<OrdemServico> verificaAtraso() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        DataSistema dt = new DataSistema();

        LocalDate dataAtual;
        LocalDate dtPrevistaBc;
        int hrAtual = dt.horaSis();
        int miAtual = dt.minutoSis();
        LocalTime horarioAtualizado = LocalTime.of(hrAtual, miAtual);
        LocalTime horarioPrevistoBc;

        dataAtual = conv.converterStringtoLocalDate(dt.dataSis());

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    dtPrevistaBc = conv.converteDatetoLocalDate(rs.getDate("previsao_entrega"));
                    horarioPrevistoBc = conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto"));

                    if (dtPrevistaBc.isEqual(dataAtual)) {

                        if (horarioPrevistoBc.isBefore(horarioAtualizado)) {

                            if (!rs.getString("status").equals("Finalizado")) {
                                alteraStatusAndamento("Atrasado", rs.getInt("codigo"));
                            }

                        }

                    } else if (dtPrevistaBc.isBefore(dataAtual)) {

                        if (!rs.getString("status").equals("Finalizado")) {
                            alteraStatusAndamento("Atrasado", rs.getInt("codigo"));
                        }
                    }

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));

                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaMecanicoLogado() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' and  cod_mecanico = " + codigoMec + " order by previsao_entrega asc, horarioPrevisto asc";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");
                    if (!(rs.getString("status").equals("Finalizado"))) {
                        os.setCodigo(rs.getInt("codigo"));
                        os.setPlacaVeiculo(rs.getString("placa"));
                        os.setNomeModelo(rs.getString("nomeModelo"));
                        os.setCorVeiculo(rs.getString("cor"));
                        if (num == 1) {
                            os.setNomeCliente(rs.getString("nome"));
                        } else if (num == 2) {
                            os.setNomeCliente(rs.getString("razaosocial"));

                        }
                        os.setNomeMecanico(rs.getString("usuario"));
                        os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                        os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                        os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                        os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                        os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                        os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                        os.setDescProblema(rs.getString("desc_problema"));
                        os.setStatus(rs.getString("status"));
                        os.setValor_total(rs.getDouble("valor_total"));
                        os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                        oslist.add(os);

                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaDesativadosMecLog() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'desativado' and  cod_mecanico = " + codigoMec + " order by previsao_entrega asc, horarioPrevisto asc";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    os.setCodigo(rs.getInt("codigo"));
                    os.setPlacaVeiculo(rs.getString("placa"));
                    os.setNomeModelo(rs.getString("nomeModelo"));
                    os.setCorVeiculo(rs.getString("cor"));
                    if (num == 1) {
                        os.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        os.setNomeCliente(rs.getString("razaosocial"));

                    }
                    os.setNomeMecanico(rs.getString("usuario"));
                    os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                    os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                    os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                    os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                    os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                    os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                    os.setDescProblema(rs.getString("desc_problema"));
                    os.setStatus(rs.getString("status"));
                    os.setValor_total(rs.getDouble("valor_total"));
                    os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                    oslist.add(os);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaPesquisaDesativadosMecLog(String pesquisa) {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select * from (select * from os where osstatus = 'desativado' and cod_mecanico = " + codigoMec + ") as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo"
                    + " where v.placa like " + pesquisa2 + " or c.nome like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    if (!(rs.getString("status").equals("Finalizado"))) {

                        os.setCodigo(rs.getInt("codigo"));
                        os.setPlacaVeiculo(rs.getString("placa"));
                        os.setNomeModelo(rs.getString("nomeModelo"));
                        os.setCorVeiculo(rs.getString("cor"));
                        if (num == 1) {
                            os.setNomeCliente(rs.getString("nome"));
                        } else if (num == 2) {
                            os.setNomeCliente(rs.getString("razaosocial"));

                        }
                        os.setNomeMecanico(rs.getString("usuario"));
                        os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                        os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                        os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                        os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                        os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                        os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                        os.setDescProblema(rs.getString("desc_problema"));
                        os.setStatus(rs.getString("status"));
                        os.setValor_total(rs.getDouble("valor_total"));
                        os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                        oslist.add(os);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaPesquisaAtivaMecLog(String pesquisa) {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select * from (select * from os where osstatus = 'desativado' and cod_mecanico = " + codigoMec + ") as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo"
                    + " where v.placa like " + pesquisa2 + " or c.nome like" + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    if (!(rs.getString("status").equals("Finalizado"))) {

                        os.setCodigo(rs.getInt("codigo"));
                        os.setPlacaVeiculo(rs.getString("placa"));
                        os.setNomeModelo(rs.getString("nomeModelo"));
                        os.setCorVeiculo(rs.getString("cor"));
                        if (num == 1) {
                            os.setNomeCliente(rs.getString("nome"));
                        } else if (num == 2) {
                            os.setNomeCliente(rs.getString("razaosocial"));

                        }
                        os.setNomeMecanico(rs.getString("usuario"));
                        os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                        os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                        os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                        os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                        os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                        os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                        os.setDescProblema(rs.getString("desc_problema"));
                        os.setStatus(rs.getString("status"));
                        os.setValor_total(rs.getDouble("valor_total"));
                        os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                        oslist.add(os);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);

        }

        return oslist;
    }

    public int buscarUltimoLoginMecanicoCodigo() {
        int codigo = 0;
        ResultSet rs;
        try {
            sql = "select * from user_atual where id in (select max (id) from user_atual)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                codigo = rs.getInt("codigo");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return codigo;

    }

    public int totalOs() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsMecLog() {

        int total = 0;
        ResultSet rs;
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select count(codigo) as total from os where od_mecanico = " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsAbertas() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Aberta'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsEmAndamento() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Em andamento' ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsAtrasadas() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Atrasado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsFinalizadas() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Finalizado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsPraHoje() {

        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(previsao_entrega) as total from os where previsao_entrega = CURRENT_DATE and status != 'Finalizado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsEmAndamentoMecLog() {
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Em andamento' and cod_mecanico = " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsAtrasadasMecLog() {
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Atrasado' and cod_mecanico = " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsFinalizadasMecLog() {
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(codigo) as total from os where status = 'Finalizado' and cod_mecanico =  " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsPraHojeMecLog() {
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        int total = 0;
        ResultSet rs;

        try {
            sql = "select count(previsao_entrega) as total from os "
                    + "where (previsao_entrega = CURRENT_DATE and status != 'Finalizado') and cod_mecanico = " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public int totalOsAbertasMegLog() {

        int total = 0;
        ResultSet rs;
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select count(codigo) as total from os where status = 'Aberta' and cod_mecanico = " + codigoMec;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            if (rs.next()) {
                total = rs.getInt("total");
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return total;

    }

    public ArrayList<OrdemServico> preencheTabelaMecanicoLogadoInformativo() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;
        int codigoMec = buscarUltimoLoginMecanicoCodigo();
        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' and  cod_mecanico = " + codigoMec + " order by previsao_entrega asc, horarioPrevisto asc";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");

                    if (!(rs.getString("status").equals("Finalizado"))) {
                        os.setCodigo(rs.getInt("codigo"));
                        os.setPlacaVeiculo(rs.getString("placa"));
                        os.setNomeModelo(rs.getString("nomeModelo"));
                        os.setCorVeiculo(rs.getString("cor"));
                        if (num == 1) {
                            os.setNomeCliente(rs.getString("nome"));
                        } else if (num == 2) {
                            os.setNomeCliente(rs.getString("razaosocial"));

                        }
                        os.setNomeMecanico(rs.getString("usuario"));
                        os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                        os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                        os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                        os.setDataInclusaoStr(formatador.format(os.getDataEntrada()));
                        os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                        os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                        os.setDescProblema(rs.getString("desc_problema"));
                        os.setStatus(rs.getString("status"));
                        os.setValor_total(rs.getDouble("valor_total"));
                        os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                        oslist.add(os);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public ArrayList<OrdemServico> preencheTabelaInformativo() {

        ArrayList<OrdemServico> oslist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from os as o "
                    + "join veiculos as v on o.cod_veiculo = v.codigo "
                    + "join modveiculo as mo on v.cod_modveic = mo.codigo "
                    + "join clientes as c on o.cod_cliente = c.codigo "
                    + "join usuario as us on o.cod_mecanico = us.codigo "
                    + "where osStatus = 'ativado' order by previsao_entrega asc, horarioPrevisto asc ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    OrdemServico os = new OrdemServico();

                    int num = rs.getInt("tipo_pessoa");
                    if (!(rs.getString("status").equals("Finalizado"))) {
                        os.setCodigo(rs.getInt("codigo"));
                        os.setPlacaVeiculo(rs.getString("placa"));
                        os.setNomeModelo(rs.getString("nomeModelo"));
                        os.setCorVeiculo(rs.getString("cor"));
                        if (num == 1) {
                            os.setNomeCliente(rs.getString("nome"));
                        } else if (num == 2) {
                            os.setNomeCliente(rs.getString("razaosocial"));

                        }
                        os.setNomeMecanico(rs.getString("usuario"));
                        os.setDataEntrada(conv.converteDatetoLocalDate(rs.getDate("data_inclusao")));
                        os.setDataPrevista(conv.converteDatetoLocalDate(rs.getDate("previsao_entrega")));
                        os.setDataPrevistaStr(formatador.format(os.getDataPrevista()));
                        os.setHorarioEntrada(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioInclusao")));
                        os.setHorarioPrevisto(conv.ConverteSqlTimerLocalTimer(rs.getTime("horarioPrevisto")));
                        os.setDescProblema(rs.getString("desc_problema"));
                        os.setStatus(rs.getString("status"));
                        os.setValor_total(rs.getDouble("valor_total"));
                        os.setValor_totalStr(dinheiroBR.format(rs.getDouble("valor_total")));
                        oslist.add(os);
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }

        return oslist;
    }

    public boolean VerificaOrdemServicoDataInclusao(String dtInicial, String dtFinal) {
        String dataInicial = "'" + dtInicial + "'";
        String dataFinal = "'" + dtFinal + "'";

        int cont = 0;
        boolean retorna = false;
        try {
            sql = " select os.data_inclusao,os.horarioinclusao,os.previsao_entrega, os.horarioprevisto,c.nome, v.placa, m.nomemodelo, os.data_saida,os.desc_problema,\n"
                    + "os.horariosaida,\n"
                    + "os.status,os.valor_total, u.usuario\n"
                    + "from os\n"
                    + "inner join veiculos v on os.cod_veiculo = v.codigo\n"
                    + "inner join modveiculo m on v.cod_modveic = m.codigo\n"
                    + "inner join clientes c on os.cod_cliente = c.codigo\n"
                    + "inner join usuario u on os.cod_mecanico = u.codigo\n"
                    + "where os.osstatus ='ativado' and (os.data_inclusao between " + dataInicial + " and " + dataFinal + ")";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();

            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

    public boolean VerificaOrdemServicoStatus(String status) {
        String status2 = "'" + status + "'";

        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select os.data_inclusao,os.horarioinclusao,os.previsao_entrega, os.horarioprevisto,c.nome, v.placa, m.nomemodelo, os.data_saida,os.desc_problema,\n"
                    + "os.horariosaida,\n"
                    + "os.status,os.valor_total, u.usuario\n"
                    + "from os\n"
                    + "inner join veiculos v on os.cod_veiculo = v.codigo\n"
                    + "inner join modveiculo m on v.cod_modveic = m.codigo\n"
                    + "inner join clientes c on os.cod_cliente = c.codigo\n"
                    + "inner join usuario u on os.cod_mecanico = u.codigo\n"
                    + "where os.osstatus ='ativado' and os.status =" + status2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

}
