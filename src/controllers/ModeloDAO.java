package controllers;

import help.ConverteData;
import bankConnection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.ModeloVeiculo;
import models.Veiculo;

public class ModeloDAO {

    String sql;
    String sql2;
    String sqlEnd;
    Connection con;
    Connection conEnd;
    PreparedStatement stm;
    private ModeloVeiculo modveic;
    private Veiculo veiculo;
    int cod;
    private final ConverteData conv = new ConverteData();

    public ModeloDAO() {

        sql = "";
        sql2 = "";
        con = null;
        stm = null;
        cod = 0;
    }

    public boolean VerificaModelo() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from modveiculo ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();

            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

    public boolean addModelo(ModeloVeiculo mo) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {
            sql = "insert into modveiculo (nomeModelo, marca,cor, qtdPortas, motor, combustivel,modeloStatus)"
                    + " values (?,?,?,?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, mo.getModelo());
            stm.setString(2, mo.getMarca());
            stm.setString(3, mo.getCor());
            stm.setInt(4, mo.getQtdPortas());
            stm.setString(5, mo.getMotor());
            stm.setString(6, mo.getCombustivel());
            stm.setString(7, "ativado");

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "MODELO OU MARCA JÁ ESTÁ CADASTRADO");
        }
        return flag;

    }

    public boolean alterarModVeiculo(ModeloVeiculo modVeiculo, int cod) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        try {
            sql = "update modveiculo set nomeModelo = ?, marca = ?,cor = ?, qtdPortas = ?,"
                    + "motor = ?, combustivel = ? where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, modVeiculo.getModelo().toUpperCase());
            stm.setString(2, modVeiculo.getMarca().toUpperCase());
            stm.setString(3, modVeiculo.getCor().toUpperCase());
            stm.setInt(4, modVeiculo.getQtdPortas());
            stm.setString(5, modVeiculo.getMotor().toUpperCase());
            stm.setString(6, modVeiculo.getCombustivel().toUpperCase());
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;

        }
        return flag;
    }

    public boolean excluirModeloVeic(int cod) {

        boolean flag = false;
        try {
            sql = "delete from modveiculo where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();

            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "MODELO DE VEICULO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }
        return flag;
    }

    public ArrayList<ModeloVeiculo> preencheTabela() {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from modveiculo where modeloStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();

                    m.setCodigo(rs.getInt("codigo"));
                    m.setModelo(rs.getString("nomeModelo"));
                    m.setMarca(rs.getString("marca"));
                    m.setCor(rs.getString("cor"));
                    m.setQtdPortas(rs.getInt("qtdPortas"));
                    m.setMotor(rs.getString("motor"));
                    m.setCombustivel(rs.getString("combustivel"));
                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

    public boolean verificaModeloIgual(ModeloVeiculo m) {

        boolean flag = false;
        ResultSet rs;

        try {
            sql = "select * from modveiculo";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    if ((rs.getString("nomeModelo").equals(m.getModelo())) && (rs.getString("marca").equals(m.getMarca()))
                            && (rs.getString("cor").equals(m.getCor())) && (rs.getString("motor").equals(m.getMotor())) && (rs.getInt("qtdPortas") == m.getQtdPortas())
                            && (rs.getString("combustivel").equals(m.getCombustivel()))) {

                        flag = true;
                    }

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return flag;
    }

    public ArrayList<ModeloVeiculo> buscaModeloTabela(String pesquisa) {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from (select * from modveiculo  where modeloStatus = 'ativado') as m where m.nomeModelo like " + pesquisa2 + "or m.marca like " + pesquisa2 + " or m.cor like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();
                    m.setCodigo(rs.getInt("codigo"));
                    m.setModelo(rs.getString("nomeModelo"));
                    m.setMarca(rs.getString("marca"));
                    m.setCor(rs.getString("cor"));
                    m.setQtdPortas(rs.getInt("qtdPortas"));
                    m.setMotor(rs.getString("motor"));
                    m.setCombustivel(rs.getString("combustivel"));

                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

    public boolean buscaModelo(int cod) {
        boolean flag = false;
        try {
            sql = "select * from modveiculo";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public boolean pendenciaModelo(int cod) {
        boolean flag = false;
        int cont = 0;
        try {
            sql = "select * from veiculos where cod_modveic = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();

            if (cont >= 1) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "MODELO DE VEÍCULO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }

        return flag;
    }

    public ModeloVeiculo consultaModeloVeic(int cod) {

        try {
            sql = "select * from modveiculo where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("codigo") == cod) {

                    modveic = new ModeloVeiculo();

                    modveic.setCodigo(rs.getInt("codigo"));
                    modveic.setModelo(rs.getString("nomeModelo"));
                    modveic.setMarca(rs.getString("marca"));
                    modveic.setCombustivel(rs.getString("combustivel"));
                    modveic.setCor(rs.getString("cor"));
                    modveic.setMotor(rs.getString("motor"));
                    modveic.setQtdPortas(rs.getInt("qtdPortas"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return modveic;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update modveiculo set modeloStatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean consultaStatusModVeiculo(int codigo) {
        boolean retorna = false;
        try {
            sql = "select * from modveiculo where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("modeloStatus").equals("ativado")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public ArrayList<ModeloVeiculo> preencheTabelaDesativados() {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;

        try {
            sql = "select * from modveiculo where modeloStatus = 'desativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();

                    m.setCodigo(rs.getInt("codigo"));
                    m.setModelo(rs.getString("nomeModelo"));
                    m.setMarca(rs.getString("marca"));
                    m.setCor(rs.getString("cor"));
                    m.setQtdPortas(rs.getInt("qtdPortas"));
                    m.setMotor(rs.getString("motor"));
                    m.setCombustivel(rs.getString("combustivel"));
                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

    public ArrayList<ModeloVeiculo> buscaModeloTabelaDesativados(String pesquisa) {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;
        String pesquisa2 = "'%" + pesquisa + "%'";
        try {

            sql = "select * from (select * from modveiculo  where modeloStatus = 'desativado') as m where m.nomeModelo like " + pesquisa2 + "or m.marca like " + pesquisa2 + " or m.cor like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();
                    m.setCodigo(rs.getInt("codigo"));
                    m.setModelo(rs.getString("nomeModelo"));
                    m.setMarca(rs.getString("marca"));
                    m.setCor(rs.getString("cor"));
                    m.setQtdPortas(rs.getInt("qtdPortas"));
                    m.setMotor(rs.getString("motor"));
                    m.setCombustivel(rs.getString("combustivel"));

                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

    public ModeloVeiculo consultaModVeicMarca(String marca) {

        try {
            sql2 = "select * from modveiculo";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql2);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getString("marca").equals(marca.toUpperCase())) {
                    modveic = new ModeloVeiculo();

                    modveic.setCodigo(rs.getInt("codigo"));
                    modveic.setModelo(rs.getString("nomeModelo"));
                    modveic.setMarca(rs.getString("marca"));
                    modveic.setCombustivel(rs.getString("combustivel"));
                    modveic.setCor(rs.getString("cor"));
                    modveic.setMotor(rs.getString("motor"));
                    modveic.setQtdPortas(rs.getInt("qtdPortas"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return modveic;
    }

    public boolean buscaModVeicMarca(String marca) {
        boolean flag = false;
        try {
            sql = "select * from modveiculo";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getString("marca").equals(marca.toUpperCase())) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public ModeloVeiculo consultaModVeicModelo(String marca) {

        try {
            sql2 = "select * from modveiculo";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql2);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getString("nomeModelo").equals(marca.toUpperCase())) {
                    modveic = new ModeloVeiculo();

                    modveic.setCodigo(rs.getInt("codigo"));
                    modveic.setModelo(rs.getString("nomeModelo"));
                    modveic.setMarca(rs.getString("marca"));
                    modveic.setCombustivel(rs.getString("combustivel"));
                    modveic.setCor(rs.getString("cor"));
                    modveic.setMotor(rs.getString("motor"));
                    modveic.setQtdPortas(rs.getInt("qtdPortas"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return modveic;
    }

    public boolean buscaModVeicModelo(String marca) {
        boolean flag = false;
        try {
            sql = "select * from modveiculo";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getString("nomeModelo").equals(marca.toUpperCase())) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public ArrayList<ModeloVeiculo> preencheTabelaMarca() {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;

        try {
            sql = "SELECT DISTINCT marca FROM modveiculo where modeloStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();

                    m.setMarca(rs.getString("marca"));

                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

    public ArrayList<ModeloVeiculo> preencheTabelaModelo() {

        ArrayList<ModeloVeiculo> modveiclist = new ArrayList();
        ResultSet rs;

        try {
            sql = "SELECT DISTINCT nomeModelo FROM modveiculo where modeloStatus = 'ativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    ModeloVeiculo m = new ModeloVeiculo();

                    m.setModelo(rs.getString("nomeModelo"));

                    modveiclist.add(m);

                } catch (SQLException e) {
                    rs.close();
                }
            }
            
            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return modveiclist;
    }

}
