package controllers;

import bankConnection.Conexao;
import help.ConverteData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.ModeloVeiculo;

import models.Veiculo;

public class VeiculoDAO {

    String sql;
    String sql2;
    String sqlEnd;
    Connection con;
    Connection conEnd;
    PreparedStatement stm;
    PreparedStatement stmEnd;
    private Veiculo veiculo;
    private final ModeloVeiculo modVeiculo;
    int cod;

    public VeiculoDAO() {

        sql = "";
        sql2 = "";
        stm = null;
        cod = 0;
        veiculo = new Veiculo();
        modVeiculo = new ModeloVeiculo();
    }

    public boolean VerificaVeiculo() {
        int cont = 0;
        boolean retorna = false;
        try {
            sql = "select * from veiculos ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                retorna = false;
            } else {
                retorna = true;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return retorna;

    }

    public boolean addVeiculo(Veiculo ve) throws Exception {

        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;
        try {
            sql = "insert into veiculos (placa, chassi, ano, km, cod_cliente, cod_modveic, veiculostatus)"
                    + " values (?,?,?,?,?,?,?)";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, ve.getPlaca().toUpperCase());
            stm.setString(2, ve.getChassi());
            stm.setInt(3, ve.getAno());
            stm.setInt(4, ve.getKm());
            stm.setInt(5, ve.getCod_cliente());
            stm.setInt(6, ve.getCod_modveic());
            stm.setString(7, "ativado");

            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, "A PLACA OU CHASSI JÁ ESTÁ CADASTRADO");
        }
        return flag;

    }

    public Veiculo consultaVeiculo(int cod) {

        try {
            sql2 = "select * from veiculos as v join clientes as c on v.cod_cliente = c.codigo join modveiculo as m on v.cod_modveic = m.codigo where v.codigo = " + cod;

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql2);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("codigo") == cod) {
                    int num = rs.getInt("tipo_pessoa");
                    veiculo = new Veiculo();

                    veiculo.setCodigo(rs.getInt("codigo"));
                    veiculo.setCod_modveic(rs.getInt("cod_modveic"));
                    veiculo.setCod_cliente(rs.getInt("cod_cliente"));

                    if (num == 1) {
                        veiculo.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        veiculo.setNomeCliente(rs.getString("razaosocial"));
                    }

                    veiculo.setCpfCnpjCliente(rs.getString("cpf_cnpj"));
                    veiculo.setTelefoneCliente(rs.getString("telefone"));
                    veiculo.setPlaca(rs.getString("placa"));
                    veiculo.setAno(rs.getInt("ano"));
                    veiculo.setKm(rs.getInt("km"));
                    veiculo.setChassi(rs.getString("chassi"));

                    modVeiculo.setModelo(rs.getString("nomeModelo"));
                    modVeiculo.setMarca(rs.getString("marca"));
                    modVeiculo.setCombustivel(rs.getString("combustivel"));
                    modVeiculo.setCor(rs.getString("cor"));
                    modVeiculo.setMotor(rs.getString("motor"));
                    modVeiculo.setQtdPortas(rs.getInt("qtdPortas"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return veiculo;
    }

    public Veiculo consultaVeiculoPlaca(String placa) {

        try {
            sql2 = "select * from veiculos as v join clientes as c on v.cod_cliente = c.codigo join modveiculo as m on v.cod_modveic = m.codigo  ";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql2);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getString("placa").equals(placa.toUpperCase())) {
                    int num = rs.getInt("tipo_pessoa");
                    veiculo = new Veiculo();

                    veiculo.setCodigo(rs.getInt("codigo"));
                    veiculo.setCod_modveic(rs.getInt("cod_modveic"));
                    veiculo.setCod_cliente(rs.getInt("cod_cliente"));

                    if (num == 1) {
                        veiculo.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        veiculo.setNomeCliente(rs.getString("razaosocial"));
                    }

                    veiculo.setCpfCnpjCliente(rs.getString("cpf_cnpj"));
                    veiculo.setTelefoneCliente(rs.getString("telefone"));
                    veiculo.setPlaca(rs.getString("placa"));
                    veiculo.setAno(rs.getInt("ano"));
                    veiculo.setKm(rs.getInt("km"));
                    veiculo.setChassi(rs.getString("chassi"));

                    veiculo.setNomeModelo(rs.getString("nomeModelo"));
                    veiculo.setMarcaModelo(rs.getString("marca"));
                    veiculo.setCorModelo(rs.getString("cor"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return veiculo;
    }

    public ModeloVeiculo recebeConsultaVeiculo() {

        ModeloVeiculo mVeiculo = modVeiculo;

        return mVeiculo;

    }

    public boolean alterarVeiculo(Veiculo ve, int cod) throws Exception {
        @SuppressWarnings("UnusedAssignment")
        boolean flag = false;

        try {

            sql = "update veiculos set placa = ?, chassi = ?,ano = ?, km = ?, cod_cliente = ?, cod_modveic=? where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, ve.getPlaca());
            stm.setString(2, ve.getChassi());
            stm.setInt(3, ve.getAno());
            stm.setInt(4, ve.getKm());
            stm.setInt(5, ve.getCod_cliente());
            stm.setInt(6, ve.getCod_modveic());
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;

        }
        return flag;
    }

    public boolean excluirVeiculo(int cod) {

        boolean flag = false;
        try {
            sql = "delete from veiculos where codigo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.execute();

            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "VEÍCULO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }
        return flag;
    }

    public ArrayList<Veiculo> buscaVeiculoTabela(String pesquisa) {

        ArrayList<Veiculo> veiculosList = new ArrayList();
        ResultSet rs;

        String pesquisa2 = "'%" + pesquisa + "%'";

        try {

            sql = "select * from (select * from veiculos where veiculostatus = 'ativado') as v "
                    + "join clientes as c on v.cod_cliente = c.codigo "
                    + "join modveiculo as m on v.cod_modveic = m.codigo "
                    + "where v.placa like " + pesquisa2 + " or c.nome like " + pesquisa2 + " or c.razaosocial like " + pesquisa2 + " or m.nomeModelo like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    Veiculo v = new Veiculo();
                    int num = rs.getInt("tipo_pessoa");

                    v.setCodigo(rs.getInt("codigo"));
                    v.setPlaca(rs.getString("placa"));
                    v.setChassi(rs.getString("chassi"));
                    v.setAno(rs.getInt("ano"));
                    v.setKm(rs.getInt("km"));
                    v.setCod_cliente(rs.getInt("cod_cliente"));
                    v.setCod_modveic(rs.getInt("cod_modveic"));
                    v.setNomeModelo(rs.getString("nomemodelo"));
                    if (num == 1) {
                        v.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        v.setNomeCliente(rs.getString("razaosocial"));

                    }

                    v.setCpfCnpjCliente(rs.getString("cpf_cnpj"));

                    veiculosList.add(v);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return veiculosList;
    }

    public boolean buscaVeiculo(int cod) {
        boolean flag = false;
        try {
            sql = "select * from veiculos";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("codigo") == cod) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public boolean buscaVeiculoAno(int ano) {
        boolean flag = false;
        try {
            sql = "select * from veiculos";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getInt("ano") == ano) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public boolean buscaVeiculoPlaca(String placa) {
        boolean flag = false;
        try {
            sql = "select * from veiculos";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getString("placa").equals(placa.toUpperCase())) {
                    flag = true;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return flag;

    }

    public boolean pendenciaVeiculo(int cod) {
        boolean flag = false;
        int cont = 0;
        try {
            sql = "select * from os where cod_veiculo = " + cod;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                cont++;
            }

            stm.close();
            con.close();
            if (cont >= 1) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "VEÍCULO NÃO FOI ENCONTRADO NA BASE DE DADOS");
        }

        return flag;
    }

    public ArrayList<Veiculo> preencheTabelaVeiculo() {

        ArrayList<Veiculo> veiculosList = new ArrayList();
        ResultSet rs;

        try {

            sql = "select * from veiculos as v "
                    + "join clientes as c on v.cod_cliente = c.codigo "
                    + "join modveiculo as m on v.cod_modveic = m.codigo "
                    + "where veiculostatus = 'ativado' ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    Veiculo v = new Veiculo();
                    int num = rs.getInt("tipo_pessoa");
                    v.setCodigo(rs.getInt("codigo"));
                    v.setPlaca(rs.getString("placa"));
                    v.setChassi(rs.getString("chassi"));
                    v.setAno(rs.getInt("ano"));
                    v.setKm(rs.getInt("km"));
                    v.setCod_cliente(rs.getInt("cod_cliente"));
                    v.setCod_modveic(rs.getInt("cod_modveic"));
                    v.setNomeModelo(rs.getString("nomemodelo"));
                    if (num == 1) {
                        v.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        v.setNomeCliente(rs.getString("razaosocial"));

                    }

                    v.setCpfCnpjCliente(rs.getString("cpf_cnpj"));

                    veiculosList.add(v);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return veiculosList;
    }

    public ArrayList<Veiculo> preencheTabelaVeiculoDesativados() {

        ArrayList<Veiculo> veiculosList = new ArrayList();
        ResultSet rs;

        try {

            sql = "select * from veiculos as v join clientes as c on v.cod_cliente = c.codigo join modveiculo as m on v.cod_modveic = m.codigo where veiculostatus = 'desativado'";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    Veiculo v = new Veiculo();
                    int num = rs.getInt("tipo_pessoa");
                    v.setCodigo(rs.getInt("codigo"));
                    v.setPlaca(rs.getString("placa"));
                    v.setChassi(rs.getString("chassi"));
                    v.setAno(rs.getInt("ano"));
                    v.setKm(rs.getInt("km"));
                    v.setCod_cliente(rs.getInt("cod_cliente"));
                    v.setCod_modveic(rs.getInt("cod_modveic"));
                    v.setNomeModelo(rs.getString("nomemodelo"));
                    if (num == 1) {
                        v.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        v.setNomeCliente(rs.getString("razaosocial"));

                    }

                    v.setCpfCnpjCliente(rs.getString("cpf_cnpj"));

                    veiculosList.add(v);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return veiculosList;
    }

    public ArrayList<Veiculo> buscaVeiculoTabelaDesativados(String pesquisa) {

        ArrayList<Veiculo> veiculosList = new ArrayList();
        ResultSet rs;

        String pesquisa2 = "'%" + pesquisa + "%'";

        try {

            sql = "select * from (select * from veiculos where veiculostatus = 'desativado') as v join clientes as c on v.cod_cliente = c.codigo join modveiculo as m on v.cod_modveic = m.codigo where v.placa like " + pesquisa2 + " or c.nome like " + pesquisa2 + " or c.razaosocial like " + pesquisa2 + " or m.nomeModelo like " + pesquisa2;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    Veiculo v = new Veiculo();
                    int num = rs.getInt("tipo_pessoa");

                    v.setCodigo(rs.getInt("codigo"));
                    v.setPlaca(rs.getString("placa"));
                    v.setChassi(rs.getString("chassi"));
                    v.setAno(rs.getInt("ano"));
                    v.setKm(rs.getInt("km"));
                    v.setCod_cliente(rs.getInt("cod_cliente"));
                    v.setCod_modveic(rs.getInt("cod_modveic"));
                    v.setNomeModelo(rs.getString("nomemodelo"));
                    if (num == 1) {
                        v.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        v.setNomeCliente(rs.getString("razaosocial"));

                    }

                    v.setCpfCnpjCliente(rs.getString("cpf_cnpj"));

                    veiculosList.add(v);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }

        return veiculosList;
    }

    public boolean AtivaDesativa(String str, int codigo) {
        boolean flag = false;
        try {

            sql = "update veiculos set veiculostatus = ? where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            stm.setString(1, str);
            stm.execute();
            stm.close();
            con.close();
            flag = true;
        } catch (SQLException e) {
            flag = false;
            JOptionPane.showMessageDialog(null, e);
        }

        return flag;
    }

    public boolean consultaStatusVeiculo(int codigo) {
        boolean retorna = false;
        try {
            sql = "select * from veiculos where codigo = " + codigo;
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            ResultSet rst = stm.executeQuery();
            while (rst.next()) {
                if (rst.getString("veiculostatus").equals("ativado")) {
                    retorna = true;
                } else {
                    retorna = false;
                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            return retorna;
        }
    }

    public Veiculo consultaVeiculoAno(int ano) {

        try {
            sql2 = "select * from veiculos as v "
                    + "join clientes as c on v.cod_cliente = c.codigo "
                    + "join modveiculo as m on v.cod_modveic = m.codigo  ";

            con = Conexao.getConexao();
            stm = con.prepareStatement(sql2);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {

                if (rs.getInt("ano") == ano) {
                    int num = rs.getInt("tipo_pessoa");
                    veiculo = new Veiculo();

                    veiculo.setCodigo(rs.getInt("codigo"));
                    veiculo.setCod_modveic(rs.getInt("cod_modveic"));
                    veiculo.setCod_cliente(rs.getInt("cod_cliente"));

                    if (num == 1) {
                        veiculo.setNomeCliente(rs.getString("nome"));
                    } else if (num == 2) {
                        veiculo.setNomeCliente(rs.getString("razaosocial"));
                    }

                    veiculo.setCpfCnpjCliente(rs.getString("cpf_cnpj"));
                    veiculo.setTelefoneCliente(rs.getString("telefone"));
                    veiculo.setPlaca(rs.getString("placa"));
                    veiculo.setAno(rs.getInt("ano"));
                    veiculo.setKm(rs.getInt("km"));
                    veiculo.setChassi(rs.getString("chassi"));

                    modVeiculo.setModelo(rs.getString("nomeModelo"));
                    modVeiculo.setMarca(rs.getString("marca"));
                    modVeiculo.setCombustivel(rs.getString("combustivel"));
                    modVeiculo.setCor(rs.getString("cor"));
                    modVeiculo.setMotor(rs.getString("motor"));
                    modVeiculo.setQtdPortas(rs.getInt("qtdPortas"));

                }
            }

            stm.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return veiculo;
    }

    public ArrayList<Veiculo> preencheTabelaVeiculoAno() {

        ArrayList<Veiculo> veiculosList = new ArrayList();
        ResultSet rs;

        try {

            sql = "SELECT DISTINCT ano from veiculos as v "
                    + "join clientes as c on v.cod_cliente = c.codigo "
                    + "join modveiculo as m on v.cod_modveic = m.codigo "
                    + "where veiculostatus = 'ativado' ";
            con = Conexao.getConexao();
            stm = con.prepareStatement(sql);
            rs = stm.executeQuery();

            while (rs.next()) {

                try {

                    Veiculo v = new Veiculo();

                    v.setAno(rs.getInt("ano"));

                    veiculosList.add(v);

                } catch (SQLException e) {
                    rs.close();
                }
            }

            stm.close();
            con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NÃO FOI POSSIVEL CARREGAR OS DADOS DA TABELA");
        }
        return veiculosList;
    }

}
