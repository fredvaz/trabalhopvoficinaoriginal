package filters;

import controllers.OrdemServicoDAO;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.OrdemServico;

public class TabelaBuscaNumeroOs extends javax.swing.JDialog {
    
    private final OrdemServicoDAO osDAO;
    int codModVeic;
    String opItem;
    
    public TabelaBuscaNumeroOs(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ((DefaultTableCellRenderer) tabelaNumOs.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        osDAO = new OrdemServicoDAO();
        preencheTabela();
        txtPesquisa.setVisible(false);
        btnRestaurar.setVisible(false);
        if (!osDAO.VerificaOrdemServico()) {
            tabelaNumOs.requestFocus();
            tabelaNumOs.changeSelection(0, 0, false, false);
        } else {
            
            btnPesquisar.setEnabled(false);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaNumOs = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        txtPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnRestaurar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Numero O.S.");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tabelaNumOs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº O.S.", "Cliente", "Inclusão"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaNumOs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaNumOsKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaNumOs);
        if (tabelaNumOs.getColumnModel().getColumnCount() > 0) {
            tabelaNumOs.getColumnModel().getColumn(0).setPreferredWidth(4);
            tabelaNumOs.getColumnModel().getColumn(1).setPreferredWidth(150);
            tabelaNumOs.getColumnModel().getColumn(2).setPreferredWidth(10);
        }

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        btnPesquisar.setBackground(new java.awt.Color(204, 204, 204));
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Find_16x16.png"))); // NOI18N
        btnPesquisar.setMnemonic('P');
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnRestaurar.setBackground(new java.awt.Color(204, 204, 204));
        btnRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Redo_32x32.png"))); // NOI18N
        btnRestaurar.setMnemonic('R');
        btnRestaurar.setText("Restaurar");
        btnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRestaurar))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesquisar)
                    .addComponent(btnRestaurar))
                .addGap(10, 10, 10)
                .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        txtPesquisa.setVisible(true);
        txtPesquisa.setEnabled(true);
        txtPesquisa.requestFocus();
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarActionPerformed
        txtPesquisa.setVisible(false);
        txtPesquisa.setEnabled(false);
        btnRestaurar.setVisible(false);
        btnRestaurar.setEnabled(false);
        txtPesquisa.requestFocus();
        preencheTabela();
    }//GEN-LAST:event_btnRestaurarActionPerformed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        btnRestaurar.setVisible(true);
        btnRestaurar.setEnabled(true);
        
        preencheTabelaPesquisaAtivados();

    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void tabelaNumOsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaNumOsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            int row = tabelaNumOs.getSelectedRow();
            String valor = tabelaNumOs.getValueAt(row, 0).toString();
            
            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }
            
        }
    }//GEN-LAST:event_tabelaNumOsKeyPressed
    
    public String itemTabela() {
        
        int row = tabelaNumOs.getSelectedRow();
        String valor = tabelaNumOs.getValueAt(row, 0).toString();
        
        return valor;
    }
    
    public void SelecionaPrimeiraLinha() {
        tabelaNumOs.clearSelection();
        tabelaNumOs.changeSelection(0, 0, false, false);
        tabelaNumOs.requestFocus();
    }
    
    public void preencheTabela() {
        
        DefaultTableModel modeloTab = (DefaultTableModel) tabelaNumOs.getModel();
        modeloTab.setNumRows(0);
        
        for (OrdemServico os : osDAO.preencheTabela()) {
            
            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getNomeCliente(),
                os.getDataInclusaoStr()
            
            });
            
        }
        
    }
    
    public void preencheTabelaPesquisaAtivados() {
        
        DefaultTableModel modeloTab = (DefaultTableModel) tabelaNumOs.getModel();
        modeloTab.setNumRows(0);
        
        for (OrdemServico os : osDAO.preencheTabelaPesquisaAtiva(txtPesquisa.getText().toUpperCase())) {
            
            modeloTab.addRow(new Object[]{
                os.getCodigo(),
                os.getNomeCliente(),
                 os.getDataInclusaoStr()
            
            });
            
        }
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnRestaurar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaNumOs;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}
