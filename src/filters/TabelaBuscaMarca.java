package filters;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.ModeloVeiculo;
import controllers.ModeloDAO;
import java.awt.event.KeyEvent;

public final class TabelaBuscaMarca extends javax.swing.JDialog {

    private final ModeloDAO modDao;
    int codModVeic;
    String opItem;

    public TabelaBuscaMarca(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ((DefaultTableCellRenderer) tabelaMarcas.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        modDao = new ModeloDAO();
        preencheTabela();
        if (!modDao.VerificaModelo()) {
            tabelaMarcas.requestFocus();
            tabelaMarcas.changeSelection(0, 0, false, false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaMarcas = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Marcas");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tabelaMarcas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Marca"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaMarcas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaMarcasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaMarcas);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tabelaMarcasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaMarcasKeyPressed
           if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int row = tabelaMarcas.getSelectedRow();
            String valor = tabelaMarcas.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tabelaMarcasKeyPressed

    public String itemTabela() {

        int row = tabelaMarcas.getSelectedRow();
        String valor = tabelaMarcas.getValueAt(row, 0).toString();

        return valor;
    }

    public void SelecionaPrimeiraLinha() {
        tabelaMarcas.clearSelection();
        tabelaMarcas.changeSelection(0, 0, false, false);
        tabelaMarcas.requestFocus();
    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaMarcas.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.preencheTabelaMarca()) {

            modeloTab.addRow(new Object[]{
                m.getMarca()
            
            });

        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaMarcas;
    // End of variables declaration//GEN-END:variables

}
