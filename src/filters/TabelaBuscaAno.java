package filters;

import controllers.VeiculoDAO;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.Veiculo;

public class TabelaBuscaAno extends javax.swing.JDialog {

    private final VeiculoDAO vDAO;
    int codVeic;
    String opItem;

    public TabelaBuscaAno(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ((DefaultTableCellRenderer) tabelaAno.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        vDAO = new VeiculoDAO();
        preencheTabelaVeic();
        if (!vDAO.VerificaVeiculo()) {
            tabelaAno.requestFocus();
            tabelaAno.changeSelection(0, 0, false, false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaAno = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ano");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        tabelaAno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ano"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaAno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaAnoKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaAno);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tabelaAnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaAnoKeyPressed
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int row = tabelaAno.getSelectedRow();
            String valor = tabelaAno.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tabelaAnoKeyPressed

    public String itemTabela() {

        int row = tabelaAno.getSelectedRow();
        String valor = tabelaAno.getValueAt(row, 0).toString();

        return valor;
    }

    public void SelecionaPrimeiraLinha() {
        tabelaAno.clearSelection();
        tabelaAno.changeSelection(0, 0, false, false);
        tabelaAno.requestFocus();
    }

    public void preencheTabelaVeic() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaAno.getModel();
        modeloTab.setNumRows(0);

        for (Veiculo v : vDAO.preencheTabelaVeiculoAno()) {

            modeloTab.addRow(new Object[]{
                v.getAno()
            });

        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaAno;
    // End of variables declaration//GEN-END:variables
}
