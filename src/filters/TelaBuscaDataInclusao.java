package filters;

import bankConnection.Conexao;
import controllers.OrdemServicoDAO;
import help.ConverteData;
import help.Mascara;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaBuscaDataInclusao extends javax.swing.JDialog {

    private MaskFormatter dataMask;
    private final Mascara masc = new Mascara();
    private final DateTimeFormatter formatador;
    private final ConverteData conv;
    private final OrdemServicoDAO osDAO;
    String dtinicial;
    String dtFinal;

    public TelaBuscaDataInclusao(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();

        try {

            dataMask = new MaskFormatter("##/##/####");

        } catch (ParseException ex) {
            Logger.getLogger(TelaBuscaDataInclusao.class.getName()).log(Level.SEVERE, null, ex);
        }

        txtDataFinal.setValue(null);
        txtDataFinal.setFormatterFactory(new DefaultFormatterFactory(dataMask));
        txtDataInicial.setValue(null);
        txtDataInicial.setFormatterFactory(new DefaultFormatterFactory(dataMask));
        formatador = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        conv = new ConverteData();
        osDAO = new OrdemServicoDAO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDataFinal = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDataInicial = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Data Inclusão");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Data Inicial");

        txtDataFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDataFinalKeyPressed(evt);
            }
        });

        jLabel2.setText("Data Final");

        txtDataInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDataInicialKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtDataInicial, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                        .addComponent(txtDataFinal, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(jLabel1))
                .addGap(75, 75, 75))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDataInicialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataInicialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtDataInicial.getText().trim().equals("")) {
                JOptionPane.showMessageDialog(null, "INFORME A DATA INICIAL");
                txtDataInicial.requestFocus();
            } else {
                if (conv.isValid(txtDataInicial.getText())) {
                    txtDataFinal.requestFocus();
                } else {

                    JOptionPane.showMessageDialog(null, "DATA INVÁLIDA!");
                    txtDataInicial.setText("");
                    txtDataInicial.requestFocus();
                }

            }

        }
    }//GEN-LAST:event_txtDataInicialKeyPressed

    private void txtDataFinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataFinalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtDataFinal.getText().trim().equals("")) {

                JOptionPane.showMessageDialog(null, "INFORME A DATA FINAL");
                txtDataFinal.requestFocus();
            } else {

                if (conv.isValid(txtDataFinal.getText())) {
                    consultaOs();
                } else {

                    JOptionPane.showMessageDialog(null, "DATA INVÁLIDA!");
                    txtDataFinal.setText("");
                    txtDataFinal.requestFocus();
                }

            }

        }
    }//GEN-LAST:event_txtDataFinalKeyPressed

    public void consultaOs() {

        dtFinal = formatador.format(conv.converterStringtoLocalDate(txtDataFinal.getText()));
        dtinicial = formatador.format(conv.converterStringtoLocalDate(txtDataInicial.getText()));

        if (!osDAO.VerificaOrdemServicoDataInclusao(dtinicial, dtFinal)) {

            if (masc.removeMascaraData(txtDataInicial.getText()).trim().isEmpty()
                    || masc.removeMascaraData(txtDataFinal.getText()).trim().isEmpty()) {
                JOptionPane.showMessageDialog(null, "INFORME A DATA INICIAL E FINAL POR FAVOR");
                txtDataInicial.requestFocus();
            } else {
                Connection conn = Conexao.getConexao();

                InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/osPorDataInclusao.jasper");

                JasperPrint jasperPrint = null;
                try {
                    HashMap parametro = new HashMap<>();
                    try {

                        parametro.put("data", new SimpleDateFormat("dd/MM/yyyy").parse(txtDataInicial.getText()));
                        parametro.put("datafinal", new SimpleDateFormat("dd/MM/yyyy").parse(txtDataFinal.getText()));
                    } catch (ParseException ex) {
                        Logger.getLogger(TelaBuscaDataInclusao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

                } catch (JRException ex) {

                }

                JasperViewer view = new JasperViewer(jasperPrint, false);
                this.dispose();
                view.setVisible(true);

            }
        } else {
            JOptionPane.showMessageDialog(null, "NÃO HÁ REGISTRO DE ORDEM DE SERVIÇO NO PERÍODO INFORMADO!");
            txtDataInicial.setText("");
            txtDataFinal.setText("");
            txtDataInicial.requestFocus();
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JFormattedTextField txtDataFinal;
    private javax.swing.JFormattedTextField txtDataInicial;
    // End of variables declaration//GEN-END:variables
}
