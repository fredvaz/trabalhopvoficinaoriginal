package filters;

import bankConnection.Conexao;
import controllers.OrdemServicoDAO;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaBuscaStituacaoOS extends javax.swing.JDialog {

    String statusInf;
    private final OrdemServicoDAO osDAO;

    public TelaBuscaStituacaoOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();
        osDAO = new OrdemServicoDAO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnRelatório = new javax.swing.JButton();
        jcombSit = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório por Situação da O.S");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnRelatório.setBackground(new java.awt.Color(204, 204, 204));
        btnRelatório.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/iconesMod/Paste_32x32.png"))); // NOI18N
        btnRelatório.setText("Gerar relatório");
        btnRelatório.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatórioActionPerformed(evt);
            }
        });

        jcombSit.setBackground(new java.awt.Color(204, 204, 204));
        jcombSit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Informe a Situação", "Aberta", "Em andamento", "Aguardando Peças", "Atrasado", "Finalizado" }));
        jcombSit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcombSitItemStateChanged(evt);
            }
        });
        jcombSit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jcombSitKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jcombSit, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnRelatório, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRelatório)
                    .addComponent(jcombSit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRelatórioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatórioActionPerformed
        if (jcombSit.getSelectedIndex() != 0) {
            consultaPorStatus();
        } else {
            JOptionPane.showMessageDialog(null, "SELECIONE UMA SITUAÇÃO PRIMEIRO!");
        }
    }//GEN-LAST:event_btnRelatórioActionPerformed

    private void jcombSitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jcombSitKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (jcombSit.getSelectedIndex() != 0) {

                statusInf = jcombSit.getSelectedItem().toString();
            }
        }
    }//GEN-LAST:event_jcombSitKeyPressed

    private void jcombSitItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcombSitItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            if (jcombSit.getSelectedIndex() != 0) {

                statusInf = jcombSit.getSelectedItem().toString();
            }
        }
    }//GEN-LAST:event_jcombSitItemStateChanged

    public void consultaPorStatus() {

        if (!osDAO.VerificaOrdemServicoStatus(statusInf)) {
            Connection conn = Conexao.getConexao();

            InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/osPorStatus.jasper");

            JasperPrint jasperPrint = null;
            try {
                HashMap parametro = new HashMap<>();
                parametro.put("status", statusInf);

                jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

            } catch (JRException ex) {
                System.out.println("Ocorreu um erro: " + ex);
            }

            JasperViewer view = new JasperViewer(jasperPrint, false);
            this.dispose();
            view.setVisible(true);
        } else {

            JOptionPane.showMessageDialog(null, "NÃO HÁ REGISTROS COM A SITUAÇÃO INFORMADA");
            jcombSit.setSelectedIndex(0);
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRelatório;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JComboBox<String> jcombSit;
    // End of variables declaration//GEN-END:variables
}
