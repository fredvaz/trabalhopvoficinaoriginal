package filters;

import controllers.ModeloDAO;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.ModeloVeiculo;

public final class TabelaBuscaModelo extends javax.swing.JDialog {

    private final ModeloDAO modDao;
    int codModVeic;
    String opItem;

    public TabelaBuscaModelo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        ((DefaultTableCellRenderer) tabelaModelo.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        modDao = new ModeloDAO();
        preencheTabela();
        if (!modDao.VerificaModelo()) {
            tabelaModelo.requestFocus();
            tabelaModelo.changeSelection(0, 0, false, false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaModelo = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modelo");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        tabelaModelo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Modelo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaModeloKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaModelo);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tabelaModeloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaModeloKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int row = tabelaModelo.getSelectedRow();
            String valor = tabelaModelo.getValueAt(row, 0).toString();

            opItem = valor;
            if (!valor.equals("")) {
                this.dispose();
            }

        }
    }//GEN-LAST:event_tabelaModeloKeyPressed

    public String itemTabela() {

        int row = tabelaModelo.getSelectedRow();
        String valor = tabelaModelo.getValueAt(row, 0).toString();

        return valor;
    }

    public void SelecionaPrimeiraLinha() {
        tabelaModelo.clearSelection();
        tabelaModelo.changeSelection(0, 0, false, false);
        tabelaModelo.requestFocus();
    }

    public void preencheTabela() {

        DefaultTableModel modeloTab = (DefaultTableModel) tabelaModelo.getModel();
        modeloTab.setNumRows(0);

        for (ModeloVeiculo m : modDao.preencheTabelaModelo()) {

            modeloTab.addRow(new Object[]{
                m.getModelo()

            });

        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaModelo;
    // End of variables declaration//GEN-END:variables
}
