package filters;

import bankConnection.Conexao;
import controllers.VeiculoDAO;
import help.LimitaNroCaracteresNumero;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import models.Veiculo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaBuscaAno extends javax.swing.JDialog {

    private String opcVeic;
    private int codVeiculo;
    private final VeiculoDAO vDAO;

    public TelaBuscaAno(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();
        vDAO = new VeiculoDAO();
        codVeiculo = 0;
        txtAno.setDocument(new LimitaNroCaracteresNumero(40));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtAno = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório Por Ano de Fabricação");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtAno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAnoKeyPressed(evt);
            }
        });

        jLabel1.setText("Informe o ano: ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 30, Short.MAX_VALUE))
                    .addComponent(txtAno))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtAno, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtAnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAnoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (txtAno.getText().trim().equals("")) {

                preencheAno();
            } else {
                retAnoVeic();

            }

            if (!txtAno.getText().trim().equals("")) {

                consultaAno();

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheAno();
        }
    }//GEN-LAST:event_txtAnoKeyPressed

    public void consultaAno() {
        if (txtAno.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "INFORME O ANO DO VEÍCULO");
            txtAno.requestFocus();
        } else {
            Connection conn = Conexao.getConexao();

            InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/veiculosPorAno.jasper");

            JasperPrint jasperPrint = null;
            try {
                HashMap parametro = new HashMap<>();
                int ano = Integer.parseInt(txtAno.getText());
                parametro.put("ano", ano);

                jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

            } catch (JRException ex) {
                System.out.println("Ocorreu um erro: " + ex);
            }

            JasperViewer view = new JasperViewer(jasperPrint, false);
            this.dispose();
            view.setVisible(true);

        }

    }

    public void preencheAno() {
        TabelaBuscaAno tba = new TabelaBuscaAno((Frame) getParent(), true);
        tba.setVisible(true);

        try {
            opcVeic = tba.opItem;

            codVeiculo = Integer.parseInt(opcVeic);
            Veiculo v;
            v = vDAO.consultaVeiculoAno(codVeiculo);

            txtAno.setText(opcVeic);

        } catch (NumberFormatException e) {

        }

    }

    public void retAnoVeic() {
        String ano = txtAno.getText();
        int codigo = Integer.parseInt(ano);
        if (vDAO.buscaVeiculoAno(codigo)) {
            Veiculo v;
            v = vDAO.consultaVeiculoAno(codigo);

            txtAno.setText(ano);

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE VEÍCULO COM O ANO INFORMADO");
            txtAno.setText("");
            txtAno.requestFocus();
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtAno;
    // End of variables declaration//GEN-END:variables
}
