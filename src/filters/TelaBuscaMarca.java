package filters;

import bankConnection.Conexao;
import controllers.ModeloDAO;
import help.LimitaNroCaracteres;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import models.ModeloVeiculo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;


public class TelaBuscaMarca extends javax.swing.JDialog {

    private String opcMod;
    private int codModelo;
    private final ModeloDAO mDAO;

    public TelaBuscaMarca(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();
        mDAO = new ModeloDAO();
        codModelo = 0;
        txtMarca.setDocument(new LimitaNroCaracteres(40));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtMarca = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório Por Marca");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMarcaKeyPressed(evt);
            }
        });

        jLabel1.setText("Informe a marca");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0))
                    .addComponent(txtMarca))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtMarcaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMarcaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (txtMarca.getText().trim().equals("")) {

                preencheModeloVeicMarca();
            } else {
                retModVeicMarca();

            }

            if (!txtMarca.getText().trim().equals("")) {

               consultaMarca();

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheModeloVeicMarca();
        }
    }//GEN-LAST:event_txtMarcaKeyPressed

    public void preencheModeloVeicMarca() {
        TabelaBuscaMarca tmodv = new TabelaBuscaMarca((Frame) getParent(), true);
        tmodv.setVisible(true);

        try {
            opcMod = tmodv.opItem;

            ModeloVeiculo mv;
            mv = mDAO.consultaModVeicMarca(opcMod);
            codModelo = mv.getCodigo();
            if (mDAO.consultaStatusModVeiculo(codModelo)) {

                txtMarca.setText(opcMod);
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UMA MARCA DE VEÍCULO QUE ESTEJA ATIVA");
                txtMarca.setText("");
                txtMarca.requestFocus();
            }

        } catch (NullPointerException e) {

        }

    }

    public void retModVeicMarca() {
        String marca = txtMarca.getText();

        if (mDAO.buscaModVeicMarca(marca)) {
            ModeloVeiculo mv;
            mv = mDAO.consultaModVeicMarca(marca);
            if (mDAO.consultaStatusModVeiculo(mv.getCodigo())) {

                txtMarca.setText(marca);

            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UMA MARCA QUE ESTEJA ATIVA");
                txtMarca.setText("");
                txtMarca.requestFocus();
            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE ESSA MARCA CADASTRADA");
            txtMarca.setText("");
            txtMarca.requestFocus();
        }

    }

    public void consultaMarca() {

        if (txtMarca.getText().trim().isEmpty()) {
             JOptionPane.showMessageDialog(null, "INFORME UMA MARCA POR FAVOR");
              txtMarca.requestFocus();
        } else {
            Connection conn = Conexao.getConexao();

            InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/veiculosPorMarca.jasper");

            JasperPrint jasperPrint = null;
            try {
                HashMap parametro = new HashMap<>();
                parametro.put("marca", txtMarca.getText());

                jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

            } catch (JRException ex) {
                System.out.println("Ocorreu um erro: " + ex);
            }

            JasperViewer view = new JasperViewer(jasperPrint, false);
            this.dispose();
            view.setVisible(true);

        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtMarca;
    // End of variables declaration//GEN-END:variables
}
