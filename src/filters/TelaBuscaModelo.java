package filters;

import bankConnection.Conexao;
import controllers.ModeloDAO;
import help.LimitaNroCaracteres;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import models.ModeloVeiculo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaBuscaModelo extends javax.swing.JDialog {

    private String opcMod;
    private int codModelo;
    private final ModeloDAO mDAO;

    public TelaBuscaModelo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();
        mDAO = new ModeloDAO();
        codModelo = 0;
        txtModelo.setDocument(new LimitaNroCaracteres(40));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtModelo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório Por Modelo");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtModeloKeyPressed(evt);
            }
        });

        jLabel1.setText("Informe o modelo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 34, Short.MAX_VALUE))
                    .addComponent(txtModelo))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtModeloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtModeloKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (txtModelo.getText().trim().equals("")) {

                preencheModeloVeicModelo();
            } else {
                retModVeicModelo();

            }

            if (!txtModelo.getText().trim().equals("")) {

                consultaModelo();

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheModeloVeicModelo();
        }

    }//GEN-LAST:event_txtModeloKeyPressed

    public void preencheModeloVeicModelo() {
        TabelaBuscaModelo tmodv = new TabelaBuscaModelo((Frame) getParent(), true);
        tmodv.setVisible(true);

        try {
            opcMod = tmodv.opItem;

            ModeloVeiculo mv;
            mv = mDAO.consultaModVeicModelo(opcMod);
            codModelo = mv.getCodigo();
            if (mDAO.consultaStatusModVeiculo(codModelo)) {

                txtModelo.setText(opcMod);
            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM MODELO DE VEÍCULO QUE ESTEJA ATIVO");
                txtModelo.setText("");
                txtModelo.requestFocus();
            }

        } catch (NullPointerException e) {

        }

    }

    public void retModVeicModelo() {
        String modelo = txtModelo.getText();

        if (mDAO.buscaModVeicModelo(modelo)) {
            ModeloVeiculo mv;
            mv = mDAO.consultaModVeicModelo(modelo);
            if (mDAO.consultaStatusModVeiculo(mv.getCodigo())) {

                txtModelo.setText(modelo);

            } else {
                JOptionPane.showMessageDialog(null, "ESCOLHA UM MODELO DE VEÍCULO QUE ESTEJA ATIVO");
                txtModelo.setText("");
                txtModelo.requestFocus();
            }

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE ESSE MODELO DE VEÍCULO CADASTRADO");
            txtModelo.setText("");
            txtModelo.requestFocus();
        }

    }

    public void consultaModelo() {

        if (txtModelo.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "INFORME UM MODELO DE VEÍCULO POR FAVOR");
            txtModelo.requestFocus();
        } else {

            Connection conn = Conexao.getConexao();

            InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/veiculosPorModelo.jasper");

            JasperPrint jasperPrint = null;
            try {
                HashMap parametro = new HashMap<>();
                parametro.put("modelo", txtModelo.getText());

                jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

            } catch (JRException ex) {
                System.out.println("Ocorreu um erro: " + ex);
            }

            JasperViewer view = new JasperViewer(jasperPrint, false);
            this.dispose();
            view.setVisible(true);

        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtModelo;
    // End of variables declaration//GEN-END:variables
}
