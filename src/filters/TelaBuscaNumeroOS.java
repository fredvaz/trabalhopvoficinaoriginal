package filters;

import bankConnection.Conexao;
import controllers.OrdemServicoDAO;
import help.LimitaNroCaracteresNumero;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import models.OrdemServico;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaBuscaNumeroOS extends javax.swing.JDialog {

    private String opcOs;
    private int codos;
    private final OrdemServicoDAO osDAO;

    public TelaBuscaNumeroOS(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.setLocation(250, 120);
        initComponents();
        osDAO = new OrdemServicoDAO();
        codos = 0;
        txtNumos.setDocument(new LimitaNroCaracteresNumero(40));
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtNumos = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório por código da O.S");
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtNumos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumosKeyPressed(evt);
            }
        });

        jLabel1.setText("Informe o Nº da O.S");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 34, Short.MAX_VALUE))
                    .addComponent(txtNumos))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtNumos, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (txtNumos.getText().trim().equals("")) {

                preencheNumOs();
            } else {
                retModNumOs();

            }

            if (!txtNumos.getText().trim().equals("")) {

                consultaNumos();

            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F12) {

            preencheNumOs();
        }
    }//GEN-LAST:event_txtNumosKeyPressed
    public void consultaNumos() {

        if (txtNumos.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "INFORME UM Nº DE ORDEM DE SERVIÇO");
            txtNumos.requestFocus();
        } else {

            Connection conn = Conexao.getConexao();

            InputStream src = this.getClass().getClassLoader().getResourceAsStream("relatorios/osPorCodigo.jasper");

            JasperPrint jasperPrint = null;
            try {
                HashMap parametro = new HashMap<>();
                int codigo = Integer.parseInt(txtNumos.getText());
                parametro.put("codigo", codigo);

                jasperPrint = JasperFillManager.fillReport(src, parametro, conn);

            } catch (JRException ex) {
                System.out.println("Ocorreu um erro: " + ex);
            }

            JasperViewer view = new JasperViewer(jasperPrint, false);
            this.dispose();
            view.setVisible(true);

        }

    }

    public void preencheNumOs() {
        TabelaBuscaNumeroOs tbno = new TabelaBuscaNumeroOs((Frame) getParent(), true);
        tbno.setVisible(true);

        try {
            opcOs = tbno.opItem;

            codos = Integer.parseInt(opcOs);
            OrdemServico os;
            os = osDAO.consultaOrdemServico(codos);

            txtNumos.setText(opcOs);

        } catch (NumberFormatException e) {

        }

    }

    public void retModNumOs() {
        String numOs = txtNumos.getText();
        int codigo = Integer.parseInt(numOs);
        if (osDAO.buscaOrdemServico(codigo)) {
            OrdemServico os;
            os = osDAO.consultaOrdemServico(codigo);

            txtNumos.setText(numOs);

        } else {
            JOptionPane.showMessageDialog(null, "NÃO EXISTE ORDEM DE SERVIÇO COM ESTE NÚMERO");
            txtNumos.setText("");
            txtNumos.requestFocus();
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtNumos;
    // End of variables declaration//GEN-END:variables
}
