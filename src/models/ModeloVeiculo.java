package models;

public class ModeloVeiculo {

    private int codigo;
    private String modelo;
    private String marca;
    private int qtdPortas;
    private String motor;
    private String combustivel;
    private String cor;
    private String modeloStatus;

    public ModeloVeiculo(int codigo, String modelo, String marca, int qtdPortas, String motor, String combustivel, String cor,String modeloStatus) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.marca = marca;
        this.qtdPortas = qtdPortas;
        this.motor = motor;
        this.combustivel = combustivel;
        this.cor = cor;
        this.modeloStatus = modeloStatus;
    }

    public ModeloVeiculo() {
       
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getQtdPortas() {
        return qtdPortas;
    }

    public void setQtdPortas(int qtdPortas) {
        this.qtdPortas = qtdPortas;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getModeloStatus() {
        return modeloStatus;
    }

    public void setModeloStatus(String modeloStatus) {
        this.modeloStatus = modeloStatus;
    }
    

}
