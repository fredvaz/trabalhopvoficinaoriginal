
package models;



public class PecaServico {
   
    private int codigo;
    int tipo;
    int quantidade;
    private String descricao;
    private double valor;
    private double subotal;
    private String pecaservicoStatus;
    public String valorStr;
    public String subtotalStr;

    public PecaServico(int codigo, int tipo, int quantidade, String descricao, double valor, double subotal, String pecaservicoStatus,String valorStr, String subtotalStr) {
        this.codigo = codigo;
        this.tipo = tipo;
        this.quantidade = quantidade;
        this.descricao = descricao;
        this.valor = valor;
        this.subotal = subotal;
        this.pecaservicoStatus = pecaservicoStatus;
        this.valorStr = valorStr;
        this.subtotalStr = subtotalStr;
    }

    public String getValorStr() {
        return valorStr;
    }

    public void setValorStr(String valorStr) {
        this.valorStr = valorStr;
    }

    public String getSubtotalStr() {
        return subtotalStr;
    }

    public void setSubtotalStr(String subtotalStr) {
        this.subtotalStr = subtotalStr;
    }

    
    
    
    public double getSubtotal() {
        return subotal;
    }

    public void setSubtotal(double subotal) {
        this.subotal = subotal;
    }

    
    public PecaServico(){
    
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }


    public String getPecaservicoStatus() {
        return pecaservicoStatus;
    }

    public void setPecaservicoStatus(String pecaservicoStatus) {
        this.pecaservicoStatus = pecaservicoStatus;
    }
    
    

    
}
