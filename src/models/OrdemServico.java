package models;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Locale;

public class OrdemServico {

    private int codigo;
    private int cod_cliente;
    private int cod_veiculo;
    private int cod_mecanico;
    private int cod_pecaServico;
    private String status;
    private int qtdPecaServico;
    private double valor_total;
    private String valor_totalStr;
    private String descProblema;
    private String nomeCliente;
    private String nomeMecanico;
    private String placaVeiculo;
    private String nomeModelo;
    private String corVeiculo;
    private String dataInclusaoStr;
    private String dataPrevistaStr;
    private String dataConclusaoStr;
    private LocalDate dataEntrada;
    private LocalDate dataSaida;
    private LocalDate dataPrevista;
    private LocalTime horarioEntrada;
    private LocalTime horarioSaida;
    private LocalTime horarioPrevisto;
    private ArrayList<PecaServico> pecaServicos;
    Locale localeBR = new Locale("pt", "BR");
    NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);

    public OrdemServico(int codigo, int cod_cliente, int cod_veiculo, int cod_mecanico, int cod_pecaServico, String status, int qtdPecaServico, 
           double valor_total,String valor_totalStr, String descProblema, String nomeCliente, String nomeMecanico, String placaVeiculo, String nomeModelo, 
           String corVeiculo, LocalDate dataEntrada, LocalDate dataSaida, LocalDate dataPrevista, LocalTime horarioEntrada, 
           LocalTime horarioSaida, LocalTime horarioPrevisto, ArrayList<PecaServico> pecaServicos, String dataPrevistaStr, 
           String dataConclusaoStr, String dataInclusaoStr) {
        this.codigo = codigo;
        this.cod_cliente = cod_cliente;
        this.cod_veiculo = cod_veiculo;
        this.cod_mecanico = cod_mecanico;
        this.cod_pecaServico = cod_pecaServico;
        this.status = status;
        this.qtdPecaServico = qtdPecaServico;
        this.valor_total = valor_total;
        this.descProblema = descProblema;
        this.nomeCliente = nomeCliente;
        this.nomeMecanico = nomeMecanico;
        this.placaVeiculo = placaVeiculo;
        this.nomeModelo = nomeModelo;
        this.corVeiculo = corVeiculo;
        this.dataEntrada = dataEntrada;
        this.dataSaida = dataSaida;
        this.dataPrevista = dataPrevista;
        this.horarioEntrada = horarioEntrada;
        this.horarioSaida = horarioSaida;
        this.horarioPrevisto = horarioPrevisto;
        this.pecaServicos = pecaServicos;
        this.dataPrevistaStr = dataPrevistaStr;
        this.dataConclusaoStr = dataConclusaoStr;
        this.dataInclusaoStr = dataInclusaoStr;
        this.valor_totalStr = valor_totalStr;
    }

    public LocalTime getHorarioEntrada() {
        return horarioEntrada;
    }

    public void setHorarioEntrada(LocalTime horarioEntrada) {
        this.horarioEntrada = horarioEntrada;
    }

    public LocalTime getHorarioSaida() {
        return horarioSaida;
    }

    public void setHorarioSaida(LocalTime horarioSaida) {
        this.horarioSaida = horarioSaida;
    }

    public LocalTime getHorarioPrevisto() {
        return horarioPrevisto;
    }

    public void setHorarioPrevisto(LocalTime horarioPrevisto) {
        this.horarioPrevisto = horarioPrevisto;
    }

    public ArrayList<PecaServico> getPecaServicos() {
        return pecaServicos;
    }

    public void setPecaServicos(ArrayList<PecaServico> pecaServicos) {
        this.pecaServicos = pecaServicos;
    }

    public OrdemServico() {

    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCod_cliente() {
        return cod_cliente;
    }

    public void setCod_cliente(int cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public int getCod_veiculo() {
        return cod_veiculo;
    }

    public void setCod_veiculo(int cod_veiculo) {
        this.cod_veiculo = cod_veiculo;
    }

    public int getCod_mecanico() {
        return cod_mecanico;
    }

    public void setCod_mecanico(int cod_mecanico) {
        this.cod_mecanico = cod_mecanico;
    }

    public int getCod_pecaServico() {
        return cod_pecaServico;
    }

    public void setCod_pecaServico(int cod_pecaServico) {
        this.cod_pecaServico = cod_pecaServico;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQtdPecaServico() {
        return qtdPecaServico;
    }

    public void setQtdPecaServico(int qtdPecaServico) {
        this.qtdPecaServico = qtdPecaServico;
    }

    public double getValor_total() {
       
        return valor_total;
    }

    public void setValor_total(double valor_total) {
        this.valor_total = valor_total;
    }

    public String getDescProblema() {
        return descProblema;
    }

    public void setDescProblema(String descProblema) {
        this.descProblema = descProblema;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getNomeMecanico() {
        return nomeMecanico;
    }

    public void setNomeMecanico(String nomeMecanico) {
        this.nomeMecanico = nomeMecanico;
    }

    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    public LocalDate getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(LocalDate dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public LocalDate getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(LocalDate dataSaida) {
        this.dataSaida = dataSaida;
    }

    public LocalDate getDataPrevista() {
        return dataPrevista;
    }

    public void setDataPrevista(LocalDate dataPrevista) {
        this.dataPrevista = dataPrevista;
    }

    public String getNomeModelo() {
        return nomeModelo;
    }

    public void setNomeModelo(String nomeModelo) {
        this.nomeModelo = nomeModelo;
    }

    public String getCorVeiculo() {
        return corVeiculo;
    }

    public void setCorVeiculo(String corVeiculo) {
        this.corVeiculo = corVeiculo;
    }

    public String getDataPrevistaStr() {
        return dataPrevistaStr;
    }

    public void setDataPrevistaStr(String dataPrevistaStr) {
        this.dataPrevistaStr = dataPrevistaStr;
    }

    public String getDataConclusaoStr() {
        return dataConclusaoStr;
    }

    public void setDataConclusaoStr(String dataConclusaoStr) {
        this.dataConclusaoStr = dataConclusaoStr;
    }

    public String getDataInclusaoStr() {
        return dataInclusaoStr;
    }

    public void setDataInclusaoStr(String dataInclusaoStr) {
        this.dataInclusaoStr = dataInclusaoStr;
    }

    public String getValor_totalStr() {
        return valor_totalStr;
    }

    public void setValor_totalStr(String valor_totalStr) {
        this.valor_totalStr = valor_totalStr;
    }

    
}
