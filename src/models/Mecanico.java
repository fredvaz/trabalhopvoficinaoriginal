package models;

public class Mecanico extends PessoaFisica {

    private int codigoMecanico;
    private String senha;

    public Mecanico(int codigoMecanico, String senha, String nome) {
        super(nome);

        this.codigoMecanico = codigoMecanico;
        this.senha = senha;
    }
    
    public Mecanico(){
    
    }

    public int getCodigoMecanico() {
        return codigoMecanico;
    }

    public void setCodigoMecanico(int codigoMecanico) {
        this.codigoMecanico = codigoMecanico;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    

}
