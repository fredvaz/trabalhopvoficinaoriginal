package models;

public class Usuario extends PessoaFisica{

    private int codigoUsuario;
   
    private String senha;
    private String grupo;
    private String statusUsuario;
    private boolean alterar;
    private boolean excluir;
    private boolean inserir;

    public Usuario(int codigoUsuario,  String senha, String grupo,String statusUsuario,  boolean alterar, boolean excluir, boolean inserir, String nome) {
         super(nome);
        this.codigoUsuario = codigoUsuario;
       this.statusUsuario = statusUsuario;
        this.senha = senha;
        this.grupo = grupo;
        this.alterar = alterar;
        this.excluir = excluir;
        this.inserir = inserir;

    }

    public Usuario() {

    }

    public String getStatusUsuario() {
        return statusUsuario;
    }

    public void setStatusUsuario(String statusUsuario) {
        this.statusUsuario = statusUsuario;
    }

    
    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public boolean isExcluir() {
        return excluir;
    }

    public void setExcluir(boolean excluir) {
        this.excluir = excluir;
    }

    public boolean isInserir() {
        return inserir;
    }

    public void setInserir(boolean inserir) {
        this.inserir = inserir;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public int getCodigousuario() {
        return (codigoUsuario);
    }

    public void setCodigousuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }
  

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
