package models;

import java.time.LocalDate;

public class Cliente {

    private int codigo;
    private String email;
    private String logradouro;
    private String numero;
    private String bairro;
    private String municipio;
    private String estado;
    private String clienteStatus;
    private String complemento;
    private String telefone;
    private LocalDate dataNasc;
    private int tipoPessoa;

    public Cliente() {

    }

    public Cliente(int codigo,  String email, String logradouro, String numero, String bairro, String municipio,
            String estado, String complemento, String telefone, LocalDate dataNasc, int tipoPessoa,String clienteStatus) {

        this.codigo = codigo;
        this.email = email;
        this.logradouro = logradouro;
        this.numero = numero;
        this.bairro = bairro;
        this.municipio = municipio;
        this.estado = estado;
        this.complemento = complemento;
        this.telefone = telefone;
        this.dataNasc = dataNasc;
        this.tipoPessoa = tipoPessoa;
        this.clienteStatus = clienteStatus;

    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getClienteStatus() {
        return clienteStatus;
    }

    public void setClienteStatus(String clienteStatus) {
        this.clienteStatus = clienteStatus;
    }

   

    public String getLogradouro() {
        return logradouro;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public LocalDate getDataNascimento() {
        return dataNasc;
    }

    public void setDataNascimento(LocalDate dataNasc) {
        this.dataNasc = dataNasc;
    }

    public int getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(int tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }
    
    

}
