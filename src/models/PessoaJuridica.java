package models;

import java.time.LocalDate;

public class PessoaJuridica extends Cliente {

    private String razaoSocial;
    private String nomeFantasia;
    private String cpf_cnpj;
    private String rg_ie;

    public PessoaJuridica(String razaoSocial, String nomeFantasia, String cpf_cnpj, String rg_ie, int codigo, String email, String logradouro, String numero, String bairro, String municipio,
            String estado, String complemento, String telefone, LocalDate dataNasc, int tipoPessoa, String clienteStatus) {

        super(codigo, email, logradouro, numero, bairro, municipio, estado, complemento, telefone, dataNasc, tipoPessoa,clienteStatus);

        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.cpf_cnpj = cpf_cnpj;
        this.rg_ie = rg_ie;
    }

    public PessoaJuridica() {

    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCpfCnpj() {
        return cpf_cnpj;
    }

    public void setCpfCnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getRgIe() {
        return rg_ie;
    }

    public void setRgIe(String rg_ie) {
        this.rg_ie = rg_ie;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

}
