package models;

public class Veiculo {

    private int codigo;
    private String placa;
    private String chassi;
    private int ano;
    private int km;
    private int cod_cliente;
    private int cod_modveic;
    private String nomeCliente;
    private String cpfCnpjCliente;
    private String telefoneCliente;
    private String nomeModelo;
    private String marcaModelo;
    private String corModelo;
    private String veiculoStatus;

    public Veiculo(int codigo, String placa, String chassi, int ano, int km, int cod_cliente, int cod_modveic, String nomeCliente, String cpfCnpjCliente, String telefoneCliente, String nomeModelo, String marcaModelo, String corModelo,String veiculoStatus) {
        this.codigo = codigo;
        this.placa = placa;
        this.chassi = chassi;
        this.ano = ano;
        this.km = km;
        this.cod_cliente = cod_cliente;
        this.cod_modveic = cod_modveic;
        this.nomeCliente = nomeCliente;
        this.cpfCnpjCliente = cpfCnpjCliente;
        this.telefoneCliente = telefoneCliente;
        this.nomeModelo = nomeModelo;
        this.marcaModelo = marcaModelo;
        this.corModelo = corModelo;
        this.veiculoStatus = veiculoStatus;
    }






    public Veiculo() {
    }

    public int getCodigo() {
        return codigo;
    }

    public String getPlaca() {
        return placa;
    }

    public String getChassi() {
        return chassi;
    }

    public int getAno() {
        return ano;
    }

    public int getKm() {
        return km;
    }

    public int getCod_cliente() {
        return cod_cliente;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public void setCod_cliente(int cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public int getCod_modveic() {
        return cod_modveic;
    }

    public void setCod_modveic(int cod_modveic) {
        this.cod_modveic = cod_modveic;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getCpfCnpjCliente() {
        return cpfCnpjCliente;
    }

    public void setCpfCnpjCliente(String cpfCnpjCliente) {
        this.cpfCnpjCliente = cpfCnpjCliente;
    }

    public String getNomeModelo() {
        return nomeModelo;
    }

    public void setNomeModelo(String nomeModelo) {
        this.nomeModelo = nomeModelo;
    }

    public String getMarcaModelo() {
        return marcaModelo;
    }

    public void setMarcaModelo(String marcaModelo) {
        this.marcaModelo = marcaModelo;
    }

    public String getCorModelo() {
        return corModelo;
    }

    public void setCorModelo(String corModelo) {
        this.corModelo = corModelo;
    }

    public String getTelefoneCliente() {
        return telefoneCliente;
    }

    public void setTelefoneCliente(String telefoneCliente) {
        this.telefoneCliente = telefoneCliente;
    }

    public String getVeiculoStatus() {
        return veiculoStatus;
    }

    public void setVeiculoStatus(String veiculoStatus) {
        this.veiculoStatus = veiculoStatus;
    }
    

    
    
}
