package models;

import java.time.LocalDate;

public class PessoaFisica extends Cliente {

    private String cpf_cnpj;
    private String rg_ie;
    private String nome;

    public PessoaFisica(String cpf_cnpj, String rg_ie,String nome, int codigo, String email, String logradouro, String numero, String bairro, String municipio,
            String estado, String complemento, String telefone, LocalDate dataNasc, int tipoPessoa, String status) {
        super(codigo, email, logradouro, numero, bairro, municipio, estado, complemento, telefone, dataNasc, tipoPessoa,status);
        this.cpf_cnpj = cpf_cnpj;
        this.rg_ie = rg_ie;
        this.nome = nome;
    }
    
    public PessoaFisica(String nome){
    
        this.nome = nome;
    }
    
     public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public PessoaFisica(){
    
    }
     public String getCpfCnpj() {
        return cpf_cnpj;
    }

    public void setCpfCnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getRgIe() {
        return rg_ie;
    }

    public void setRgIe(String rg_ie) {
        this.rg_ie = rg_ie;
    }
    


}
