CREATE DATABASE ogm
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'C'
       LC_CTYPE = 'C'
       CONNECTION LIMIT = -1;


create table clientes(
	codigo serial primary key,
	nome varchar(50),
	cpf_cnpj varchar(50) unique,
	tipo_pessoa INTEGER,
	razaosocial varchar(50),
	rg varchar(50),
	dataNasc date,
	email varchar(50),
	telefone varchar(50),
	rua varchar(50),
	numest varchar(30),
	bairro varchar(50),
	municipio varchar(50),
	estado varchar(50),
	complemento varchar(50),
	clienteStatus varchar(50)

);


create table user_atual(
	id serial,
	nome varchar(50),
	codigo integer
);

create table usuario(
	codigo serial primary key,
	usuario varchar(50) unique,
	usuarioStatus varchar(50),
	senha varchar(1000),
	cpf varchar(50),
	tipo varchar(50),
	editar boolean,
	inserir boolean,
	excluir boolean
);


create table modveiculo(

	codigo serial primary key,
	nomeModelo varchar(50),
	marca varchar(50),
	cor varchar(50),
	qtdPortas integer,
	motor varchar(50),
	combustivel varchar(50),
	modeloStatus varchar(50)
);


create table veiculos(
	codigo serial primary key,
	placa varchar(50) unique,
	chassi varchar(50) unique,
	ano INTEGER,
	km INTEGER,
	cod_cliente integer,
	cod_modveic integer,
	veiculostatus varchar(50),
	CONSTRAINT cod_cliente FOREIGN KEY(cod_cliente) REFERENCES clientes(codigo),
	CONSTRAINT cod_modveic FOREIGN KEY(cod_modveic) REFERENCES modveiculo(codigo)
	);
	

create table pecaServico(

	codigo serial primary key,
	descricao varchar(50) unique,
	valor double PRECISION,
	tipo integer,
	pecaServicoStatus varchar(50)
);


create table os(
	codigo serial primary key,
	data_inclusao date,
	data_saida date,
	previsao_entrega date,
	osStatus varchar(30),
	horarioInclusao time,
	horarioPrevisto time,
	horarioSaida time,
	status varchar(30),
	desc_problema varchar(4000),
	valor_total double precision,
	cod_veiculo integer,
	cod_mecanico integer,
	cod_cliente integer,
	CONSTRAINT cod_cliente FOREIGN KEY(cod_cliente) REFERENCES clientes(codigo),
	CONSTRAINT cod_veiculo FOREIGN KEY(cod_veiculo) REFERENCES veiculos(codigo),
	CONSTRAINT cod_mecanico FOREIGN KEY(cod_mecanico) REFERENCES usuario(codigo)

);



create table osPecasServicos(
	codigo serial primary key,
	
	cod_os serial,
	cod_peca_servico integer,
	quantidade integer,
	valorUn double precision,
	valortotalPecaServ double precision,
	CONSTRAINT cod_os FOREIGN KEY(cod_os) REFERENCES os(codigo) ON DELETE CASCADE ,
	CONSTRAINT cod_peca_servico FOREIGN KEY(cod_peca_servico) REFERENCES pecaServico(codigo)

);



create table bk_clientes(
	codigo serial primary key,
	nome varchar(50),
	cpf_cnpj varchar(50) unique,
	tipo_pessoa INTEGER,
	razaosocial varchar(50),
	rg varchar(50),
	dataNasc date,
	email varchar(50),
	telefone varchar(50),
	rua varchar(50),
	numest varchar(30),
	bairro varchar(50),
	municipio varchar(50),
	estado varchar(50),
	complemento varchar(50),
	clienteStatus varchar(50)
);

CREATE OR REPLACE FUNCTION public.salvarexcluidosCliente()
RETURNS trigger AS
$BODY$ BEGIN
INSERT INTO bk_clientes VALUES (old.codigo,old.nome,old.cpf_cnpj,old.tipo_pessoa,old.razaosocial,old.rg,old.dataNasc, old.email, old.telefone, old.rua, old.numest, old.bairro,old.municipio, old.estado, old.complemento, old.clienteStatus);

RETURN NULL;
END;$BODY$
LANGUAGE 'plpgsql';


CREATE TRIGGER exclusao_cliente
AFTER DELETE
ON public.clientes
FOR EACH ROW
EXECUTE PROCEDURE public.salvarexcluidosCliente();

create table bk_usuario(
	codigo serial primary key,
	usuario varchar(50) unique,
	usuarioStatus varchar(50),
	senha varchar(1000),
	cpf varchar(50),
	tipo varchar(50),
	editar boolean,
	inserir boolean,
	excluir boolean
);

CREATE OR REPLACE FUNCTION public.salvarexcluidosUsuario()
RETURNS trigger AS
$BODY$ BEGIN
INSERT INTO bk_usuario VALUES (old.codigo,old.usuario,old.usuarioStatus,old.senha,old.cpf,old.tipo,old.editar, old.inserir, old.excluir);

RETURN NULL;
END;$BODY$
LANGUAGE 'plpgsql';


CREATE TRIGGER exclusao_usuario
AFTER DELETE
ON public.usuario
FOR EACH ROW
EXECUTE PROCEDURE public.salvarexcluidosUsuario();

create table bk_veiculos(
	codigo serial primary key,
	placa varchar(50) unique,
	chassi varchar(50) unique,
	ano INTEGER,
	km INTEGER,
	cod_cliente integer,
	cod_modveic integer,
	veiculostatus varchar(50)
	
);

CREATE OR REPLACE FUNCTION public.salvarexcluidosVeiculo()
RETURNS trigger AS
$BODY$ BEGIN
INSERT INTO bk_veiculos VALUES (old.codigo, old.placa, old.chassi, old.ano, old.km , old.cod_cliente , old.cod_modveic, old.veiculostatus);

RETURN NULL;
END;$BODY$
LANGUAGE 'plpgsql';


CREATE TRIGGER exclusao_veiculo
AFTER DELETE
ON public.veiculos
FOR EACH ROW
EXECUTE PROCEDURE public.salvarexcluidosVeiculo();

create table bk_modveiculo(

	codigo serial primary key,
	nomeModelo varchar(50),
	marca varchar(50),
	cor varchar(50),
	qtdPortas integer,
	motor varchar(50),
	combustivel varchar(50),
	modeloStatus varchar(50)
);

CREATE OR REPLACE FUNCTION public.salvarexcluidosModveiculo()
RETURNS trigger AS
$BODY$ BEGIN
INSERT INTO bk_modveiculo VALUES (old.codigo, old.nomeModelo, old.marca, old.cor, old.qtdPortas , old.motor , old.combustivel, old.modeloStatus);

RETURN NULL;
END;$BODY$
LANGUAGE 'plpgsql';


CREATE TRIGGER exclusao_Modveiculo
AFTER DELETE
ON public.modveiculo
FOR EACH ROW
EXECUTE PROCEDURE public.salvarexcluidosModveiculo();


create table bk_pecaServico(

	codigo serial primary key,
	descricao varchar(50) unique,
	valor double PRECISION,
	tipo integer,
	pecaServicoStatus varchar(50)
);

CREATE OR REPLACE FUNCTION public.salvarexcluidosPecaServico()
RETURNS trigger AS
$BODY$ BEGIN
INSERT INTO bk_pecaServico VALUES (old.codigo, old.descricao, old.valor, old.tipo, old.pecaServicoStatus);

RETURN NULL;
END;$BODY$
LANGUAGE 'plpgsql';


CREATE TRIGGER exclusao_pecaServico
AFTER DELETE
ON public.pecaServico
FOR EACH ROW
EXECUTE PROCEDURE public.salvarexcluidosPecaServico();